----------------------------------------------------------------------

WinMP3Shrink - "read me" file.

  Introduction

  1) Use

  2) Legal mentions

  3) History

----------------------------------------------------------------------

Introduction:
------------

WinMP3Shrink, UTILITY FOR MP3 ENCODING/RE-ENCODING.

Simply encode or re-encode your MP3 by using the famous Lame encoder.

A simple and efficient front-end.

Add some files in the list, by using one of the two buttons,
add file(s) or import folder.

You can choose the bit rate and encoding mode;
CBR (Constant Bit Rate) or ABR (Average Bit Rate).

Id3 tags (v1 & v2) can be preserved.

This software can also work with a full folder containing
sub-directories, optionally reproducing the folder structure.

Very light, no installation is required.

----------------------------------------------------------------------

1) Use:
   ---

Select your files, your bitrate, your encoding mode,
then click on "GO!". Amazing, isn't it? ;)

You can select more than one file in "Add files...".

By default, each recompressed file is stored in the source
directory, and a suffix is added to the file name.

Ex: let a file "C:\Audio\Morris dancers mania.mp3".

If you choose CBR 112 kbps as encoding parameters, you get:
"C:\Audio\Morris dancers mania_C112kbps.mp3"

If you choose ABR 96 kbps as encoding parameters, you get:
"C:\Audio\Morris dancers mania_A96kbps.mp3"

This behaviour can be changed in "advanced parameters",
and you can choose a fixed destination folder:
in this case, a single checkbox allows to get a full
reproduction of the imported folder structure,
and the suffixes are not added to the file names.

If you understand french, find a detailed tutorial at:
http://www.gravure-news.com/html/tuto/35/page01.php

----------------------------------------------------------------------

2) Legal mentions:
   --------------

This software uses the Lame encoder
http://www.mp3dev.org

The part "verification of a new version and download"
is realized with help of the Indy Delphi components suite.
http://www.indyproject.org

Compression by UPX
http://upx.sourceforge.net/

Particular thanks to Gravure-News and everyone who
helps me to realize this project and make it evolve...
http://www.gravure-news.com

----------------------------------------------------------------------

3) History:
   -------


* 12/23/2006: version 1.3 build 1396 release 24 [12/23/2006 19:26]

  functionalities added/modified:

  - "DRAG AND DROP" of files and folders from Windows explorer.

  - New option: "Lame priority".

  - For the destination folder: button "Create a new folder".

  - Now, the same file can't appear twice in the list.

  - Pretty icons in option tabs.

  - Lame 3.97 final included.

  - Compressed with UPX 2.03.

  - Communication with Lame has been entirely rewritten in prevision of
    next evolutions. LameInfo.exe is no more necessary.


  corrected bugs:

  - Some problems concerning the choice of a directory
    (floppy drive searching, name edition, etc.).

  - No more ugly blinking while encoding.

  - Some minor bugs.



* 01/14/2006: version 1.2 build 1253 release 23 [01/14/2006 13:15]

  functionalities added/modified:

  - Cosmetic modifications for the main window and the "Options" dialog.

  - New option: computer shutdown

  - New option: delayed start.

  - First step of deep modifications concerning localization ability.
    If you want to add your language to WinMP3Shrink,
    please contact me first on http://capjack.free.fr.
    You will obtain the localization kit.
    Note: this version is always Ansi, not Unicode.

  - WinMP3Shrink now correctly works under Windows 95.
    More a real challenge than a real utility.


  corrected bugs:

  - Problems when the option "keep sub-dirs" was not checked.

  - Non-PCM wav files were not correctly intercepted.

  - Minor display problems under Windows 9x



* 08/31/2005: version 1.1 build 1006 release 22 [08/31/2005 22:01]


  functionalities added/modified:

  - Complete rewriting of the "new version" detection system.

  - Socket error is now displayed.


  corrected bugs:

  - SERIOUS BUG: in some particular cases, original mp3 file
    was crushed. Corrected.

  - Some minor problems concerning the english localization.

  - Minor bug: in some very particular cases, when importing
    a folder, the error "destination folder is included in
    the source folder" was incorrectly reported.


* 08/06/2005: version 1.0 build 1000 release 21 [08/06/2005 19:42]

  FIRST STABLE VERSION RELEASED

--------------------------------------------------------------------------------------------------------------------------------------------

Software entirely coded with Delphi 2005 by CapJack

(c) CapJack 2005

http://capjack.free.fr

----------------------------------------------------------------------
