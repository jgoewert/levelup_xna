----------------------------------------------------------------------

WinMP3Shrink - fichier "lisez-moi".

  Introduction

  1) Utilisation

  2) Mentions l�gales

  3) Historique

----------------------------------------------------------------------

Introduction :
------------

WinMP3Shrink, UTILITAIRE d'ENCODAGE/REENCODAGE DE MP3.

Encodez ou r�-encodez vos mp3 simplement, avec le c�l�bre encodeur Lame

Interface graphique simple et efficace.

Ajoutez des fichiers dans la liste, � l'aide d'un des deux boutons
"Importer un dossier" ou "Ajouter des fichiers".

On peut choisir le bitrate et le type d'encodage :
CBR (Constant Bit Rate) ou ABR (Average Bit Rate).

Les tags Id3 (v1 & v2) peuvent �tre conserv�s.

Ce logiciel permet de travailler avec des r�pertoires entiers,
et peut m�me reproduire la structure de sous-r�pertoires.

Tr�s l�ger, aucune installation n'est requise.

----------------------------------------------------------------------

1) Utilisation :
   -----------

S�lectionnez vos fichiers, votre bitrate, votre mode,
et cliquez sur "GO!". �tonnant, non ? ;)

Il est possible de s�lectionner plusieurs fichiers � la fois.

Par d�faut, chaque mp3 recompress� est stock� dans le r�pertoire
d'origine, et se voit ajouter l'indication du bitrate � son nom.

Ex : soit le fichier "C:\Audio\L'accord�on de tonton Maurice.mp3".

Si le bitrate choisi est CBR 112 kbps, le fichier r�sultant sera :
"C:\Audio\L'accord�on de tonton Maurice_C112kbps.mp3"

Si le bitrate choisi est ABR 96 kbps, le fichier r�sultant sera :
"C:\Audio\L'accord�on de tonton Maurice_A96kbps.mp3"

Il est possible de modifier ce comportement dans les param�tres
avanc�s, et de d�finir un r�pertoire de destination fixe :
dans ce cas, une case � cocher permet d'obtenir la reproduction
des sous-r�pertoires import�s, et il n'y aura pas de suffixes
ajout�s aux noms des fichiers.

Un tutoriel en fran�ais est disponible � cette adresse :
http://www.gravure-news.com/html/tuto/35/page01.php

----------------------------------------------------------------------

2) Mentions l�gales :
   ----------------

Ce logiciel utilise l'encodeur Lame
http://www.mp3dev.org

La partie "v�rification d'une nouvelle version et t�l�chargement"
est r�alis�e � l'aide des composants Delphi de la suite Indy
http://www.indyproject.org

Compression par UPX
http://upx.sourceforge.net/

Un remerciement tout particulier � Gravure-News, et � tous ceux
qui me permettent de r�aliser ce projet et de le faire �voluer...
http://www.gravure-news.com

----------------------------------------------------------------------

3) Historique :
   ----------


* 23/12/2006 : version 1.3 build 1396 release 24 [23/12/2006 19:26]

  fonctions ajout�es/modifi�es :

  - "DRAG AND DROP" de fichiers et r�pertoires depuis l'explorateur Windows.

  - Nouvelle option : "priorit� de Lame".

  - Pour le dossier de destination : bouton "Cr�er un nouveau dossier"

  - Le m�me fichier ne peut plus appara�tre plus d'une fois dans la liste.

  - Ic�nes d�coratives dans les onglets d'options

  - Livr� avec la version finale de Lame 3.97

  - Communication avec Lame enti�rement r��crite en vue d'�volutions future;
    LameInfo.exe n'est plus n�cessaire

  - Compress� avec UPX 2.03


  bugs corrig�s :

  - Correction de plusieurs probl�mes li�s au choix d'un r�pertoire
    (lecteurs de disquettes qui cherchent, �dition, etc.)

  - Suppression du clignotement d�sagr�able lors de l'encodage

  - Autres bugs mineurs



* 14/01/2006 : version 1.2 build 1253 release 23 [14/01/2006 13:15]

  fonctions ajout�es/modifi�es :

  - Modifications cosm�tiques de la fen�tre principale
    et de la bo�te de dialogue d'options.

  - Nouvelle option : extinction de l'ordinateur � la fin du travail

  - Nouvelle option : d�marrage en diff�r� r�glable.

  - Premi�re �tape de modifications en profondeur concernant
    la capacit� de localisation. Si vous d�sirez ajouter votre langue
    � WinMP3Shrink, contactez-moi sur http://capjack.free.fr.
    Vous recevrez alors le kit de localisation.
    Note : cette version est toujours en Ansi, pas en Unicode.

  - WinMP3Shrink fonctionne maintenant correctement sous Windows 95;
    plus un d�fi personnel qu'une r�elle utilit�...


  bugs corrig�s :

  - Probl�mes si l'option "reproduire les sous r�pertoires" est d�coch�e.

  - L'interception des fichiers .wav non-PCM �tait incorrecte.

  - Probl�mes d'affichage mineurs sous Windows 9x.



* 31/08/2005 : version 1.1 build 1006 release 22 [31/08/2005 22:01]


  fonctions ajout�es/modifi�es :

  - R��criture compl�te du syst�me de mise � jour automatique.

  - L'erreur socket �ventuelle est affich�e.


  bugs corrig�s :

  - BUG GRAVE : dans certains cas de figure, il pouvait y avoir
    �crasement du mp3 original. Corrig�.

  - Quelques probl�mes de traduction de la version anglaise.

  - bug b�nin : dans des cas tr�s particuliers, lors de l'importation
    d'un dossier, se d�clenchait intempestivement l'erreur :
    "R�pertoire de destination inclus dans le r�pertoire source".



* 06/08/2005 : version 1.0 build 1000 release 21 [06/08/2005 19:42]

  PREMI�RE VERSION STABLE

--------------------------------------------------------------------------------------------------------------------------------------------

Programme enti�rement r�alis� sous Delphi 2005 par CapJack

(c) CapJack 2005

http://capjack.free.fr

----------------------------------------------------------------------
