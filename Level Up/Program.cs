#region Header
// --------------------------------
// <copyright file="Program.cs" company="John Goewert">
//     Copyright John Goewert 2011.
// </copyright>
// <author>John Goewert</author>
// ---------------------------------
#endregion

namespace Level_Up
{
#if WINDOWS || XBOX

    internal static class Program
    {
        #region Methods

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        private static void Main(string[] args)
        {
            using (var game = new Game1())
            {
                game.Run();
            }
        }

        #endregion Methods
    }

#endif
}