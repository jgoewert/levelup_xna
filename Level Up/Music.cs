﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Content;
namespace Level_Up
{
    public class Music
    {
        private MediaState previousMediaState;
        Dictionary<string, Song> songs = new Dictionary<string, Song>();

        public Music(ContentManager content)
        {
            // Setup music player
            MediaPlayer.IsRepeating = false;
            MediaPlayer.IsShuffled = false;
            MediaPlayer.Volume = 1.0f;
        }

        public void LoadContent()
        {
            DirectoryInfo dir = new DirectoryInfo(Global.content.RootDirectory + "\\Music\\");
            FileInfo[] files = dir.GetFiles("*.xnb");
            foreach (FileInfo file in files)
            {
                this.AddSong(Path.GetFileNameWithoutExtension(file.Name), Global.content.Load<Song>("Music\\" + Path.GetFileNameWithoutExtension(file.Name)));
            }
            MediaPlayer.Stop(); 
            previousMediaState = MediaState.Playing;
        }

        public void SetSong(string key)
        {
            if (!MediaPlayer.IsMuted)
            {
                try
                {
                    MediaPlayer.Stop();
                Song currentSong = songs[key];
                MediaPlayer.Play(currentSong);
                }
                catch(Exception e)
                {}
            }
        }

        public void SetRandomSong()
        {

            Song[] songarray = songs.Values.ToArray();
            try
            {
                MediaPlayer.Play((Song) Global.Pick(songarray));
            }
            catch (InvalidOperationException e)
            {

            }
        }

        public void Update()
        {
            if ((MediaPlayer.State == MediaState.Stopped) && (previousMediaState == MediaState.Playing))
            {
                Song[] songarray = songs.Values.ToArray();
                try
                {
                    MediaPlayer.Play((Song) Global.Pick(songarray));
                }
                catch (InvalidOperationException e)
                {
                    
                }
            }

            previousMediaState = MediaPlayer.State;
        }

        public void AddSong(string name, Song newSong)
        {
            songs.Add(name, newSong);
        }

        public void Stop()
        {
            MediaPlayer.Stop();
        }

        public void Pause()
        {
            MediaPlayer.Pause();
        }

        public void Resume()
        {
            MediaPlayer.Resume();
        }
    }
}
