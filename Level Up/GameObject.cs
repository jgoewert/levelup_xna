﻿#region Header
// --------------------------------
// <copyright file="GameObject.cs" company="John Goewert">
//     Copyright John Goewert 2011.
// </copyright>
// <author>John Goewert</author>
// ---------------------------------
#endregion

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

public class GameObject
{
    #region Fields

    private bool _alive; // ENCAPSULATE FIELD BY CODEIT.RIGHT

    public bool Alive
    {
        get
        {
            return _alive;
        }
        set
        {
            _alive = value;
        }
    }
    private Vector2 boundary;

    /* Boundary Rules
         * 0 - No rules
         * 1 - Destroy on leave
         * 2 - Can't Leave Screen
         * 3 - Can't Leave Horizonal, Flips Vertical
         */
    private int _boundaryRules; // ENCAPSULATE FIELD BY CODEIT.RIGHT

    public int BoundaryRules
    {
        get
        {
            return _boundaryRules;
        }
        set
        {
            _boundaryRules = value;
        }
    }
    private bool _dieing; // ENCAPSULATE FIELD BY CODEIT.RIGHT

    public bool Dieing
    {
        get
        {
            return _dieing;
        }
        set
        {
            _dieing = value;
        }
    }
    private Vector2 _position; // ENCAPSULATE FIELD BY CODEIT.RIGHT

    public Vector2 Position
    {
        get
        {
            return _position;
        }
        set
        {
            _position = value;
        }
    }
    private float _rotation; // ENCAPSULATE FIELD BY CODEIT.RIGHT

    public float Rotation
    {
        get
        {
            return _rotation;
        }
        set
        {
            _rotation = value;
        }
    }
    private float _rotationSpeed; // ENCAPSULATE FIELD BY CODEIT.RIGHT

    public float RotationSpeed
    {
        get
        {
            return _rotationSpeed;
        }
        set
        {
            _rotationSpeed = value;
        }
    }
    private float _scale = 1.0f; // ENCAPSULATE FIELD BY CODEIT.RIGHT

    public float Scale
    {
        get
        {
            return _scale;
        }
        set
        {
            _scale = value;
        }
    }
    private Sprites.AnimatedTexture _sprite; // ENCAPSULATE FIELD BY CODEIT.RIGHT

    public Sprites.AnimatedTexture Sprite
    {
        get
        {
            return _sprite;
        }
        set
        {
            _sprite = value;
        }
    }
    private Rectangle _spriteRectangle; // ENCAPSULATE FIELD BY CODEIT.RIGHT

    public Rectangle SpriteRectangle
    {
        get
        {
            return _spriteRectangle;
        }
        set
        {
            _spriteRectangle = value;
        }
    }
    private Vector2 _velocity; // ENCAPSULATE FIELD BY CODEIT.RIGHT

    public Vector2 Velocity
    {
        get
        {
            return _velocity;
        }
        set
        {
            _velocity = value;
        }
    }

    #endregion Fields

    #region Constructors

    public GameObject(ContentManager content, string image, int numFrames, Vector2 viewport)
    {
        /*Rotation = 0.0f*/;
        Position = Vector2.Zero;
        Sprite = new Sprites.AnimatedTexture(Vector2.Zero, Rotation, 1.0f, 0.5f);
        Sprite.Load(content, image, numFrames, 6);
        Velocity = Vector2.Zero;
        this.Alive = false;
        boundary = viewport;
    }

    #endregion Constructors

    #region Methods

    public void DrawFrame(SpriteBatch batch)
    {
        Sprite.DrawFrame(batch, Position, Rotation);
    }

    public void Update(float elapsed)
    {
        Sprite.UpdateFrame(elapsed);
        Position += Velocity * elapsed;
        Rotation += RotationSpeed * elapsed;

        switch (BoundaryRules)
        {
            case 0:
                break;
            case 1:
                if ((_position.X < 0) || (_position.X > boundary.X) ||
                    (_position.Y < 0) || (_position.Y > boundary.Y))
                    this.Alive = false;
                break;
            case 2:
                if (_position.X < 0)
                {
                    _position.X = 0;
                }
                else
                {
                    if (_position.X > boundary.X)
                    {
                        _position.X = boundary.X;
                    }
                }
                if (_position.Y < 0)
                {
                    _position.Y = 0;
                }
                else
                {
                    if (_position.Y > boundary.Y)
                    {
                        _position.Y = boundary.Y;
                    }
                }
                break;
            case 3:
                if (_position.X < 0)
                {
                    _position.X = 0;
                }
                else
                {
                    if (_position.X > boundary.X)
                    {
                        _position.X = boundary.X;
                    }
                }
                if (_position.Y < 0)
                {
                    _position.Y = boundary.Y;
                }
                else
                {
                    if (_position.Y > boundary.Y)
                    {
                        _position.Y = 0;
                    }
                }
                break;
        }
        SpriteRectangle = new Rectangle(
            (int)Position.X - (Sprite.Width / 2),
            (int)Position.Y - (Sprite.Height / 2),
            Sprite.Width,
            Sprite.Height);
    }

    #endregion Methods
}