﻿//-------------------------------------------------------------------------------------------------
// <copyright file="C:\proj\levelup\Level Up\Global.cs" company="John Goewert">
// Copyright (c) John Goewert.  All rights reserved.
// </copyright>
//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------
// <copyright file="C:\proj\levelup\Level Up\Global.cs" company="John Goewert">
// Copyright (c) John Goewert.  All rights reserved.
// </copyright>
//-------------------------------------------------------------------------------------------------
// --------------------------------
// <copyright file="Global.cs" company="John Goewert">
//     Copyright John Goewert 2011.
// </copyright>
// <author>John Goewert</author>
// ---------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using EasyStorage;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Content;
using SpynDoctor;
using System.Globalization;
using System.IO;
using Level_Up;
using EasyConfig;

#if DEBUG || XBOX
using Microsoft.Xna.Framework.GamerServices;
#endif



public sealed class Global
{
    public static GraphicsDeviceManager Graphics;
    public static SharedSaveDevice SaveDevice;
    public static ContentManager content;
    public static bool Tutorial;
    public static EasyConfig.ConfigFile configFile;
    public static int MenuStyle;
    public static int TutorialCounter = 0;

    public static Item[] Armors = new[]
                               {
                                   new Item("Lace", 1),
                                   new Item("Macrame", 2),
                                   new Item("Burlap", 3),
                                   new Item("Canvas", 4),
                                   new Item("Flannel", 5),
                                   new Item("Chamois", 6),
                                   new Item("Pleathers", 7),
                                   new Item("Leathers", 8),
                                   new Item("Bearskin", 9),
                                   new Item("Ringmail", 10),
                                   new Item("Scale Mail", 12),
                                   new Item("Chainmail", 14),
                                   new Item("Splint Mail", 15),
                                   new Item("Platemail", 16),
                                   new Item("ABS", 17),
                                   new Item("Kevlar", 18),
                                   new Item("Titanium", 19),
                                   new Item("Mithril Mail", 20),
                                   new Item("Diamond Mail", 25),
                                   new Item("Plasma", 30)
                               };
  
    public static string[] BoringItems = new[]
                                      {
                                          "nail",
                                          "lunchpail",
                                          "sock",
                                          "I.O.U.",
                                          "cookie",
                                          "pint",
                                          "toothpick",
                                          "writ",
                                          "newspaper",
                                          "letter",
                                          "plank",
                                          "hat",
                                          "egg",
                                          "coin",
                                          "needle",
                                          "bucket",
                                          "ladder",
                                          "chicken",
                                          "twig",
                                          "dirtclod",
                                          "counterpane",
                                          "vest",
                                          "teratoma",
                                          "bunny",
                                          "rock",
                                          "pole",
                                          "carrot",
                                          "canoe",
                                          "inkwell",
                                          "hoe",
                                          "bandage",
                                          "trowel",
                                          "towel",
                                          "planter box",
                                          "anvil",
                                          "axle",
                                          "tuppence",
                                          "casket",
                                          "nosegay",
                                          "trinket",
                                          "credenza",
                                          "writ"
                                      }; 

    public static Item[] Classes = new[]
                                {
                                    new Item("Ur-Paladin", 0, new[] {"Wis", "Con"}),
                                    new Item("Voodoo Princess", 0, new[] {"Int", "Cha"}),
                                    new Item("Robot Monk", 0, new[] {"Str"}),
                                    new Item("Mu-Fu Monk", 0, new[] {"Dex"}),
                                    new Item("Mage Illusioner", 0, new[] {"Int", "MP Max"}),
                                    new Item("Shiv-Knight", 0, new[] {"Dex"}),
                                    new Item("Inner Mason", 0, new[] {"Con"}),
                                    new Item("Fighter-Organist", 0, new[] {"Cha", "Str"}),
                                    new Item("Puma Burgular", 0, new[] {"Dex"}),
                                    new Item("Runeloremaster", 0, new[] {"Wis"}),
                                    new Item("Hunter Strangler", 0, new[] {"Dex", "Int"}),
                                    new Item("Battle-Felon", 0, new[] {"Str"}),
                                    new Item("Tickle-Mimic", 0, new[] {"Wis", "Int"}),
                                    new Item("Slow Poisoner", 0, new[] {"Con"}),
                                    new Item("Bastard Lunatic", 0, new[] {"Con"}),
                                    new Item("Lowling", 0, new[] {"Wis"}),
                                    new Item("Birdrider", 0, new[] {"Wis"}),
                                    new Item("Vermineer", 0, new[] {"Int"}),
                                    new Item("Herpavore", 0, new[] {"Con"})
                                }; 

    public static string[] DefenseAttrib = new[]
                                        {
                                            "Studded|+1",
                                            "Banded|+2",
                                            "Gilded|+2",
                                            "Festooned|+3",
                                            "Holy|+4",
                                            "Cambric|+1",
                                            "Fine|+4",
                                            "Impressive|+5",
                                            "Custom|+3"
                                        };

    public static string[] DefenseBad = new[]
                                     {
                                         "Holey|-1",
                                         "Patched|-1",
                                         "Threadbare|-2",
                                         "Faded|-1",
                                         "Rusty|-3",
                                         "Motheaten|-3",
                                         "Mildewed|-2",
                                         "Torn|-3",
                                         "Dented|-3",
                                         "Cursed|-5",
                                         "Plastic|-4",
                                         "Cracked|-4",
                                         "Warped|-3",
                                         "Corroded|-3"
                                     }; 

    public static string[] Equips = new[]
                                 {
                                     "Weapon",
                                     "Shield",
                                     "Helm",
                                     "Hauberk",
                                     "Brassairts",
                                     "Vambraces",
                                     "Gauntlets",
                                     "Gambeson",
                                     "Cuisses",
                                     "Greaves",
                                     "Sollerets"
                                 };

    public static string[] ImpressiveTitles = new[]
                                           {
                                               "King",
                                               "Queen",
                                               "Lord",
                                               "Lady",
                                               "Viceroy",
                                               "Mayor",
                                               "Prince",
                                               "Princess",
                                               "Chief",
                                               "Boss",
                                               "Archbishop"
                                           };

    public static string[] ItemAttrib = new[]
                                     {
                                         "Golden",
                                         "Gilded",
                                         "Spectral",
                                         "Astral",
                                         "Garlanded",
                                         "Precious",
                                         "Crafted",
                                         "Dual",
                                         "Filigreed",
                                         "Cruciate",
                                         "Arcane",
                                         "Blessed",
                                         "Reverential",
                                         "Lucky",
                                         "Enchanted",
                                         "Gleaming",
                                         "Grandiose",
                                         "Sacred",
                                         "Legendary",
                                         "Mythic",
                                         "Crystalline",
                                         "Austere",
                                         "Ostentatious",
                                         "One True",
                                         "Proverbial",
                                         "Fearsome",
                                         "Deadly",
                                         "Benevolent",
                                         "Unearthly",
                                         "Magnificent",
                                         "Iron",
                                         "Ormolu",
                                         "Puissant"
                                     };

    public static string[] ItemOfs = new[]
                                  {
                                      "Foreboding",
                                      "Foreshadowing",
                                      "Nervousness",
                                      "Happiness",
                                      "Torpor",
                                      "Danger",
                                      "Craft",
                                      "Silence",
                                      "Invisibility",
                                      "Rapidity",
                                      "Pleasure",
                                      "Practicality",
                                      "Hurting",
                                      "Joy",
                                      "Petulance",
                                      "Intrusion",
                                      "Chaos",
                                      "Suffering",
                                      "Extroversion",
                                      "Frenzy",
                                      "Solitude",
                                      "Punctuality",
                                      "Efficiency",
                                      "Comfort",
                                      "Patience",
                                      "Internment",
                                      "Incarceration",
                                      "Misapprehension",
                                      "Loyalty",
                                      "Envy",
                                      "Acrimony",
                                      "Worry",
                                      "Fear",
                                      "Awe",
                                      "Guile",
                                      "Prurience",
                                      "Fortune",
                                      "Perspicacity",
                                      "Domination",
                                      "Submission",
                                      "Fealty",
                                      "Hunger",
                                      "Despair",
                                      "Cruelty",
                                      "Grob",
                                      "Dignard",
                                      "Ra",
                                      "the Bone",
                                      "Diamonique",
                                      "Electrum",
                                      "Hydragyrum"
                                  };

    public static string[] MonMods = new[]
                                  {
                                      "-4 tiny *",
                                      "-4 dying *",
                                      "-3 crippled *",
                                      "-3 baby *",
                                      "-2 adolescent",
                                      "-2 very sick *",
                                      "-1 lesser *",
                                      "-1 undernourished *",
                                      "-1 lonely *",
                                      "+1 greater *",
                                      "+1 * Elder",
                                      "+2 war *",
                                      "+2 Battle-*",
                                      "+3 Were-*",
                                      "+3 undead *",
                                      "+4 giant *",
                                      "+4 * Rex"
                                  };

    public static Item[] Monsters = new[]
                                 {
                                     new Item("Anhkheg", 6, new[] {"chitin"}),
                                    new Item("Anklyosaurus", 9, new[] {"tail"}),
                                    new Item("Ant", 0, new[] {"antenna"}),
                                    new Item("Ape", 4, new[] {"rear"}),
                                    new Item("Asmodeus", 52, new[] {"leathers"}),
                                    new Item("Baalzebul", 43, new[] {"pajamas"}),
                                    new Item("Bacon Elemental", 10, new[] {"bit"}),
                                    new Item("Balor", 8, new[] {"whip"}),
                                    new Item("Baluchitherium", 14, new[] {"ear"}),
                                    new Item("Banshee", 7, new[] {"larynx"}),
                                    new Item("Barbed Devil", 8, new[] {"flame"}),
                                    new Item("Bat", 1, new[] {"wing"}),
                                    new Item("Beagle", 0, new[] {"collar"}),
                                    new Item("Beef Giant", 11, new[] {"steak"}),
                                    new Item("Beer Golem", 15, new[] {"foam"}),
                                    new Item("Beholder", 10, new[] {"eyestalk"}),
                                    new Item("Beige Dragon", 9, new[] {"*"}),
                                    new Item("Berserker", 1, new[] {"shirt"}),
                                    new Item("Black Dragon", 7, new[] {"*"}),
                                    new Item("Black Pudding", 10, new[] {"saliva"}),
                                    new Item("Blink Dog", 4, new[] {"eyelid"}),
                                    new Item("Blue Dragon", 9, new[] {"*"}),
                                    new Item("Bone Devil", 9, new[] {"hook"}),
                                    new Item("Boogie", 3, new[] {"slime"}),
                                    new Item("Boy Scout", 3, new[] {"merit-badge"}),
                                    new Item("Brass Dragon", 7, new[] {"pole"}),
                                    new Item("Brontosaurus", 30, new[] {"brain"}),
                                    new Item("Bronze Dragon", 9, new[] {"medal"}),
                                    new Item("Brown Elf", 1, new[] {"tusk"}),
                                    new Item("Bug", 0, new[] {"wing"}),
                                    new Item("Bugbear", 3, new[] {"skin"}),
                                    new Item("Bugboar", 3, new[] {"tusk"}),
                                    new Item("Cardboard Golem", 14, new[] {"recycling"}),
                                    new Item("Carrion Crawler", 3, new[] {"egg"}),
                                    new Item("Catoblepas", 6, new[] {"neck"}),
                                    new Item("Caveman", 2, new[] {"club"}),
                                    new Item("Centipede", 0, new[] {"leg"}),
                                    new Item("Cheese Elemental", 14, new[] {"curd"}),
                                    new Item("Chicken", 1, new[] {"nugget"}),
                                    new Item("Chihuahua", 3, new[] {"taquito"}),
                                    new Item("Chromatic Dragon", 16, new[] {"scale"}),
                                    new Item("Cloud Giant", 12, new[] {"condensation"}),
                                    new Item("Cockatrice", 5, new[] {"wattle"}),
                                    new Item("Copper Dragon", 8, new[] {"loafer"}),
                                    new Item("Crayfish", 0, new[] {"antenna"}),
                                    new Item("Cub Scout", 1, new[] {"neckerchief"}),
                                    new Item("Demogorgon", 53, new[] {"tentacle"}),
                                    new Item("Diplodocus", 24, new[] {"fin"}),
                                    new Item("Dispater", 30, new[] {"matches"}),
                                    new Item("Djinn", 7, new[] {"lamp"}),
                                    new Item("Dragon Turtle", 13, new[] {"shell"}),
                                    new Item("Dwarf", 1, new[] {"lamp"}),
                                    new Item("Eagle Scout", 4, new[] {"merit-badge"}),
                                    new Item("Eel", 2, new[] {"sashimi"}),
                                    new Item("Efreet", 10, new[] {"cinder"}),
                                    new Item("Elasmosaurus", 15, new[] {"neck"}),
                                    new Item("Erinyes", 6, new[] {"twine"}),
                                    new Item("Ettin", 10, new[] {"fur"}),
                                    new Item("Fire Giant", 11, new[] {"cinder"}),
                                    new Item("Fly", 0, new[] {"*"}),
                                    new Item("Frog", 0, new[] {"leg"}),
                                    new Item("Frost Giant", 10, new[] {"winter-tree"}),
                                    new Item("Gargoyle", 4, new[] {"gravel"}),
                                    new Item("Gelatinous Cube", 4, new[] {"jam"}),
                                    new Item("Geryon", 30, new[] {"cornucopia"}),
                                    new Item("Ghast", 4, new[] {"vomit"}),
                                    new Item("Ghost", 10, new[] {"*"}),
                                    new Item("Ghoul", 2, new[] {"muscle"}),
                                    new Item("Girl Scout", 2, new[] {"cookie"}),
                                    new Item("Glabrezu", 10, new[] {"collar"}),
                                    new Item("Gnoll", 2, new[] {"collar"}),
                                    new Item("Gnome", 1, new[] {"hat"}),
                                    new Item("Goblin", 1, new[] {"ear"}),
                                    new Item("Gold Dragon", 8, new[] {"filling"}),
                                    new Item("Gorgon", 8, new[] {"gargle"}),
                                    new Item("Gorgosaurus", 13, new[] {"arm"}),
                                    new Item("Gray Ooze", 3, new[] {"gravy"}),
                                    new Item("Green Dragon", 8, new[] {"*"}),
                                    new Item("Green Slime", 2, new[] {"sample"}),
                                    new Item("Grid Bug", 1, new[] {"carapace"}),
                                    new Item("Griffon", 7, new[] {"sorting-hat"}),
                                    new Item("Hair Elemental", 16, new[] {"follicle"}),
                                    new Item("Hell Hound", 5, new[] {"tongue"}),
                                    new Item("Hezrou", 9, new[] {"leg"}),
                                    new Item("Hill Giant", 8, new[] {"corpse"}),
                                    new Item("Hippocampus", 4, new[] {"mane"}),
                                    new Item("Hippogriff", 3, new[] {"egg"}),
                                    new Item("Hobgoblin", 1, new[] {"patella"}),
                                    new Item("Hydra", 8, new[] {"gyrum"}),
                                    new Item("Ice Devil", 11, new[] {"winter-tree"}),
                                    new Item("Iguanadon", 6, new[] {"thumb"}),
                                    new Item("Imp", 2, new[] {"tail"}),
                                    new Item("Invisible Stalker", 8, new[] {"*"}),
                                    new Item("Iron Peasant", 3, new[] {"rust"}),
                                    new Item("Jellyrock", 9, new[] {"seedling"}),
                                    new Item("Jubilex", 17, new[] {"gel"}),
                                    new Item("Jumpskin", 3, new[] {"shin"}),
                                    new Item("Kobold", 1, new[] {"scalp"}),
                                    new Item("Leather Golem", 15, new[] {"fob"}),
                                    new Item("Leprechaun", 1, new[] {"hat"}),
                                    new Item("Leucrotta", 6, new[] {"hoof"}),
                                    new Item("Lich", 11, new[] {"crown"}),
                                    new Item("Lurker", 10, new[] {"banhammer"}),
                                    new Item("Malebranche", 5, new[] {"fork"}),
                                    new Item("Man-o-war", 3, new[] {"tentacle"}),
                                    new Item("Manes", 1, new[] {"tooth"}),
                                    new Item("Mastodon", 12, new[] {"tusk"}),
                                    new Item("Medusa", 6, new[] {"eye"}),
                                    new Item("Megalosaurus", 12, new[] {"jaw"}),
                                    new Item("Midge", 0, new[] {"corpse"}),
                                    new Item("Mimic", 9, new[] {"hinge"}),
                                    new Item("Mind Flayer", 8, new[] {"tentacle"}),
                                    new Item("Mini Giant", 4, new[] {"pompadour"}),
                                    new Item("Minotaur", 6, new[] {"map"}),
                                    new Item("Moakum", 8, new[] {"shin"}),
                                    new Item("Monoclonius", 8, new[] {"horn"}),
                                    new Item("Morkoth", 7, new[] {"teeth"}),
                                    new Item("Moth", 0, new[] {"dust"}),
                                    new Item("Multicell", 2, new[] {"dendrite"}),
                                    new Item("Mummy", 6, new[] {"gauze"}),
                                    new Item("Nebbish", 1, new[] {"belly"}),
                                    new Item("Neo-Otyugh", 11, new[] {"organ"}),
                                    new Item("Nixie", 1, new[] {"webbing"}),
                                    new Item("Nymph", 3, new[] {"hanky"}),
                                    new Item("Ochre Jelly", 6, new[] {"nucleus"}),
                                    new Item("Octopus", 2, new[] {"beak"}),
                                    new Item("Ogre Mage", 5, new[] {"apparel"}),
                                    new Item("Ogre", 4, new[] {"talon"}),
                                    new Item("Orc", 1, new[] {"snout"}),
                                    new Item("Orcus", 27, new[] {"wand"}),
                                    new Item("Otyugh", 7, new[] {"organ"}),
                                    new Item("Oxygen Golem", 17, new[] {"platelet"}),
                                    new Item("Pentasaurus", 12, new[] {"head"}),
                                    new Item("Peryton", 4, new[] {"antler"}),
                                    new Item("Piercer", 3, new[] {"tip"}),
                                    new Item("Pirate", 1, new[] {"booty"}),
                                    new Item("Pit Fiend", 13, new[] {"seed"}),
                                    new Item("Pixie", 1, new[] {"dust"}),
                                    new Item("Plaid Dragon", 7, new[] {"sporrin"}),
                                    new Item("Platinum Dragon", 21, new[] {"*"}),
                                    new Item("Porcelain Giant", 9, new[] {"fixture"}),
                                    new Item("Poroid", 4, new[] {"node"}),
                                    new Item("Purple Worm", 15, new[] {"dung"}),
                                    new Item("Quartz Giant", 10, new[] {"crystal"}),
                                    new Item("Rakshasa", 7, new[] {"pajamas"}),
                                    new Item("Rat", 1, new[] {"tail"}),
                                    new Item("Red Dragon", 10, new[] {"cocktail"}),
                                    new Item("Remorhaz", 11, new[] {"protrusion"}),
                                    new Item("Rice Giant", 8, new[] {"grain"}),
                                    new Item("Roc", 18, new[] {"wing"}),
                                    new Item("Roper", 11, new[] {"twine"}),
                                    new Item("Rot Grub", 1, new[] {"rot"}),
                                    new Item("Rubber Golem", 16, new[] {"ball"}),
                                    new Item("Rust Monster", 5, new[] {"shavings"}),
                                    new Item("Sand Elemental", 8, new[] {"glass"}),
                                    new Item("Sea Elf", 1, new[] {"jerkin"}),
                                    new Item("Shadow", 3, new[] {"silhouette"}),
                                    new Item("Shambling Mound", 10, new[] {"mulch"}),
                                    new Item("Shedu", 9, new[] {"hoof"}),
                                    new Item("Shrieker", 3, new[] {"stalk"}),
                                    new Item("Silver Dragon", 10, new[] {"*"}),
                                    new Item("Skeleton", 1, new[] {"clavicle"}),
                                    new Item("Spectre", 7, new[] {"vestige"}),
                                    new Item("Spider", 0, new[] {"web"}),
                                    new Item("Sprite", 1, new[] {"can"}),
                                    new Item("Stegosaurus", 18, new[] {"plate"}),
                                    new Item("Stirge", 1, new[] {"proboscis"}),
                                    new Item("Stone Giant", 9, new[] {"gravel"}),
                                    new Item("Storm Giant", 15, new[] {"barometer"}),
                                    new Item("Stun Bear", 5, new[] {"tooth"}),
                                    new Item("Stun Worm", 2, new[] {"stunner"}),
                                    new Item("Su-monster", 5, new[] {"tail"}),
                                    new Item("Swamp Elf", 1, new[] {"lilypad"}),
                                    new Item("Tin Dragon", 8, new[] {"*"}),
                                    new Item("Titan", 20, new[] {"sandal"}),
                                    new Item("Trapper", 12, new[] {"shag"}),
                                    new Item("Triceratops", 16, new[] {"horn"}),
                                    new Item("Triton", 3, new[] {"scale"}),
                                    new Item("Troglodyte", 2, new[] {"tail"}),
                                    new Item("Troll", 6, new[] {"hide"}),
                                    new Item("Tyranosauraus Rex", 18, new[] {"forearm"}),
                                    new Item("Umber Hulk", 8, new[] {"claw"}),
                                    new Item("Uruk", 2, new[] {"boot"}),
                                    new Item("Vampire", 8, new[] {"pancreas"}),
                                    new Item("Violet Fungi", 3, new[] {"spore"}),
                                    new Item("Vrock", 8, new[] {"neck"}),
                                    new Item("Wasp", 0, new[] {"stinger"}),
                                    new Item("Whippet", 2, new[] {"collar"}),
                                    new Item("White Dragon", 6, new[] {"tooth"}),
                                    new Item("Wight", 4, new[] {"lung"}),
                                    new Item("Will-o'-the-Wisp", 9, new[] {"wisp"}),
                                    new Item("Wolf", 2, new[] {"paw"}),
                                    new Item("Wolog", 4, new[] {"lemma"}),
                                    new Item("Wraith", 5, new[] {"finger"}),
                                    new Item("Wyvern", 7, new[] {"wing"}),
                                    new Item("Xorn", 7, new[] {"jaw"}),
                                    new Item("Yeenoghu", 25, new[] {"flail"}),
                                    new Item("Yellow Mold", 1, new[] {"spore"}),
                                    new Item("Yeti", 4, new[] {"fur"}),
                                    new Item("Zombie", 2, new[] {"forehead"})

                                 }; 


    public static string[] OffenseAttrib = new[]
                                        {
                                            "Polished|+1",
                                            "Serrated|+1",
                                            "Heavy|+1",
                                            "Pronged|+2",
                                            "Steely|+2",
                                            "Vicious|+3",
                                            "Venomed|+4",
                                            "Stabbity|+4",
                                            "Dancing|+5",
                                            "Invisible|+6",
                                            "Vorpal|+7"
                                        };

    public static string[] OffenseBad = new[]
                                     {
                                         "Dull|-2",
                                         "Tarnished|-1",
                                         "Rusty|-3",
                                         "Padded|-5",
                                         "Bent|-4",
                                         "Mini|-4",
                                         "Rubber|-6",
                                         "Nerf|-7",
                                         "Unbalanced|-2"
                                     }; 

    public static Item[] Races = new[]
                              {
                                  new Item("Half Orc", 0, new[] {"HP Max"}),
                                  new Item("Half Man", 0, new[] {"Cha"}),
                                  new Item("Half Halfling", 0, new[] {"Dex"}),
                                  new Item("Double Hobbit", 0, new[] {"Str"}),
                                  new Item("Hob-Hobbit", 0, new[] {"Dex", "Con"}),
                                  new Item("Low Elf", 0, new[] {"Con"}),
                                  new Item("Dung Elf", 0, new[] {"Wis"}),
                                  new Item("Talking Pony", 0, new[] {"MP Max", "Int"}),
                                  new Item("Gyrognome", 0, new[] {"Dex"}),
                                  new Item("Lesser Dwarf", 0, new[] {"Con"}),
                                  new Item("Crested Dwarf", 0, new[] {"Cha"}),
                                  new Item("Eel Man", 0, new[] {"Dex"}),
                                  new Item("Panda Man", 0, new[] {"Con", "Str"}),
                                  new Item("Trans-Kobold", 0, new[] {"Wis"}),
                                  new Item("Enchanted Motorcycle", 0, new[] {"MP Max"}),
                                  new Item("Will o' the Wisp", 0, new[] {"Wis"}),
                                  new Item("Battle-Finch", 0, new[] {"Dex", "Int"}),
                                  new Item("Double Wookiee", 0, new[] {"Str"}),
                                  new Item("Skraeling", 0, new[] {"Wis"}),
                                  new Item("Demicanadian", 0, new[] {"Con"}),
                                  new Item("Land Squid", 0, new[] {"Str", "HP Max"}),
                                  new Item("Derpellion", 0, new[] {"Cha", "MP Max"})
                              }; 

    private static Random _random = new Random(); // ENCAPSULATE FIELD BY CODEIT.RIGHT

    public static Random RandomValue
    {
        get
        {
            return _random;
        }
    }

    public static Item[] Shields = new[]
                                {
                                    new Item("Parasol", 0),
                                    new Item("Pie Plate", 1),
                                    new Item("Garbage Can Lid", 2),
                                    new Item("Buckler", 3),
                                    new Item("Plexiglass", 4),
                                    new Item("Fender", 4),
                                    new Item("Round Shield", 5),
                                    new Item("Carapace", 5),
                                    new Item("Scutum", 6),
                                    new Item("Propugner", 6),
                                    new Item("Kite Shield", 7),
                                    new Item("Pavise", 8),
                                    new Item("Tower Shield", 9),
                                    new Item("Baroque Shield", 11),
                                    new Item("Aegis", 12),
                                    new Item("Magnetic Field", 18)
                                };


    public static Music music; 

    public static string[] Specials = new[]
                                   {
                                       "Diadem",
                                       "Festoon",
                                       "Gemstone",
                                       "Phial",
                                       "Tiara",
                                       "Scabbard",
                                       "Arrow",
                                       "Lens",
                                       "Lamp",
                                       "Hymnal",
                                       "Fleece",
                                       "Laurel",
                                       "Brooch",
                                       "Gimlet",
                                       "Cobble",
                                       "Albatross",
                                       "Brazier",
                                       "Bandolier",
                                       "Tome",
                                       "Garnet",
                                       "Amethyst",
                                       "Candelabra",
                                       "Corset",
                                       "Sphere",
                                       "Sceptre",
                                       "Ankh",
                                       "Talisman",
                                       "Orb",
                                       "Gammel",
                                       "Ornament",
                                       "Brocade",
                                       "Galoon",
                                       "Bijou",
                                       "Spangle",
                                       "Gimcrack",
                                       "Hood",
                                       "Vulpeculum"
                                   }; 

    public static string[] Spells = new[]
                                 {
                                     "Slime Finger",
                                     "Rabbit Punch",
                                     "Hastiness",
                                     "Good Move",
                                     "Sadness",
                                     "Seasick",
                                     "Gyp",
                                     "Shoelaces",
                                     "Innoculate",
                                     "Cone of Annoyance",
                                     "Magnetic Orb",
                                     "Invisible Hands",
                                     "Revolting Cloud",
                                     "Aqueous Humor",
                                     "Spectral Miasma",
                                     "Clever Fellow",
                                     "Lockjaw",
                                     "History Lesson",
                                     "Hydrophobia",
                                     "Big Sister",
                                     "Cone of Paste",
                                     "Mulligan",
                                     "Nestor's Bright Idea",
                                     "Holy Batpole",
                                     "Tumor (Benign)",
                                     "Braingate",
                                     "Summon a Bitch",
                                     "Nonplus",
                                     "Animate Nightstand",
                                     "Eye of the Troglodyte",
                                     "Curse Name",
                                     "Dropsy",
                                     "Vitreous Humor",
                                     "Roger's Grand Illusion",
                                     "Covet",
                                     "Black Idaho",
                                     "Astral Miasma",
                                     "Spectral Oyster",
                                     "Acrid Hands",
                                     "Angioplasty",
                                     "Grognor's Big Day Off",
                                     "Tumor (Malignant)",
                                     "Animate Tunic",
                                     "Ursine Armor",
                                     "Holy Roller",
                                     "Tonsilectomy",
                                     "Curse Family",
                                     "Infinite Confusion"
                                 };

    public static string[] Titles = new[]
                                 {
                                     "Mr.",
                                     "Mrs.",
                                     "Sir",
                                     "Sgt.",
                                     "Ms.",
                                     "Captain",
                                     "Chief",
                                     "Admiral",
                                     "Saint"
                                 }; 

    public static Item[] Weapons = new[]
                                {
                                    new Item("Stick", 0),
                                    new Item("Broken Bottle", 1),
                                    new Item("Shiv", 1),
                                    new Item("Sprig", 1),
                                    new Item("Oxgoad", 1),
                                    new Item("Eelspear", 2),
                                    new Item("Bowie Knife", 2),
                                    new Item("Clawhammer", 2),
                                    new Item("Handpeen", 2),
                                    new Item("Andiron", 3),
                                    new Item("Hatchet", 3),
                                    new Item("Tomahawk", 3),
                                    new Item("Hackbarm", 3),
                                    new Item("Crowbar", 4),
                                    new Item("Mace", 4),
                                    new Item("Battleadze", 4),
                                    new Item("Leafmace", 5),
                                    new Item("Shortsword", 5),
                                    new Item("Longiron", 5),
                                    new Item("Poachard", 5),
                                    new Item("Baselard", 5),
                                    new Item("Whinyard", 6),
                                    new Item("Blunderbuss", 6),
                                    new Item("Longsword", 6),
                                    new Item("Crankbow", 6),
                                    new Item("Blibo", 7),
                                    new Item("Broadsword", 7),
                                    new Item("Kreen", 7),
                                    new Item("Morning Star", 8),
                                    new Item("Pole-adze", 8),
                                    new Item("Spontoon", 8),
                                    new Item("Bastard Sword", 9),
                                    new Item("Peen-arm", 9),
                                    new Item("Culverin", 10),
                                    new Item("Lance", 10),
                                    new Item("Halberd", 11),
                                    new Item("Poleax", 12),
                                    new Item("Bandyclef", 15)
                                }; 

    private static readonly object syncRoot = new object();

    private static volatile Global instance;

    public static PlayerIndex CurrentPlayer;

#if DEBUG || XBOX
    public static string FriendName = "";
    public static SpynDoctor.TopScoreListContainer Scores;
    public static bool SyncComplete = false;
    public static SpynDoctor.OnlineDataSyncManager SyncManager;
#endif

    public enum Stat
    {
        Str = 0,
        Con,
        Dex,
        Int,
        Wis,
        Cha,
        [Description("HP Max")] HpMax,
        [DescriptionAttribute("MP Max")] MpMax
    }

    public enum Trait
    {
        Name = 0,
        Race,
        Class,
        Level
    }

    public static Global Instance
    {
        get
        {
            if (instance == null)
            {
                lock (syncRoot)
                    if (instance == null)
                        instance = new Global();
            }
            return instance;
        }
    }

    public static int GetEnumCount<T>()
    {
        string[] t = GetEnumNames<T>();
        return t.Length;
    }

    public static string GetEnumDescription(object value)
    {
        string retVal = "";
        try
        {
            FieldInfo fieldInfo = value.GetType().GetField(value.ToString());
            var attributes =
                (DescriptionAttribute[]) fieldInfo.GetCustomAttributes(typeof(DescriptionAttribute), false);
            retVal =
                ((attributes.Length > 0) ? attributes[0].Description : value.ToString());
        }
        catch (NullReferenceException)
        {
            // Occurs when we attempt to get description of an enum value that does not exist
            retVal = (string) value;
        }
        return retVal;
    }

    public static string[] GetEnumNames<T>()
    {
        IEnumerable<T> tx = (from x in typeof (T).GetFields(BindingFlags.Static | BindingFlags.Public)
                             select (T) x.GetValue(null));
        List<T> rx = tx.ToList();
        var s = new List<string>();
        foreach (T t in rx)
            s.Add(GetEnumDescription(t));
        return s.ToArray();
    }

    public static void saveDeviceSaveCompleted(object sender, FileActionCompletedEventArgs args)
    {
        // just write some debug output for our verification
        //    Debug.WriteLine("SaveCompleted!");
    }

    public static string GenerateName()
    {
        string[] Temp1 = "br|cr|dr|fr|gr|j|kr|l|m|n|pr||||r|sh|tr|v|wh|x|y|z".Split('|');
        string[] Temp2 = "a|a|e|e|i|i|o|o|u|u|ae|ie|oo|ou".Split('|');
        string[] Temp3 = "b|ck|d|g|k|m|n|p|t|v|x|z".Split('|');
        object[] Parts = new object[3] { Temp1, Temp2, Temp3 };
        string result = null;
        for (int i = 0; i <= 5; ++i)
            result += Pick((string[])Parts[i%3]);
        return char.ToUpper(result[0], CultureInfo.CurrentCulture) + result.Substring(1);
    }

    public static void LoadSettings()
    {
         // make sure the device is ready
            if (Global.SaveDevice.IsReady)
            {
                if (Global.SaveDevice.FileExists("LevelUp", "settings.cfg"))
                {
                    Global.SaveDevice.Load(
                    "LevelUp",
                    "settings.cfg",
                    stream =>
                    {
                        configFile = new EasyConfig.ConfigFile(stream);
                    });
                }
                else
                {
                    configFile = new EasyConfig.ConfigFile("Content/defaultsettings.ini");
                }
            }
            MediaPlayer.Volume = configFile.SettingGroups["Audio"].Settings["Volume"].GetValueAsFloat();
            MediaPlayer.IsMuted = configFile.SettingGroups["Audio"].Settings["Mute"].GetValueAsBool();
            MenuStyle = configFile.SettingGroups["Menu"].Settings["Style"].GetValueAsInt();
            try
            {
                Tutorial = configFile.SettingGroups["Menu"].Settings["Tutorial"].GetValueAsBool();
            }
            catch (Exception e)
            {
                Tutorial = true;
                configFile.SettingGroups["Menu"].AddSetting("Tutorial", true);
            }
            SaveSettings();
    }

    public static void SaveSettings()
    {

        configFile.SettingGroups["Audio"].Settings["Volume"].SetValue(MediaPlayer.Volume);
        configFile.SettingGroups["Audio"].Settings["Mute"].SetValue(MediaPlayer.IsMuted);
        configFile.SettingGroups["Menu"].Settings["Style"].SetValue(MenuStyle);
        configFile.SettingGroups["Menu"].Settings["Tutorial"].SetValue(Tutorial);
        
        try
        {
            // make sure the device is ready
            if (Global.SaveDevice.IsReady)
            {
                // save a file asynchronously. this will trigger IsBusy to return true
                // for the duration of the save process.
                Global.SaveDevice.SaveAsync(
                    "LevelUp",
                    "settings.cfg",
                    stream =>
                    {
                        configFile.Save(stream);
                    });
            }
        }
         catch (Exception e)
        {
            Debug.WriteLine(e.Message);
        }
    }

    public static void SaveScores(bool mChanged)
    {
#if DEBUG || XBOX
        if (mChanged)
        {
            // make sure the device is ready
            if (Global.SaveDevice.IsReady)
            {
                // save a file asynchronously. this will trigger IsBusy to return true
                // for the duration of the save process.
                Global.SaveDevice.SaveAsync(
                    "LevelUp",
                    "Scores.cfg",
                    stream =>
                    {
                        BinaryWriter writer = new BinaryWriter(stream);
                        Global.Scores.Save(writer);
                        writer.Close();
                    });
            }
            mChanged = false;
        }
#endif
    }

    public static object Pick(object[] array)
    {
        return array[RandomValue.Next(0, array.Length)];
    }

    public static void CreateScoresTable()
    {
#if DEBUG || XBOX
        // if (!Guide.IsTrialMode && mPlayer != null && mPlayer.IsSignedInToLive && mPlayer.Privileges.AllowOnlineSessions)

        // save a file asynchronously. this will trigger IsBusy to return true
        // for the duration of the save process.
        if (SaveDevice.FileExists("LevelUp", "Scores.cfg"))
        {
            SaveDevice.Load(
                "LevelUp",
                "Scores.cfg",
                stream =>
                {
                    BinaryReader reader = new BinaryReader(stream);
                    Scores = new SpynDoctor.TopScoreListContainer(reader);
                    reader.Close();
                });
        }
        else
        {
            Scores = new SpynDoctor.TopScoreListContainer(1, 100);
        }
        SyncManager.Start(Gamer.SignedInGamers[CurrentPlayer], Scores);
#endif

    }
    
}