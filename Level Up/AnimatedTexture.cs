//-------------------------------------------------------------------------------------------------
// <copyright file="C:\proj\levelup\Level Up\AnimatedTexture.cs" company="John Goewert">
// Copyright (c) John Goewert.  All rights reserved.
// </copyright>
//-------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------
// AnimatedTexture.cs
//
// Microsoft XNA Community Game Platform
// Copyright (C) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------

using System;
using System.Collections.Generic;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

public class AnimatedTexture
{
        public int Height;
        public Vector2 Origin;
        public Rectangle rect;
        public float Scale, Depth;
        public Texture2D texture;
        public int Width;

        private int Frame;
        private int framecount;
        private bool Paused;
        private float TimePerFrame;
        private float TotalElapsed;

        public AnimatedTexture(Vector2 origin, float rotation,
                               float scale, float depth)
        {
            this.Origin = origin;
            //this.Rotation = rotation;
            this.Scale = scale;
            this.Depth = depth;
        }

        public bool IsPaused
        {
            get { return this.Paused; }
        }

        // class AnimatedTexture
        public void DrawFrame(SpriteBatch batch, Vector2 screenPos, float rotation)
        {
            this.DrawFrame(batch, Frame, screenPos, rotation);
        }

        public void DrawFrame(SpriteBatch batch, int frame, Vector2 screenPos, float rotation)
        {
            int FrameWidth = this.texture.Width / framecount;
            Rectangle sourcerect = new Rectangle(FrameWidth * frame, 0,
                                                 FrameWidth, this.texture.Height);
            batch.Draw(this.texture, screenPos, sourcerect, Color.White,
                       rotation, this.Origin, Scale, SpriteEffects.None, Depth);
        }

        public void Load(ContentManager content, string asset,
                         int frameCount, int framesPerSec)
        {
            this.framecount = frameCount;
            this.texture = content.Load<Texture2D>(asset);
            this.TimePerFrame = (float)1 / framesPerSec;
            this.Frame = 0;
            this.TotalElapsed = 0;
            this.Paused = false;
            this.Origin = new Vector2((this.texture.Width / 2) / frameCount, this.texture.Height / 2);
            this.Width = this.texture.Width / frameCount;
            this.Height = this.texture.Height;
        }

        public void Pause()
        {
            this.Paused = true;
        }

        public void Play()
        {
            this.Paused = false;
        }

        public void Reset()
        {
            this.Frame = 0;
            this.TotalElapsed = 0f;
        }

        public void Stop()
        {
            this.Pause();
            this.Reset();
        }

        // class AnimatedTexture
        public void UpdateFrame(float elapsed)
        {
            if (this.Paused)
            {
                return;
            }
            this.TotalElapsed += elapsed;
            if (this.TotalElapsed > TimePerFrame)
            {
                this.Frame++;
                // Keep the Frame between 0 and the total frames, minus one.
                this.Frame = this.Frame % framecount;
                this.TotalElapsed -= TimePerFrame;
            }
        }
}