#region Header
// --------------------------------
// <copyright file="Game1.cs" company="John Goewert">
//     Copyright John Goewert 2011.
// </copyright>
// <author>John Goewert</author>
// ---------------------------------
#endregion


using System;
using System.Diagnostics;
using GameStateManagement;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Content;
#if DEBUG || XBOX
using Microsoft.Xna.Framework.GamerServices;
#endif
using OrbUI;
using OrbUI.Components;
using OrbUI.Components.GUI;
using OrbUI.Components.GUI.Controls;
using OrbUI.Components.Input.Controller;
using OrbUI.Exceptions;


namespace Level_Up
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Game
    {
        #region Fields

        // By preloading any assets used by UI rendering, we avoid framerate glitches
        // when they suddenly need to be loaded in the middle of a menu transition.
        private static readonly string[] preloadAssets =
            {
                "gradient",
            };

        private Player player = Player.Instance;

        private readonly ScreenManager screenManager;

        #endregion Fields

        #region Constructors

        public Game1()
        {
            Content.RootDirectory = "Content";
            
            Global.Graphics = new GraphicsDeviceManager(this);
            Global.Graphics.PreferredBackBufferWidth = 1280;
            Global.Graphics.PreferredBackBufferHeight = 720;
            Global.Graphics.SynchronizeWithVerticalRetrace = true;
            // graphics.IsFullScreen = true;

#if DEBUG || XBOX
            Components.Add(new Microsoft.Xna.Framework.GamerServices.GamerServicesComponent(this));

            SignedInGamer.SignedIn += new EventHandler<SignedInEventArgs>(SignedInGamerSignedIn);
            SignedInGamer.SignedOut += new EventHandler<SignedOutEventArgs>(SignedInGamerSignedOut);

            Global.SyncManager = new SpynDoctor.OnlineDataSyncManager(0, this);
            Components.Add(Global.SyncManager);
#endif
            // Create the screen manager component.
            screenManager = new ScreenManager(this);
            Components.Add(screenManager);
            
            Global.content = new ContentManager(screenManager.Game.Services, "Content");
            
            // Activate the first screens.
            LoadingScreen.Load(screenManager, false, null, new CreditsScreen());

            MediaPlayer.IsMuted = true;
        }

        #endregion Constructors

        #region Methods

        protected override void Dispose(bool disposing)
        {
            try
            {
                if (OrbMaster.Initialized && disposing)
                    OrbMaster.Dispose();
            }
            catch (NotInitializedException e)
            {
                Debug.WriteLine(e.Message);
            }
            catch (ObjectDisposedException e)
            {
                Debug.WriteLine(e.Message);
            }
            base.Dispose(disposing);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        protected override void Draw(GameTime gameTime)
        {
            screenManager.GraphicsDevice.Clear(Color.Black);

            // The real drawing happens inside the screen manager component.
            base.Draw(gameTime);
        }

        protected override void EndRun()
        {
#if DEBUG || XBOX
            if ((Global.SyncManager.Enabled == true) && !Global.SyncManager.IsStopping())
                Global.SyncManager.Stop(delegate() { this.Exit(); }, true);
#endif
            base.EndRun();
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            base.Initialize();

            screenManager.GraphicsDevice.BlendState = BlendState.AlphaBlend;
            screenManager.GraphicsDevice.DepthStencilState = DepthStencilState.None;
            screenManager.GraphicsDevice.RasterizerState = RasterizerState.CullCounterClockwise;
            screenManager.GraphicsDevice.SamplerStates[0] = SamplerState.LinearClamp;
            Global.Graphics.ApplyChanges();

            // initialize OrbUI
            OrbValidation.Name = "John Goewert";
            OrbValidation.Code =
                OrbValidation.Code =
                new byte[]
                    {
                        152, 250, 136, 153, 055, 158, 158, 158, 158, 155, 139, 155, 155, 174, 203, 158, 203, 000, 214, 129,
                        086, 149, 003, 041, 168, 003, 248, 232, 240, 092, 027, 081, 049, 169, 000, 089, 120, 064, 008, 244,
                        232, 250, 082, 254, 212, 116, 030, 088, 088, 088, 088, 168, 172, 168, 168, 161, 222, 130, 121, 078,
                        188, 191, 192, 066, 212, 194, 084, 000
                    };

            // initialize OrbUI's skin
            InitializeResources();

            OrbMaster.Initialize(this, screenManager.GraphicsDevice, Global.Graphics);

            // for optimizing speed while debugging, allow StencilHelper to collect stenciled Controls
            StencilHelper.CollectStenciledControls = true;

            IsMouseVisible = false;
        }

        /// <summary>
        /// Loads graphics content.
        /// </summary>
        protected override void LoadContent()
        {
            foreach (string asset in preloadAssets)
            {
                Content.Load<object>(asset);
            }

            Global.music = new Music(Content);
            Global.music.LoadContent();
        }

        protected override void OnExiting(Object sender, EventArgs args)
        {
            base.OnExiting(sender, args);
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            try
            {    
        
            // First, update CursorState, which will update the cursor's positions and button states.
            CursorState.Update(gameTime);

            // Second, update OrbFormCollection, which will update all of the Controls.
            if (OrbFormCollection.Initialized)
                OrbFormCollection.Update(gameTime);
            // Third, update OrbListManager, which will release unused resources periodically.
            if (OrbListManager.Initialized)
                OrbListManager.Update(gameTime);
            }
            catch (NotInitializedException e)
            {
            }
            
            Global.music.Update();
        }

        private static void InitializeResources()
        {
            #region initialize OrbUI resources

            const string g_sFontName_Gamefont = "gamefont";

#if !XBOX
            OrbResources.OrbLocalizedKeyboard_DefaultCultureName = "en-US";
#endif
            OrbResources.OrbContentManager_RootDirectory = "Content";
            OrbResources.Control_FontName = g_sFontName_Gamefont;
            OrbResources.CursorState_LimitToCursorBoundary = true;
            OrbResources.OrbResourceManager_GifDecoder_SubFolderPath = string.Empty;
            OrbResources.OrbResourceManager_SpriteFont_SubFolderPath = "fonts\\";
            OrbResources.OrbResourceManager_Texture_SubFolderPath = "";
            OrbResources.OrbResourceManager_Texture2D_SubFolderPath = "";

            OrbResources.Control_Button_Backdrop_BackColor = Color.White;
            OrbResources.Control_Button_Backdrop_BackgroundTextureName = "";
            OrbResources.Control_Button_Backdrop_BorderColor = Color.Black;
            OrbResources.Control_Button_Backdrop_BorderWidth = 1;
            OrbResources.Control_Button_Backdrop_BottomLeftTextureName = "UI\\Button_BottomLeft";
            OrbResources.Control_Button_Backdrop_BottomMiddleTextureName = "UI\\Button_BottomMiddle";
            OrbResources.Control_Button_Backdrop_BottomRightTextureName = "UI\\Button_BottomRight";
            OrbResources.Control_Button_Backdrop_DrawBackground = false;
            OrbResources.Control_Button_Backdrop_DrawBackgroundImage = false;
            OrbResources.Control_Button_Backdrop_DrawBorder = false;
            OrbResources.Control_Button_Backdrop_DrawTextureLayout = true;
            OrbResources.Control_Button_Backdrop_InstanceName = "{0}_Button_Backdrop";
            OrbResources.Control_Button_Backdrop_MiddleLeftTextureName = "UI\\Button_MiddleLeft";
            OrbResources.Control_Button_Backdrop_MiddleRightTextureName = "UI\\Button_MiddleRight";
            OrbResources.Control_Button_Backdrop_MiddleTextureName = "UI\\Button_Middle";
            OrbResources.Control_Button_Backdrop_TopLeftTextureName = "UI\\Button_TopLeft";
            OrbResources.Control_Button_Backdrop_TopMiddleTextureName = "UI\\Button_TopMiddle";
            OrbResources.Control_Button_Backdrop_TopRightTextureName = "UI\\Button_TopRight";
            OrbResources.Control_Button_Backdrop_Visible = true;
            OrbResources.Control_Button_BorderWidth = 1;
            OrbResources.Control_Button_DefaultHeight = 35;
            OrbResources.Control_Button_DefaultWidth = 100;
            OrbResources.Control_Button_DisabledColor = new Color(150, 150, 150, 255);
            OrbResources.Control_Button_FontName = string.Empty;
            OrbResources.Control_Button_MouseDownColor = Color.Silver;
            OrbResources.Control_Button_MouseDownTextColor = Color.Black;
            OrbResources.Control_Button_MouseOverColor = Color.LightGray;
            OrbResources.Control_Button_MouseOverTextColor = Color.Black;
            OrbResources.Control_Button_OrbHorizontalTextAlign = HorizontalTextAlignment.Center;
            OrbResources.Control_Button_OrbTextFormatting = TextFormattings.Ellipse;
            OrbResources.Control_Button_OrbVerticalTextAlign = VerticalTextAlignment.Middle;
            OrbResources.Control_ButtonGroup_DeselectedColorTint = new Color(200, 200, 200, 255);
            OrbResources.Control_ButtonGroup_MouseHoverColor = new Color(220, 220, 220, 255);
            OrbResources.Control_ButtonGroup_SelectedColorTint = new Color(255, 255, 255, 255);
            OrbResources.Control_ButtonRow_BorderWidth = 2;
            OrbResources.Control_ButtonRow_DimColor = new Color(217, 217, 217, 255);
            OrbResources.Control_ButtonRow_FontName = string.Empty;
            OrbResources.Control_ButtonRow_HighlightColor = new Color(230, 230, 230, 255);
            OrbResources.Control_ButtonRow_SelectedColor = Color.White;
            OrbResources.Control_ButtonRow_TextColor = Color.Black;
            OrbResources.Control_ButtonRow_TextureName = @"UI\ButtonRow_Button";
            OrbResources.Control_CheckBox_Backdrop_BackColor = Color.White;
            OrbResources.Control_CheckBox_Backdrop_BackgroundTextureName = "";
            OrbResources.Control_CheckBox_Backdrop_BorderColor = Color.Black;
            OrbResources.Control_CheckBox_Backdrop_BorderWidth = 1;
            OrbResources.Control_CheckBox_Backdrop_BottomLeftTextureName = "";
            OrbResources.Control_CheckBox_Backdrop_BottomMiddleTextureName = "";
            OrbResources.Control_CheckBox_Backdrop_BottomRightTextureName = "";
            OrbResources.Control_CheckBox_Backdrop_DrawBackground = false;
            OrbResources.Control_CheckBox_Backdrop_DrawBackgroundImage = false;
            OrbResources.Control_CheckBox_Backdrop_DrawBorder = false;
            OrbResources.Control_CheckBox_Backdrop_DrawTextureLayout = false;
            OrbResources.Control_CheckBox_Backdrop_InstanceName = "{0}_CheckBox_Backdrop";
            OrbResources.Control_CheckBox_Backdrop_MiddleLeftTextureName = "";
            OrbResources.Control_CheckBox_Backdrop_MiddleRightTextureName = "";
            OrbResources.Control_CheckBox_Backdrop_MiddleTextureName = "";
            OrbResources.Control_CheckBox_Backdrop_TopLeftTextureName = "";
            OrbResources.Control_CheckBox_Backdrop_TopMiddleTextureName = "";
            OrbResources.Control_CheckBox_Backdrop_TopRightTextureName = "";
            OrbResources.Control_CheckBox_Backdrop_Visible = false;
            OrbResources.Control_CheckBox_BorderWidth = 2;
            OrbResources.Control_CheckBox_CanUncheck = true;
            OrbResources.Control_CheckBox_CheckedTextureName = @"UI\CheckBox_Checked";
            OrbResources.Control_CheckBox_FontName = string.Empty;
            OrbResources.Control_CheckBox_Label_HorizontalTextAlign = HorizontalTextAlignment.Left;
            OrbResources.Control_CheckBox_Label_InstanceName = "{0}_Label";
            OrbResources.Control_CheckBox_Label_TextColor = Color.White;
            OrbResources.Control_CheckBox_Label_TextFormatting = TextFormattings.Ellipse;
            OrbResources.Control_CheckBox_Label_VerticalTextAlign = VerticalTextAlignment.Middle;
            OrbResources.Control_CheckBox_OverlayColor = Color.DarkGray;
            OrbResources.Control_CheckBox_UncheckedTextureName = @"UI\CheckBox_Unchecked";
            OrbResources.Control_CheckBoxGroup_CanUncheck = true;
            OrbResources.Control_ComboBox_Button_Backdrop_BackgroundTextureName = @"UI\ComboBox_Button";
            OrbResources.Control_ComboBox_ButtonName = "{0}_ComboBox_Button";
            OrbResources.Control_ComboBox_FontName = string.Empty;
            OrbResources.Control_ComboBox_ListBoxItemCount = 6;
            OrbResources.Control_ComboBox_ListboxName = "{0}_ComboBox_ListBox";
            OrbResources.Control_ComboBox_TextBoxName = "{0}_ComboBox_TextBox";
            OrbResources.Control_Form_Backdrop_BottomLeftTextureName = @"UI\Form_BottomLeft";
            OrbResources.Control_Form_Backdrop_BottomMiddleTextureName = @"UI\Form_BottomMiddle";
            OrbResources.Control_Form_Backdrop_BottomRightTextureName = @"UI\Form_BottomRight";
            OrbResources.Control_Form_Backdrop_BottomRightTextureName_Resize = @"UI\Form_BottomRight_Resize";
            OrbResources.Control_Form_Backdrop_InstanceName = "{0}_frmBorder";
            OrbResources.Control_Form_Backdrop_MiddleLeftTextureName = @"UI\Form_MiddleLeft";
            OrbResources.Control_Form_Backdrop_MiddleRightTextureName = @"UI\Form_MiddleRight";
            OrbResources.Control_Form_Backdrop_MiddleTextureName = @"UI\antique-wood-texture";
            OrbResources.Control_Form_Backdrop_TopLeftTextureName = @"UI\Form_TopLeft";
            OrbResources.Control_Form_Backdrop_TopMiddleTextureName = @"UI\Form_TopMiddle";
            OrbResources.Control_Form_Backdrop_TopRightTextureName = @"UI\Form_TopRight";
            OrbResources.Control_Form_BottomBoundaryOffset = 10;
            OrbResources.Control_Form_CloseButton_Name = "{0}_btClose";
            OrbResources.Control_Form_CloseButton_Size = new Point(20, 20);
            OrbResources.Control_Form_CloseButton_TextureName = @"UI\Header_CloseButton";
            OrbResources.Control_Form_DragAnywhere = true;
            OrbResources.Control_Form_DragAnywhereFallThroughTypes.Add(typeof(FPSCounter));
            OrbResources.Control_Form_DragAnywhereFallThroughTypes.Add(typeof(Label));
            OrbResources.Control_Form_DragAnywhereFallThroughTypes.Add(typeof(PictureBox));
            OrbResources.Control_Form_FontName = string.Empty;
            OrbResources.Control_Form_TitleTextColor = Color.White; // JCG
            OrbResources.Control_Form_HeaderButtonOffset = 10;
            OrbResources.Control_Form_HeaderButtonSpacing = 4;
            OrbResources.Control_Form_Icon_Position = new Point(6, 4);
            OrbResources.Control_Form_Icon_Size = new Point(20, 20);
            OrbResources.Control_Form_Icon_TextureName = @"UI\Form_Icon";
            OrbResources.Control_Form_LeftBoundaryOffset = 5;
            OrbResources.Control_Form_MaximizeButton_Name = "{0}_btMaximize";
            OrbResources.Control_Form_MaximizeButton_Size = new Point(20, 20);
            OrbResources.Control_Form_MaximizeButton_TextureName = @"UI\Header_MaximizeButton";
            OrbResources.Control_Form_MinimizeButton_Name = "{0}_btMinimize";
            OrbResources.Control_Form_MinimizeButton_Size = new Point(20, 20);
            OrbResources.Control_Form_MinimizeButton_TextureName = @"UI\Header_MinimizeButton";
            OrbResources.Control_Form_MinimizedAllowMove = true;
            OrbResources.Control_Form_MinimizedSize = new Point(400, 32);
            OrbResources.Control_Form_ResizeAreaSize = new Point(15, 15);
            OrbResources.Control_Form_RestoreButton_Name = "{0}_btRestore";
            OrbResources.Control_Form_RestoreButton_Size = new Point(20, 20);
            OrbResources.Control_Form_RestoreButton_TextureName = @"UI\Header_RestoreButton";
            OrbResources.Control_Form_RightBoundaryOffset = 5;
            OrbResources.Control_Form_TitleTextColor = Color.Black;
            OrbResources.Control_Form_TopBoundaryOffset = 20;
            OrbResources.Control_FPSCounter_Text = "FPS: {0}";
            OrbResources.Control_HorizontalScrollBar_BackgroundTextureName = @"UI\HorizontalScrollBar_Background";
            OrbResources.Control_HorizontalScrollBar_CursorLeftWidth = 6;
            OrbResources.Control_HorizontalScrollBar_CursorMinimumWidth = 13;
            OrbResources.Control_HorizontalScrollBar_CursorRightWidth = 7;
            OrbResources.Control_HorizontalScrollBar_CursorTextureName = @"UI\HorizontalScrollBar_Cursor";
            OrbResources.Control_HorizontalScrollBar_Height = 12;
            OrbResources.Control_HorizontalScrollBar_ButtonImageName = @"UI\HorizontalScrollBar_Button";
            OrbResources.Control_HorizontalScrollBar_LeftButtonName = "{0}_btLeft";
            OrbResources.Control_HorizontalScrollBar_OverlayColor = Color.White;
            OrbResources.Control_HorizontalScrollBar_RightButtonName = "{0}_btRight";
            OrbResources.Control_HorizontalScrollBar_ScrollRepeatDelay = 5;
            OrbResources.Control_HorizontalScrollBar_ScrollRepeatInitialDelay = 500;
            OrbResources.Control_Label_Backdrop_BackColor = Color.White;
            OrbResources.Control_Label_Backdrop_BackgroundTextureName = "";
            OrbResources.Control_Label_Backdrop_BorderColor = Color.Black;
            OrbResources.Control_Label_Backdrop_BorderWidth = 0;
            OrbResources.Control_Label_Backdrop_BottomLeftTextureName = "";
            OrbResources.Control_Label_Backdrop_BottomMiddleTextureName = "";
            OrbResources.Control_Label_Backdrop_BottomRightTextureName = "";
            OrbResources.Control_Label_Backdrop_DrawBackground = false;
            OrbResources.Control_Label_Backdrop_DrawBackgroundImage = false;
            OrbResources.Control_Label_Backdrop_DrawBorder = false;
            OrbResources.Control_Label_Backdrop_DrawTextureLayout = false;
            OrbResources.Control_Label_Backdrop_InstanceName = "{0}_Label_Backdrop";
            OrbResources.Control_Label_Backdrop_MiddleLeftTextureName = "";
            OrbResources.Control_Label_Backdrop_MiddleRightTextureName = "";
            OrbResources.Control_Label_Backdrop_MiddleTextureName = "";
            OrbResources.Control_Label_Backdrop_TopLeftTextureName = "";
            OrbResources.Control_Label_Backdrop_TopMiddleTextureName = "";
            OrbResources.Control_Label_Backdrop_TopRightTextureName = "";
            OrbResources.Control_Label_Backdrop_Visible = false;
            OrbResources.Control_Label_BorderWidth = 0;
            OrbResources.Control_Label_FontName = string.Empty;
            OrbResources.Control_ListBox_Backdrop_BackColor = Color.Black;
            OrbResources.Control_ListBox_Backdrop_BackgroundTextureName = "";
            OrbResources.Control_ListBox_Backdrop_BorderColor = Color.Black;
            OrbResources.Control_ListBox_Backdrop_BorderWidth = 1;
            OrbResources.Control_ListBox_Backdrop_BottomLeftTextureName = "UI\\ListBox_BottomLeft";
            OrbResources.Control_ListBox_Backdrop_BottomMiddleTextureName = "UI\\ListBox_BottomMiddle";
            OrbResources.Control_ListBox_Backdrop_BottomRightTextureName = "UI\\ListBox_BottomRight";
            OrbResources.Control_ListBox_Backdrop_DrawBackground = false;
            OrbResources.Control_ListBox_Backdrop_DrawBackgroundImage = false;
            OrbResources.Control_ListBox_Backdrop_DrawBorder = false;
            OrbResources.Control_ListBox_Backdrop_DrawTextureLayout = true;
            OrbResources.Control_ListBox_Backdrop_InstanceName = "{0}_ListBox_Backdrop";
            OrbResources.Control_ListBox_Backdrop_MiddleLeftTextureName = "UI\\ListBox_MiddleLeft";
            OrbResources.Control_ListBox_Backdrop_MiddleRightTextureName = "UI\\ListBox_MiddleRight";
            OrbResources.Control_ListBox_Backdrop_MiddleTextureName = "UI\\ListBox_Middle";
            OrbResources.Control_ListBox_Backdrop_TopLeftTextureName = "UI\\ListBox_TopLeft";
            OrbResources.Control_ListBox_Backdrop_TopMiddleTextureName = "UI\\ListBox_TopMiddle";
            OrbResources.Control_ListBox_Backdrop_TopRightTextureName = "UI\\ListBox_TopRight";
            OrbResources.Control_ListBox_Backdrop_Visible = true;
            OrbResources.Control_ListBox_FontName = string.Empty;
            OrbResources.Control_ListBox_HorizontalScrollBarName = "{0}_ListBox_HScrollBar";
            OrbResources.Control_ListBox_ItemHoverColor = Color.Silver;
            OrbResources.Control_ListBox_ItemSelectedColor = Color.LightGray;
            OrbResources.Control_ListBox_TextColor = Color.White;
            OrbResources.Control_ListBox_VerticalScrollBarName = "{0}_ListBox_VScrollBar";
            OrbResources.Control_ListView_Backdrop_BackColor = Color.Black;
            OrbResources.Control_ListView_Backdrop_BackgroundTextureName = "UI\\ListView_Header_Middle";
            OrbResources.Control_ListView_Backdrop_BorderColor = Color.Gray;
            OrbResources.Control_ListView_Backdrop_BorderWidth = 1;
            OrbResources.Control_ListView_Backdrop_BottomLeftTextureName = "UI\\ListView_BottomLeft";
            OrbResources.Control_ListView_Backdrop_BottomMiddleTextureName = "UI\\ListView_BottomMiddle";
            OrbResources.Control_ListView_Backdrop_BottomRightTextureName = "UI\\ListView_BottomRight";
            OrbResources.Control_ListView_Backdrop_DrawBackground = true;
            OrbResources.Control_ListView_Backdrop_DrawBackgroundImage = true;
            OrbResources.Control_ListView_Backdrop_DrawBorder = true;
            OrbResources.Control_ListView_Backdrop_DrawTextureLayout = true;
            OrbResources.Control_ListView_Backdrop_InstanceName = "{0}_ListView_Backdrop";
            OrbResources.Control_ListView_Backdrop_MiddleLeftTextureName = "UI\\ListView_MiddleLeft";
            OrbResources.Control_ListView_Backdrop_MiddleRightTextureName = "UI\\ListView_MiddleRight";
            OrbResources.Control_ListView_Backdrop_MiddleTextureName = "UI\\ListView_Middle";
            OrbResources.Control_ListView_Backdrop_TopLeftTextureName = "UI\\ListView_TopLeft";
            OrbResources.Control_ListView_Backdrop_TopMiddleTextureName = "UI\\ListView_TopMiddle";
            OrbResources.Control_ListView_Backdrop_TopRightTextureName = "UI\\ListView_TopRight";
            OrbResources.Control_ListView_Backdrop_Visible = true;
            OrbResources.Control_ListView_BorderWidth = 1;
            OrbResources.Control_ListView_FontName = string.Empty;
            OrbResources.Control_ListView_Header_Gradient_Colors_CursorDown = new[] { Color.LightGray, Color.LightGray };
            OrbResources.Control_ListView_Header_Gradient_Colors_CursorEnter = new[] { Color.LightGray, Color.LightGray };
            OrbResources.Control_ListView_Header_Gradient_Colors_CursorNormal = new[] { Color.LightGray, Color.LightGray };
            OrbResources.Control_ListView_Header_Gradient_Percents_CursorDown = new[] { 40, 50 };
            OrbResources.Control_ListView_Header_Gradient_Percents_CursorEnter = new[] { 40, 50 };
            OrbResources.Control_ListView_Header_Gradient_Percents_CursorNormal = new[] { 40, 50 };
            OrbResources.Control_ListView_Header_MouseHover_Border_OverlayColor = Color.Black;
            OrbResources.Control_ListView_Header_MouseHover_OverlayColor = Color.Black;
            OrbResources.Control_ListView_HoverColor = new Color(220, 220, 220);
            OrbResources.Control_ListView_HScrollBarName = "{0}_ListView_HScrollBar";
            OrbResources.Control_ListView_ItemColumnMargin = 10;
            OrbResources.Control_ListView_SelectedColor = Color.LightGray;
            OrbResources.Control_ListView_SortArrow_TextureName = "UI\\ListView_SortArrow";
            OrbResources.Control_ListView_TextColor = Color.White;
            OrbResources.Control_ListView_VScrollBarName = "{0}_ListView_VScrollBar";
            OrbResources.Control_MainMenu_AmpersandNavigationIndicator = '&';
            OrbResources.Control_MainMenu_BackColor = Color.Black;
            OrbResources.Control_MainMenu_Backdrop_BackColor = Color.Gray;
            OrbResources.Control_MainMenu_Backdrop_BackgroundTextureName = "";
            OrbResources.Control_MainMenu_Backdrop_BorderColor = Color.Black;
            OrbResources.Control_MainMenu_Backdrop_BorderWidth = 1;
            OrbResources.Control_MainMenu_Backdrop_BottomLeftTextureName = "";
            OrbResources.Control_MainMenu_Backdrop_BottomMiddleTextureName = "";
            OrbResources.Control_MainMenu_Backdrop_BottomRightTextureName = "";
            OrbResources.Control_MainMenu_Backdrop_DrawBackground = false;
            OrbResources.Control_MainMenu_Backdrop_DrawBackgroundImage = false;
            OrbResources.Control_MainMenu_Backdrop_DrawBorder = true;
            OrbResources.Control_MainMenu_Backdrop_DrawTextureLayout = false;
            OrbResources.Control_MainMenu_Backdrop_InstanceName = "{0}_MainMenu_Backdrop";
            OrbResources.Control_MainMenu_Backdrop_MiddleLeftTextureName = "";
            OrbResources.Control_MainMenu_Backdrop_MiddleRightTextureName = "";
            OrbResources.Control_MainMenu_Backdrop_MiddleTextureName = "";
            OrbResources.Control_MainMenu_Backdrop_TopLeftTextureName = "";
            OrbResources.Control_MainMenu_Backdrop_TopMiddleTextureName = "";
            OrbResources.Control_MainMenu_Backdrop_TopRightTextureName = "";
            OrbResources.Control_MainMenu_Backdrop_Visible = true;
            OrbResources.Control_MainMenu_BorderWidth = 1;
            OrbResources.Control_MainMenu_FontName = g_sFontName_Gamefont;
            OrbResources.Control_MainMenu_MenuItem_MouseDown_OverlayColor = new Color(160, 160, 160, 255);
            OrbResources.Control_MainMenu_MenuItem_MouseHover_OverlayColor = new Color(180, 180, 180, 255);
            OrbResources.Control_MainMenu_MenuItem_Selected_OverlayColor = new Color(200, 200, 200, 255);
            OrbResources.Control_MenuItem_ArrowOverlayColor = Color.Black;
            OrbResources.Control_MenuItem_ArrowTextureName = @"UI\\Right_Arrow";
            OrbResources.Control_MenuItem_Backdrop_BackColor = Color.White;
            OrbResources.Control_MenuItem_Backdrop_BackgroundTextureName = "";
            OrbResources.Control_MenuItem_Backdrop_BorderColor = Color.Black;
            OrbResources.Control_MenuItem_Backdrop_BorderWidth = 1;
            OrbResources.Control_MenuItem_Backdrop_BottomLeftTextureName = "";
            OrbResources.Control_MenuItem_Backdrop_BottomMiddleTextureName = "";
            OrbResources.Control_MenuItem_Backdrop_BottomRightTextureName = "";
            OrbResources.Control_MenuItem_Backdrop_DrawBackground = false;
            OrbResources.Control_MenuItem_Backdrop_DrawBackgroundImage = false;
            OrbResources.Control_MenuItem_Backdrop_DrawBorder = true;
            OrbResources.Control_MenuItem_Backdrop_DrawTextureLayout = false;
            OrbResources.Control_MenuItem_Backdrop_InstanceName = "{0}_MenuItem_Backdrop";
            OrbResources.Control_MenuItem_Backdrop_MiddleLeftTextureName = "";
            OrbResources.Control_MenuItem_Backdrop_MiddleRightTextureName = "";
            OrbResources.Control_MenuItem_Backdrop_MiddleTextureName = "";
            OrbResources.Control_MenuItem_Backdrop_TopLeftTextureName = "";
            OrbResources.Control_MenuItem_Backdrop_TopMiddleTextureName = "";
            OrbResources.Control_MenuItem_Backdrop_TopRightTextureName = "";
            OrbResources.Control_MenuItem_Backdrop_Visible = true;
            OrbResources.Control_MenuItem_BorderWidth = 1;
            OrbResources.Control_MenuItem_FontName = string.Empty;
            OrbResources.Control_MenuItem_HorizontalRule_Height = 1;
            OrbResources.Control_MenuItem_HorizontalRule_Text = "-";
            OrbResources.Control_MenuItem_HorizontalRule_VerticalPadding = 10;
            OrbResources.Control_MenuItem_HorizontalRuleColor = Color.Silver;
            OrbResources.Control_MenuItem_HoverOverlayColor = Color.Silver;
            OrbResources.Control_MenuItem_OpenDelay = 500;
            OrbResources.Control_MenuItem_OverlayColor = Color.LightGray;
            OrbResources.Control_MenuItem_TextColor = Color.Black;
            OrbResources.Control_MessageBox_ButtonSpacing = 20;
            OrbResources.Control_MessageBox_ButtonYOffset = 90;
            OrbResources.Control_MessageBox_CancelButton_InstanceName = "{0}_btnCancel";
            OrbResources.Control_MessageBox_CancelButton_Text = "Cancel";
            OrbResources.Control_MessageBox_CancelButton_TextColor = Color.Black;
            OrbResources.Control_MessageBox_CancelButton_Width = 100;
            OrbResources.Control_MessageBox_InputHeight = 30;
            OrbResources.Control_MessageBox_InputTextBox_InstanceName = "{0}_txtInput";
            OrbResources.Control_MessageBox_InputWidthDifference = 80;
            OrbResources.Control_MessageBox_InputYOffset = 124;
            OrbResources.Control_MessageBox_Label_FontName = string.Empty;
            OrbResources.Control_MessageBox_Label_Formatting = TextFormattings.WrapWords;
            OrbResources.Control_MessageBox_Label_HorizontalTextAlign = HorizontalTextAlignment.Center;
            OrbResources.Control_MessageBox_Label_InstanceName = "{0}_lbl";
            OrbResources.Control_MessageBox_Label_TextColor = Color.White; // jcg
            OrbResources.Control_MessageBox_Label_VerticalTextAlign = VerticalTextAlignment.Middle;
            OrbResources.Control_MessageBox_Label_WidthOffset = 40;
            OrbResources.Control_MessageBox_Label_Y = 0;
            OrbResources.Control_MessageBox_LabelHeightOffset = 80;
            OrbResources.Control_MessageBox_LabelWithInputBoxHeightOffset = 124;
            OrbResources.Control_MessageBox_NoButton_InstanceName = "{0}_btnNo";
            OrbResources.Control_MessageBox_NoButton_Text = "No";
            OrbResources.Control_MessageBox_NoButton_TextColor = Color.Black;
            OrbResources.Control_MessageBox_NoButton_Width = 100;
            OrbResources.Control_MessageBox_OKButton_InstanceName = "{0}_btnOK";
            OrbResources.Control_MessageBox_OKButton_Text = "OK";
            OrbResources.Control_MessageBox_OKButton_TextColor = Color.Black;
            OrbResources.Control_MessageBox_OKButton_Width = 100;
            OrbResources.Control_MessageBox_YesButton_InstanceName = "{0}_btnYes";
            OrbResources.Control_MessageBox_YesButton_Text = "Yes";
            OrbResources.Control_MessageBox_YesButton_TextColor = Color.Black;
            OrbResources.Control_MessageBox_YesButton_Width = 100;
            OrbResources.Control_OnScreenKeyboard_BackSize = new Point(70, 30);
            OrbResources.Control_OnScreenKeyboard_BackslashSize = new Point(50, 30);
            OrbResources.Control_OnScreenKeyboard_BackText = "back";
            OrbResources.Control_OnScreenKeyboard_ButtonSpacing = new Point(10, 10);
            OrbResources.Control_OnScreenKeyboard_CapsLockSize = new Point(60, 30);
            OrbResources.Control_OnScreenKeyboard_CapsLockText = "caps";
            OrbResources.Control_OnScreenKeyboard_EnterSize = new Point(80, 30);
            OrbResources.Control_OnScreenKeyboard_EnterText = "enter";
            OrbResources.Control_OnScreenKeyboard_LeftShiftSize = new Point(80, 30);
            OrbResources.Control_OnScreenKeyboard_LeftShiftText = "shift";
            OrbResources.Control_OnScreenKeyboard_LetterSize = new Point(30, 30);
            OrbResources.Control_OnScreenKeyboard_RightShiftSize = new Point(100, 30);
            OrbResources.Control_OnScreenKeyboard_RightShiftText = "shift";
            OrbResources.Control_OnScreenKeyboard_Size = new Point(635, 240);
            OrbResources.Control_OnScreenKeyboard_SpaceBarXOffset = 170;
            OrbResources.Control_OnScreenKeyboard_SpaceSize = new Point(190, 30);
            OrbResources.Control_OnScreenKeyboard_SpaceText = "";
            OrbResources.Control_OnScreenKeyboard_StartingPosition = new Point(10, 2);
            OrbResources.Control_OnScreenKeyboard_TabSize = new Point(50, 30);
            OrbResources.Control_OnScreenKeyboard_TabText = "tab";
            OrbResources.Control_Panel_Backdrop_BackColor = Color.White;
            OrbResources.Control_Panel_Backdrop_BackgroundTextureName = "";
            OrbResources.Control_Panel_Backdrop_BorderColor = Color.Black;
            OrbResources.Control_Panel_Backdrop_BorderWidth = 1;
            OrbResources.Control_Panel_Backdrop_BottomLeftTextureName = "";
            OrbResources.Control_Panel_Backdrop_BottomMiddleTextureName = "";
            OrbResources.Control_Panel_Backdrop_BottomRightTextureName = "";
            OrbResources.Control_Panel_Backdrop_DrawBackground = false;
            OrbResources.Control_Panel_Backdrop_DrawBackgroundImage = false;
            OrbResources.Control_Panel_Backdrop_DrawBorder = true;
            OrbResources.Control_Panel_Backdrop_DrawTextureLayout = false;
            OrbResources.Control_Panel_Backdrop_InstanceName = "{0}_Panel_Backdrop";
            OrbResources.Control_Panel_Backdrop_MiddleLeftTextureName = "";
            OrbResources.Control_Panel_Backdrop_MiddleRightTextureName = "";
            OrbResources.Control_Panel_Backdrop_MiddleTextureName = "";
            OrbResources.Control_Panel_Backdrop_TopLeftTextureName = "";
            OrbResources.Control_Panel_Backdrop_TopMiddleTextureName = "";
            OrbResources.Control_Panel_Backdrop_TopRightTextureName = "";
            OrbResources.Control_Panel_Backdrop_Visible = false;
            OrbResources.Control_Panel_BorderWidth = 1;
            OrbResources.Control_PictureBox_Backdrop_BackColor = Color.Black;
            OrbResources.Control_PictureBox_Backdrop_BackgroundTextureName = "";
            OrbResources.Control_PictureBox_Backdrop_BorderColor = Color.DarkGray;
            OrbResources.Control_PictureBox_Backdrop_BorderWidth = 1;
            OrbResources.Control_PictureBox_Backdrop_BottomLeftTextureName = "";
            OrbResources.Control_PictureBox_Backdrop_BottomMiddleTextureName = "";
            OrbResources.Control_PictureBox_Backdrop_BottomRightTextureName = "";
            OrbResources.Control_PictureBox_Backdrop_DrawBackground = false;
            OrbResources.Control_PictureBox_Backdrop_DrawBackgroundImage = false;
            OrbResources.Control_PictureBox_Backdrop_DrawBorder = true;
            OrbResources.Control_PictureBox_Backdrop_DrawTextureLayout = false;
            OrbResources.Control_PictureBox_Backdrop_InstanceName = "{0}_PictureBox_Backdrop";
            OrbResources.Control_PictureBox_Backdrop_MiddleLeftTextureName = "";
            OrbResources.Control_PictureBox_Backdrop_MiddleRightTextureName = "";
            OrbResources.Control_PictureBox_Backdrop_MiddleTextureName = "";
            OrbResources.Control_PictureBox_Backdrop_TopLeftTextureName = "";
            OrbResources.Control_PictureBox_Backdrop_TopMiddleTextureName = "";
            OrbResources.Control_PictureBox_Backdrop_TopRightTextureName = "";
            OrbResources.Control_PictureBox_Backdrop_Visible = true;
            OrbResources.Control_PictureBox_BorderWidth = 1;
            OrbResources.Control_Potentiometer_Backdrop_BackColor = Color.White;
            OrbResources.Control_Potentiometer_Backdrop_BackgroundTextureName = @"UI\Potentiometer_Background";
            OrbResources.Control_Potentiometer_Backdrop_BorderColor = Color.Black;
            OrbResources.Control_Potentiometer_Backdrop_BorderWidth = 0;
            OrbResources.Control_Potentiometer_Backdrop_BottomLeftTextureName = "";
            OrbResources.Control_Potentiometer_Backdrop_BottomMiddleTextureName = "";
            OrbResources.Control_Potentiometer_Backdrop_BottomRightTextureName = "";
            OrbResources.Control_Potentiometer_Backdrop_DrawBackground = false;
            OrbResources.Control_Potentiometer_Backdrop_DrawBackgroundImage = true;
            OrbResources.Control_Potentiometer_Backdrop_DrawBorder = false;
            OrbResources.Control_Potentiometer_Backdrop_DrawTextureLayout = false;
            OrbResources.Control_Potentiometer_Backdrop_InstanceName = "{0}_Potentiometer_Backdrop";
            OrbResources.Control_Potentiometer_Backdrop_MiddleLeftTextureName = "";
            OrbResources.Control_Potentiometer_Backdrop_MiddleRightTextureName = "";
            OrbResources.Control_Potentiometer_Backdrop_MiddleTextureName = "";
            OrbResources.Control_Potentiometer_Backdrop_TopLeftTextureName = "";
            OrbResources.Control_Potentiometer_Backdrop_TopMiddleTextureName = "";
            OrbResources.Control_Potentiometer_Backdrop_TopRightTextureName = "";
            OrbResources.Control_Potentiometer_Backdrop_Visible = true;
            OrbResources.Control_Potentiometer_Center = new Point(110, 110);
            OrbResources.Control_Potentiometer_DebugDegrees = false;
            OrbResources.Control_Potentiometer_MaxDegree = 486f;
            OrbResources.Control_Potentiometer_MinDegree = 232f;
            OrbResources.Control_Potentiometer_OverlayColor = Color.White;
            OrbResources.Control_Potentiometer_TextureName = @"UI\Potentiometer";
            OrbResources.Control_ProgressBar_Backdrop_BackColor = Color.White;
            OrbResources.Control_ProgressBar_Backdrop_BackgroundTextureName = "";
            OrbResources.Control_ProgressBar_Backdrop_BorderColor = Color.Black;
            OrbResources.Control_ProgressBar_Backdrop_BorderWidth = 1;
            OrbResources.Control_ProgressBar_Backdrop_BottomLeftTextureName = "";
            OrbResources.Control_ProgressBar_Backdrop_BottomMiddleTextureName = "";
            OrbResources.Control_ProgressBar_Backdrop_BottomRightTextureName = "";
            OrbResources.Control_ProgressBar_Backdrop_DrawBackground = false;
            OrbResources.Control_ProgressBar_Backdrop_DrawBackgroundImage = false;
            OrbResources.Control_ProgressBar_Backdrop_DrawBorder = true;
            OrbResources.Control_ProgressBar_Backdrop_DrawTextureLayout = false;
            OrbResources.Control_ProgressBar_Backdrop_InstanceName = "{0}_ProgressBar_Backdrop";
            OrbResources.Control_ProgressBar_Backdrop_MiddleLeftTextureName = "";
            OrbResources.Control_ProgressBar_Backdrop_MiddleRightTextureName = "";
            OrbResources.Control_ProgressBar_Backdrop_MiddleTextureName = "";
            OrbResources.Control_ProgressBar_Backdrop_TopLeftTextureName = "";
            OrbResources.Control_ProgressBar_Backdrop_TopMiddleTextureName = "";
            OrbResources.Control_ProgressBar_Backdrop_TopRightTextureName = "";
            OrbResources.Control_ProgressBar_Backdrop_Visible = true;
            OrbResources.Control_Progressbar_BlockSpacing = 1;
            OrbResources.Control_ProgressBar_BorderWidth = 2;
            OrbResources.Control_ProgressBar_Gradient_Colors = new[]
                                                                   {
                                                                       Color.White, Color.LightBlue, Color.PaleTurquoise,
                                                                       Color.DodgerBlue, Color.White
                                                                   };
            OrbResources.Control_ProgressBar_Gradient_Percents = new[] { 0, 10, 40, 60, 90 };
            OrbResources.Control_ProgressBar_OverlayColor = Color.SkyBlue;
            OrbResources.Control_RadioButton_Backdrop_BackColor = Color.White;
            OrbResources.Control_RadioButton_Backdrop_BackgroundTextureName = "";
            OrbResources.Control_RadioButton_Backdrop_BorderColor = Color.White;
            OrbResources.Control_RadioButton_Backdrop_BorderWidth = 1;
            OrbResources.Control_RadioButton_Backdrop_BottomLeftTextureName = "";
            OrbResources.Control_RadioButton_Backdrop_BottomMiddleTextureName = "";
            OrbResources.Control_RadioButton_Backdrop_BottomRightTextureName = "";
            OrbResources.Control_RadioButton_Backdrop_DrawBackground = true;
            OrbResources.Control_RadioButton_Backdrop_DrawBackgroundImage = false;
            OrbResources.Control_RadioButton_Backdrop_DrawBorder = true;
            OrbResources.Control_RadioButton_Backdrop_DrawTextureLayout = false;
            OrbResources.Control_RadioButton_Backdrop_InstanceName = "{0}_RadioButton_Backdrop";
            OrbResources.Control_RadioButton_Backdrop_MiddleLeftTextureName = "";
            OrbResources.Control_RadioButton_Backdrop_MiddleRightTextureName = "";
            OrbResources.Control_RadioButton_Backdrop_MiddleTextureName = "";
            OrbResources.Control_RadioButton_Backdrop_TopLeftTextureName = "";
            OrbResources.Control_RadioButton_Backdrop_TopMiddleTextureName = "";
            OrbResources.Control_RadioButton_Backdrop_TopRightTextureName = "";
            OrbResources.Control_RadioButton_Backdrop_Visible = false;
            OrbResources.Control_RadioButton_BorderWidth = 2;
            OrbResources.Control_RadioButton_CanUncheck = true;
            OrbResources.Control_RadioButton_CheckedTextureName = @"UI\RadioButton_Checked";
            OrbResources.Control_RadioButton_FontName = string.Empty;
            OrbResources.Control_RadioButton_Label_HorizontalTextAlign = HorizontalTextAlignment.Left;
            OrbResources.Control_RadioButton_Label_InstanceName = "{0}_Label";
            OrbResources.Control_RadioButton_Label_TextColor = Color.White;
            OrbResources.Control_RadioButton_Label_TextFormatting = TextFormattings.Ellipse;
            OrbResources.Control_RadioButton_Label_VerticalTextAlign = VerticalTextAlignment.Middle;
            OrbResources.Control_RadioButton_OverlayColor = Color.White;
            OrbResources.Control_RadioButton_UncheckedTextureName = @"UI\RadioButton_Unchecked";
            OrbResources.Control_RadioButtonGroup_CanUncheck = false;
            OrbResources.Control_Slider_CenterTextureName = @"UI\Slider_Center";
            OrbResources.Control_Slider_CursorOverlayColor = Color.White;
            OrbResources.Control_Slider_CursorOverlayCursorOverColor = Color.SkyBlue;
            OrbResources.Control_Slider_CursorTextureName = @"UI\Slider_Cursor";
            OrbResources.Control_Slider_HasHighlight = true;
            OrbResources.Control_Slider_HighlightOverlayColor = Color.LightBlue;
            OrbResources.Control_Slider_LeftTextureName = @"UI\Slider_Left";
            OrbResources.Control_Slider_OverlayColor = Color.White;
            OrbResources.Control_TextBox_Backdrop_BackColor = Color.White;
            OrbResources.Control_TextBox_Backdrop_BackgroundTextureName = "";
            OrbResources.Control_TextBox_Backdrop_BorderColor = Color.Black;
            OrbResources.Control_TextBox_Backdrop_BorderWidth = 1;
            OrbResources.Control_TextBox_Backdrop_BottomLeftTextureName = "UI\\TextBox_BottomLeft";
            OrbResources.Control_TextBox_Backdrop_BottomMiddleTextureName = "UI\\TextBox_BottomMiddle";
            OrbResources.Control_TextBox_Backdrop_BottomRightTextureName = "UI\\TextBox_BottomRight";
            OrbResources.Control_TextBox_Backdrop_DrawBackground = false;
            OrbResources.Control_TextBox_Backdrop_DrawBackgroundImage = false;
            OrbResources.Control_TextBox_Backdrop_DrawBorder = false;
            OrbResources.Control_TextBox_Backdrop_DrawTextureLayout = true;
            OrbResources.Control_TextBox_Backdrop_InstanceName = "{0}_TextBox_Backdrop";
            OrbResources.Control_TextBox_Backdrop_MiddleLeftTextureName = "UI\\TextBox_MiddleLeft";
            OrbResources.Control_TextBox_Backdrop_MiddleRightTextureName = "UI\\TextBox_MiddleRight";
            OrbResources.Control_TextBox_Backdrop_MiddleTextureName = "UI\\TextBox_Middle";
            OrbResources.Control_TextBox_Backdrop_TopLeftTextureName = "UI\\TextBox_TopLeft";
            OrbResources.Control_TextBox_Backdrop_TopMiddleTextureName = "UI\\TextBox_TopMiddle";
            OrbResources.Control_TextBox_Backdrop_TopRightTextureName = "UI\\TextBox_TopRight";
            OrbResources.Control_TextBox_Backdrop_Visible = true;
            OrbResources.Control_TextBox_BorderWidth = 2;
            OrbResources.Control_TextBox_FontName = string.Empty;
            OrbResources.Control_TextBox_HorizontalScrollBarName = "{0}_TextBox_HScrollBar";
            OrbResources.Control_TextBox_TextColor = Color.Black;
            OrbResources.Control_TextBox_VerticalScrollBarName = "{0}_TextBox_VScrollBar";
            OrbResources.Control_ToolTipLabel_FontName = g_sFontName_Gamefont;
            OrbResources.Control_ToolTipLabel_InstanceName = "lblToolTip";
            OrbResources.Control_VerticalScrollBar_BackgroundTextureName = @"UI\VerticalScrollBar_Background";
            OrbResources.Control_VerticalScrollBar_ButtonDownName = "{0}_btDown";
            OrbResources.Control_VerticalScrollBar_ButtonImageName = @"UI\VerticalScrollBar_Button";
            OrbResources.Control_VerticalScrollBar_ButtonUpName = "{0}_btUp";
            OrbResources.Control_VerticalScrollBar_CursorBottomHeight = 7;
            OrbResources.Control_VerticalScrollBar_CursorMinimumHeight = 13;
            OrbResources.Control_VerticalScrollBar_CursorTextureName = @"UI\VerticalScrollBar_Cursor";
            OrbResources.Control_VerticalScrollBar_CursorTopHeight = 6;
            OrbResources.Control_VerticalScrollBar_OverlayColor = Color.White;
            OrbResources.Control_VerticalScrollBar_ScrollRepeatDelay = 5;
            OrbResources.Control_VerticalScrollBar_ScrollRepeatInitialDelay = 500;
            OrbResources.Control_VerticalScrollBar_Width = 12;
            OrbResources.OrbImageCursor_AppStarting_TextureName = "";
            OrbResources.OrbImageCursor_Center = new Vector2(0f, 0f);
            OrbResources.OrbImageCursor_Cross_TextureName = "";
            OrbResources.OrbImageCursor_Default_TextureName = "UI\\Cursor_Arrow";
            OrbResources.OrbImageCursor_DrawShadow = true;
            OrbResources.OrbImageCursor_Effect = SpriteEffects.None;
            OrbResources.OrbImageCursor_Hand_TextureName = "UI\\Cursor_Hand";
            OrbResources.OrbImageCursor_Help_TextureName = "";
            OrbResources.OrbImageCursor_HSplit_TextureName = "";
            OrbResources.OrbImageCursor_IBeam_TextureName = "UI\\Cursor_IBeam";
            OrbResources.OrbImageCursor_No_TextureName = "";
            OrbResources.OrbImageCursor_NoMove2D_TextureName = "";
            OrbResources.OrbImageCursor_NoMoveHoriz_TextureName = "";
            OrbResources.OrbImageCursor_NoMoveVert_TextureName = "";
            OrbResources.OrbImageCursor_OverlayColor = Color.White;
            OrbResources.OrbImageCursor_PanEast_TextureName = "";
            OrbResources.OrbImageCursor_PanNE_TextureName = "";
            OrbResources.OrbImageCursor_PanNorth_TextureName = "";
            OrbResources.OrbImageCursor_PanNW_TextureName = "";
            OrbResources.OrbImageCursor_PanSE_TextureName = "";
            OrbResources.OrbImageCursor_PanSouth_TextureName = "";
            OrbResources.OrbImageCursor_PanSW_TextureName = "";
            OrbResources.OrbImageCursor_PanWest_TextureName = "";
            OrbResources.OrbImageCursor_Rotation = 0;
            OrbResources.OrbImageCursor_Scale = 1f;
            OrbResources.OrbImageCursor_ShadowColor = new Color(0, 0, 0, 0.1f);
            OrbResources.OrbImageCursor_ShadowOffset = new Vector2(2f, 4f);
            OrbResources.OrbImageCursor_SizeAll_TextureName = "";
            OrbResources.OrbImageCursor_SizeNESW_TextureName = "";
            OrbResources.OrbImageCursor_SizeNS_TextureName = "";
            OrbResources.OrbImageCursor_SizeNWSE_TextureName = "UI\\Cursor_SizeNWSE";
            OrbResources.OrbImageCursor_SizeWE_TextureName = "";
            OrbResources.OrbImageCursor_UpArrow_TextureName = "";
            OrbResources.OrbImageCursor_VSplit_TextureName = "";
            OrbResources.OrbImageCursor_WaitCursor_TextureName = "";
            OrbResources.OrbTextHelper_BreakableCharacters.Add(' ');
            OrbResources.OrbTextHelper_BreakableCharacters.Add('-');
            OrbResources.OrbTextHelper_BreakableCharacters.Add('/');
            OrbResources.OrbTextHelper_BreakableCharacters.Add('\\');
            OrbResources.OrbTextHelper_BreakableCharacters.Add('\t');
            #endregion
        }

        #endregion Methods

#if DEBUG || XBOX
        void SignedInGamerSignedIn(object sender, SignedInEventArgs e)
        {

        }

        void SignedInGamerSignedOut(object sender, SignedOutEventArgs e)
        {
            // Return to menu screen if current Player signs out
            if (e.Gamer.PlayerIndex == Global.CurrentPlayer)
            {
                // Stop the sync if it isn't currently stopping or stopped
                if (Global.SyncManager.Enabled == true && Global.SyncManager.IsStopping() == false)
                    Global.SyncManager.Stop(null, false);
                LoadingScreen.Load(screenManager, true, null, new BackgroundScreen(), new TitleScreen());

            }
        }
#endif
    }
}