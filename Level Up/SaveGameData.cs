﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Content;
using System.Xml.Serialization;
using System.Xml;


#if (Windows) 
    [Serializable()]
#endif
public class SaveGameData
{
    public DateTime Birth;
    public DateTime LastSave;
    public string Gamertag;
    public string[] Traits;
    public string[] Stats;
    public string[] Equipment;
    public string[] Spells;
    public string[] Spellsvalue;
    public string[] Plot;
    public string[] Quests;
    public string[] Items;
    public string[] Itemsvalue;
    public int Itemcount;
    public int ExperiencePercentage;
    public int ExperienceTotal;
    public int ExperienceMax;
    public int PlotPercentage;
    public int PlotTotal;
    public int PlotMax;
    public int QuestsPercentage;
    public int QuestsTotal;
    public int QuestsMax;
    public string Questmonster;
}
