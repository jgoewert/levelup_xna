#region Header
// --------------------------------
// <copyright file="TopScoreList.cs" company="John Goewert">
//     Copyright John Goewert 2011.
// </copyright>
// <author>John Goewert</author>
// ---------------------------------

/*
Copyright (c) 2010 Spyn Doctor Games (Johannes Hubert). All rights reserved.

Redistribution and use in binary forms, with or without modification, and for whatever
purpose (including commercial) are permitted. Atribution is not required. If you want
to give attribution, use the following text and URL (may be translated where required):
        Uses source code by Spyn Doctor Games - http://www.spyn-doctor.de

Redistribution and use in source forms, with or without modification, are permitted
provided that redistributions of source code retain the above copyright notice, this
list of conditions and the following disclaimer.

THIS SOFTWARE IS PROVIDED BY SPYN DOCTOR GAMES (JOHANNES HUBERT) "AS IS" AND ANY EXPRESS
OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
SPYN DOCTOR GAMES (JOHANNES HUBERT) OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Last change: 2010-09-16
*/

#endregion Header

using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Diagnostics;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Net;

namespace SpynDoctor
{
    public class TopScoreList
    {
        #region Fields

        private readonly object SYNC = new object();

        private readonly List<TopScoreEntry> mEntryList;
        private readonly Dictionary<string, TopScoreEntry> mEntryMap;
        private readonly List<TopScoreEntry> mFilteredList;
        private readonly int mMaxSize;

        #endregion Fields

        #region Constructors

        public TopScoreList(int maxSize)
        {
            mMaxSize = maxSize;
            mEntryList = new List<TopScoreEntry>();
            mFilteredList = new List<TopScoreEntry>();
            mEntryMap = new Dictionary<string, TopScoreEntry>();
        }

        public TopScoreList(BinaryReader reader)
            : this(reader.ReadInt32())
        {
            int count = reader.ReadInt32();
            for (int i = 0; i < count; i++)
            {
                var entry = new TopScoreEntry(reader, true);
                if (entry.IsLegal())
                {
                    mEntryMap[entry.Gamertag + "/" + entry.PlayerName] = entry;
                    mEntryList.Add(entry);
                }
            }
        }

        #endregion Constructors

        #region Methods

        public bool AddEntry(TopScoreEntry entry)
        {
            if (!entry.IsLegal())
                return false;

            lock (SYNC)
            {
                string gamertag = entry.Gamertag + "/" + entry.PlayerName;
                if (mEntryMap.ContainsKey(gamertag))
                {
                    // Existing entry found for this gamertag
                    TopScoreEntry existingEntry = mEntryMap[gamertag];
                    int compareValue = entry.CompareTo(existingEntry);
                    if (compareValue < 0)
                    {
                        // new entry is smaller: do not insert
                        return false;
                    }
                    else if (compareValue == 0)
                    {
                        // both entries are equal: Keep existing entry but transfer "IsLocal" state
                        existingEntry.IsLocalEntry = entry.IsLocalEntry;
                        return false;
                    }
                    else
                    {
                        // existing entry is smaller: replace with new entry
                        mEntryList.Remove(existingEntry);
                        this.AddNewEntry(entry); // this also replaces existing entry in mEntryMap
                        return true;
                    }
                }
                else
                    return this.AddNewEntry(entry);
            }
        }

        public bool ContainsEntryForGamertag(string gamertag)
        {
            lock (SYNC)
            {
                for (int i = 0; i < mEntryList.Count; i++)
                {
                    if (mEntryList[i].Gamertag == gamertag)
                        return true;
                }
                return false;
            }
        }

        public void FillPageFromFilteredList(int pageNumber, TopScoreEntry[] page, SignedInGamer gamer)
        {
            lock (SYNC)
            {
                InitFilteredList(gamer, true);
                FillPage(mFilteredList, false, pageNumber, page);
            }
        }

        public void FillPageFromFullList(int pageNumber, TopScoreEntry[] page)
        {
            lock (SYNC)
            {
                FillPage(mEntryList, true, pageNumber, page);
            }
        }

        public int FillPageThatContainsGamertagFromFilteredList(TopScoreEntry[] page, SignedInGamer gamer)
        {
            lock (SYNC)
            {
                InitFilteredList(gamer, true);

                int indexOfGamertag = 0;
                for (int i = 0; i < mFilteredList.Count; i++)
                {
                    if (mFilteredList[i].Gamertag == gamer.Gamertag)
                    {
                        indexOfGamertag = i;
                        break;
                    }
                }
                int pageNumber = indexOfGamertag / page.Length;
                FillPage(mFilteredList, false, pageNumber, page);
                return pageNumber;
            }
        }

        public int FillPageThatContainsGamertagFromFullList(TopScoreEntry[] page, string gamertag)
        {
            lock (SYNC)
            {
                int indexOfGamertag = 0;
                for (int i = 0; i < mEntryList.Count; i++)
                {
                    if (mEntryList[i].Gamertag == gamertag)
                    {
                        indexOfGamertag = i;
                        break;
                    }
                }
                int pageNumber = indexOfGamertag / page.Length;
                FillPage(mEntryList, true, pageNumber, page);
                return pageNumber;
            }
        }

        public int FillPageWithGamertagFromFullList(TopScoreEntry[] page, string gamertag)
        {
            lock (SYNC)
            {
                int indexOfGamertag = 0;
                for (int i = 0; i < mEntryList.Count; i++)
                {
                    if (mEntryList[i].Gamertag == gamertag)
                    {
                        indexOfGamertag = i;
                        break;
                    }
                }
                int pageNumber = indexOfGamertag / page.Length;
                FillPage(mEntryList, true, pageNumber, page,gamertag);
                return pageNumber;
            }
        }

        public int GetFilteredCount(SignedInGamer gamer)
        {
            lock (SYNC)
            {
                InitFilteredList(gamer, false);
                return mFilteredList.Count;
            }
        }

        public int GetFullCount()
        {
            lock (SYNC)
            {
                return mEntryList.Count;
            }
        }

        public void InitForTransfer()
        {
            lock (SYNC)
            {
                foreach (TopScoreEntry entry in mEntryList)
                    entry.IsLocalEntry = true; // at the beginning of a transfer, all entries are local
            }
        }

        public bool ReadTransferEntry(PacketReader reader)
        {
            lock (SYNC)
            {
                return this.AddEntry(new TopScoreEntry(reader, false));
            }
        }

        public void Write(BinaryWriter writer)
        {
            lock (SYNC)
            {
                writer.Write(mMaxSize);
                writer.Write(mEntryList.Count);
                foreach (TopScoreEntry entry in mEntryList)
                    entry.Write(writer);
            }
        }

        public int WriteNextTransferEntry(PacketWriter writer, int myListIndex, int entryIndex)
        {
            lock (SYNC)
            {
                while (entryIndex < mEntryList.Count)
                {
                    // While there are still more entries in the current list:
                    // Find a local entry that needs transfer
                    if (mEntryList[entryIndex].IsLocalEntry)
                    {

                        writer.Write(TopScoreListContainer.MARKEREntry);
                        writer.Write((byte)myListIndex);
                        mEntryList[entryIndex].Write(writer);
                        return entryIndex + 1;
                    }
                    else
                    {
                        entryIndex++;
                        Thread.Sleep(1);
                    }
                }
                return -1;
            }
        }

        private bool AddNewEntry(TopScoreEntry entry)
        {
            for (int i = 0; i < mEntryList.Count; i++)
            {
                if (entry.CompareTo(mEntryList[i]) >= 0)
                {
                    // Found existing smaller entry: Insert this one before
                    mEntryList.Insert(i, entry);
                    mEntryMap[entry.Gamertag + "/" + entry.PlayerName] = entry;
                    // Delete last entry if there are now too many
                    if (mEntryList.Count > mMaxSize)
                    {
                        TopScoreEntry removedEntry = mEntryList[mMaxSize];
                        mEntryList.RemoveAt(mMaxSize);
                        mEntryMap.Remove(removedEntry.Gamertag + "/" + removedEntry.PlayerName);
                    }
                    return true;
                }
            }

            // No existing smaller entry found, but still space in list: Add at end
            if (mEntryList.Count < mMaxSize)
            {
                mEntryList.Add(entry);
                mEntryMap[entry.Gamertag + "/" + entry.PlayerName] = entry;
                return true;
            }

            // Entry added at end or No existing smaller entry found and list is full: Do not add
            return false;
        }

        private void FillPage(List<TopScoreEntry> list, bool initRank, int pageNumber, TopScoreEntry[] page, string gamertag)
        {
            int index = pageNumber * page.Length;
            int count = 0;
            for (int i = 0; i < list.Count; i++)
            {
                if (count < page.Length)
                {
                    if (list[index].Gamertag.Equals(gamertag))
                    {
                        page[count] = list[index];
                        count++;
                    }
                }
                index++;
            }
            if (count < page.Length)
            {
                for (int i = count; i < page.Length; i++)
                {
                    page[i] = null;
                }
            }

        }

        private void FillPage(List<TopScoreEntry> list, bool initRank, int pageNumber, TopScoreEntry[] page)
        {
            int index = pageNumber * page.Length;
            for (int i = 0; i < page.Length; i++)
            {
                if (index >= 0 && index < list.Count)
                {
                    page[i] = list[index];
                    if (initRank)
                        page[i].RankAtLastPageFill = index + 1;
                }
                else
                    page[i] = null;
                index++;
            }
        }

        private void InitFilteredList(SignedInGamer gamer, bool initRank)
        {
            string gamertag = gamer.Gamertag;
            
            if (!gamer.IsSignedInToLive)
            {
                return;
            }

            FriendCollection friendsFilter = gamer.GetFriends();
            mFilteredList.Clear();
            for (int i = 0; i < mEntryList.Count; i++)
            {
                TopScoreEntry entry = mEntryList[i];
                if (entry.Gamertag == gamertag)
                {
                    mFilteredList.Add(entry);
                    if (initRank)
                        entry.RankAtLastPageFill = i + 1;
                }
                else
                {
                    foreach (FriendGamer friend in friendsFilter)
                    {
                        if (entry.Gamertag == friend.Gamertag)
                        {
                            mFilteredList.Add(entry);
                            if (initRank)
                                entry.RankAtLastPageFill = i + 1;
                            break;
                        }
                    }
                }
            }
        }

        #endregion Methods
    }
}