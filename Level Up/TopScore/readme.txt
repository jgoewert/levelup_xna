---= OnlineDataSyncManager =---

A framework of classes for peer-to-peer distributed scoreboards, for use in
Xbox LIVE Indie Games (with XNA 4.0)

Written by Spyn Doctor (Johannes Hubert)

Contact: http://www.spyn-doctor.de,
         or look for me in the Creators Club forum http://forums.xna.com (as Spyn Doctor)

----------------------------------------------------------------------------------------

Copyright (c) 2010-2011 Spyn Doctor Games (Johannes Hubert). All rights reserved.

Redistribution and use in binary forms, with or without modification, and for whatever
purpose (including commercial) are permitted. Atribution is not required. If you want
to give attribution, use the following text and URL (may be translated where required):
		Uses source code by Spyn Doctor Games - http://www.spyn-doctor.de

Redistribution and use in source forms, with or without modification, are permitted
provided that redistributions of source code retain the above copyright notice, this
list of conditions and the following disclaimer.

THIS SOFTWARE IS PROVIDED BY SPYN DOCTOR GAMES (JOHANNES HUBERT) "AS IS" AND ANY EXPRESS
OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
SPYN DOCTOR GAMES (JOHANNES HUBERT) OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Last change: 2011-01-20

----------------------------------------------------------------------------------------

The frameworks consists of two parts:

Part 1: The network component
-----------------------------

This part contains one class and one interface:

        OnlineDataSyncManager
        IOnlineSyncTarget

This part performs the actual data synching between two instances of a XBLIG game.
This network component part is agnostic about the actual kind of data that is being synched.
Most likely, it will be used to synch online score boards, but it can be used to synch any
kind of data that implements the IOnlineSyncTarget interface.
Please see the comments in the source code for IOnlineSyncTarget for more details of how
to implement this interface.
For a sample implementation of this IOnlineSyncTarget interface, see the description of
"part 2" below, which shows how to use this interface to set up syncable global scoreboards.


The OnlineDataSyncManager is a standard XNA GameComponent.

To integrate this into your game, you would usually add a member of type OnlineDataSyncManager
to your game class:

	private OnlineDataSyncManager	mSyncManager;
	
Then in the game's Initialize method, you create a new manager and add it to your components:

	mSyncManager = new OnlineDataSyncManager(0, this);
	Components.Add(mSyncManager);

(Note: The "0" in the ctor is the version number. See the comments in the source code of
       OnlineDataSyncManager for more details about this version number.)

Then after you have detected the active player, usually after the "Press Start to Play" screen,
when the active PlayerIndex is known, you first check if the active player is actually allowed
to open a network session, and if that is the case, you start the manager:

	if (!Guide.IsTrialMode && mPlayer != null && mPlayer.IsSignedInToLive && mPlayer.Privileges.AllowOnlineSessions)
		mSyncManager.start(mPlayer, dataToSync);

where dataToSync must be an object that implement the IOnlineSyncTarget interface. This is the data
that is to be synched with other instances of the game, over Xbox LIVE. For example the class
TopScoreListContainer implements this interface (see "part 2" below).

Finally, you also must make sure to stop the manager again, when necessary. Stopping the manager is
a bit tricky, because it is running in a separate thread and may be in a state that requires a
few more update cycles to properly clean up.

You need to stop the manager if the user signs out in the middle of the game, i.e. after you detect
the sign-out. In such a case, you can stop the manager without specifying an AfterStopDelegate
("null" as the first argument) and by specifying that you are *not* stopping the manager because
the game exits ("false" as the second argument):

	mSyncManager.stop(null, false);
	
After calling this stop method, do whatever you usually do in your game after you detect that
the current player signed out (either continue the game without a profile, or for example return
to the "Press Start to Play" screen).

Similarly, you need to stop the manager if your game provides any other method for the player to
return to the "Press Start to Play" screen (for example via a "Back" item in the main menu or
similar). In such a case, stop the manager without a delegate and a "false" argument just like
shown above.

Of course if you have a good reason, you *can* also pass a non-null AfterStopDelegate in the
above case, with code that you want to have executed as soon as the manager has been stopped.
But for the case above, where the game itself is *not* exiting, you always must pass "false" as
the second argument.

You also need to stop the manager when exiting the game. And in *this* case, you must pass "true"
as the second argument, so that the manager knows that the game is in the process of exiting.
In addition, you must put the actual Game.Exit() command into the delegate that you pass as the
first argument and that is executed by the manager after the stop command was successful:

	mSyncManager.stop(delegate() {
			ReferenceToMyGame.Exit();
		},
		true);

The above makes sure, that the actual call to Game.Exit() is only executed once the manager has been
stopped properly, which in turn allows the manager to properly end the current session, making life
easier for the peer on the other side of the session.
If you have other code that needs to be performed on game exit, then depending on the nature of that
code, you can either put it before the stop() command itself, or also put it into the delegate.

IMPORTANT: When the call to stop() returns, then this does *not* necessarily mean that the manager
is already stopped completely - it may still need a few more frames. You must *not* call stop() or
start() again before it has stopped completely.
In most cases this is not a problem, because the situations in your game where you want to start or
stop the manager are usually in separate locations, so that there are always at least a few seconds
(and thus many frames) between them (for example during which the player has to navigate a menu, or
similar). But if you want to be absolutely sure that the processing of a stop-call has finished,
query the isStopping() method: This method returns "false" if currently the manager is either running
normally or is completely stopped. It returns "true" if the manager is currently in the process of
stopping, i.e. while it is no longer running normally but also not yet completely stopped. So after
a call to stop(), this method may return "true" for a few frames, before it returns "false" again.
During the time it returns "true", you must *not* call stop() or start().
In combination with the Enabled property, you can exactly determine the current state of the manager:
isStopping() == false && Enabled == true  -> Manager is running normally
isStopping() == false && Enabled == false -> Manager is completely stopped
isStopping() == true                      -> Manager is currently in the process of stopping

Summary:

The above describes how to initialize, start and stop the OnlineDataSyncManager component. It is this
component which handles the actual network traffic, via a separate worker thread.
The component however does not know *what* data it actually transfers. It only requires that the data
to transfer implements the IOnlineSyncTarget interface. The following part shows a sample implementation
of this interface, for global scoreboards.

*** Note: Please see also the comments in the source code
*** of OnlineDataSyncManager and IOnlineSyncTarget!


Part 2: A sample implementation for global scoreboards
------------------------------------------------------

This part contains three classes:

		TopScoreListContainer
		TopScoreList
		TopScoreEntry
		
These classes show a sample implementation of data that can be synchronized with the
OnlineDataSyncManager component. It implements a typical global scoreboard, with the
following features:

- You can specify how many scoreboards there shall be.
  For example only a single score board, or one score board for each difficulty, or one
  scoreboard for each level, or whatever.

- You can specify how many entries the scoreboards shall have at max.
  For example you can specify that each scoreboard shall have up to 10 entries (top-10), or 100 entries,
  or like I did with Your Doodles Are Bugged! and Kuchibi, up to 1000 entries per scoreboard.
  
- Each scoreboard entry contains the gamertag and the associated score.

- The entries are ordered so that the entry with the highest score comes first (= rank #1).

- For each gamertag, there is only one entry per scoreboard. If you have several scoreboards,
  then there can be one entry on each, for a given gamertag. If an entry for a certain gamertag
  is added to a scoreboard that already contains an entry for this gamertag, the following happens:
  If the new entry has a lower or equal score than the old entry, then the new entry is ignored, the old
  entry remains in the list. If the new entry has a higher score than the old entry, then the entry is
  moved to its new rank among the other entries, according to its new higher score.

TopScoreEntry is the class that implements a single entry in one of the score list.

TopScoreList is the class that implements a single score list (with its entries).

TopScoreListContainer is the class that wraps several score lists (which may also be only a single one,
if you only need one for your game).

You can either use this sample implementation as a pure sample, and do your own implementation of
IOnlineSyncTarget based on this sample, or you can just use the implementation "as is", if it fulfills
your requirements, or you can adjust it to your own needs.

The most likely adjustment would probably be, if you need to store more than the gamertag and score,
or if you need to store something else instead of the score (for example something that can not be
expressed as a score "int", like a finishing time of type "TimeSpan", or similar).

Edit the TopScoreEntry class if you need to make such changes, i.e. if you want to add new fields to
a scoreboard entry, or change the existing fields. If you do that, you would have to add the field
and adjust the code that writes and reads an entry, and also adjust the "compareTo" code to compare
two entries and the "isLegal" code, that checks if an entry is legal.


To integrate the score boards into your game, you would usually add a member of type
TopScoreListContainer to your game class:

	private TopScoreListContainer	mScores;
	
Then usually in the game's LoadContent method, you initialize this member:

If you already have a stored version of a previous score container on a storage device, then you
would open a FileStream to read the store file, wrap this into a BinaryReader, and then pass this
reader to the ctor:

	BinaryReader reader = new BinaryReader(...);
	mScores = new TopScoreListContainer(reader);
	reader.Close();

This will create a new score container and automatically fill it with all score lists and score
entries from the file on which the reader operates.

If you do not yet have a stored version of a previous score container, then you use the other ctor:

	mScores = new TopScoreListContainer(listCount, maxEntryCountPerList);

where you replace "listCount" and "maxEntryCountPerList" with the desired values.

Later, when you start the OnlineDataSyncManager, you will then pass this score container to the
sync manager:

	if (!Guide.IsTrialMode && mPlayer != null && mPlayer.IsSignedInToLive && mPlayer.Privileges.AllowOnlineSessions)
		mSyncManager.start(mPlayer, mScores);


All interaction with the top-score lists should go via this mScores object:

During the game, when a player achieves a new score that is supposed to be added to one of the score
lists in the container, call the addEntry() method:

	TopScoreEntry entry = new TopScoreEntry(...);
	mScores.addEntry(listIndex, entry, mSyncManager);

where you specify the "listIndex" of the score list into which the entry shall be added (0-based).
The list will automatically check if there already is an entry for the given gamertag and replace it
with the new entry, if the new entry is "better", or do nothing if the existing entry is "better".
If the new entry is actually added to the list because it is "better", then the call to addEntry will
also notify the OnlineDataSyncManager that you pass as the third argument, so that the manager can
start a new round of score sharing, where this new entry is included. (In certain situations, you can
also pass "null" as the third argument, in which case there won't be any such notification and adding
a new entry will not trigger any score sharing, but usually you should pass your OnlineDataSyncManager
instance.)

After adding a new entry to one of the lists, you may also want to save the score container (whenever
you usually do such data saving in your game). For this, open a FileStream on the file into which
you want to save, then contruct a BinaryWriter on this stream, and use this writer to call the save()
method:

	BinaryWriter writer = new BinaryWriter(...);
	mScores.save(writer);
	writer.Close();


To find out how many entries are in a certain list, use the getFullListSize() method:

	int size = mScores.getFullListSize(listIndex);
	
with the "listIndex" for the desired list.


To find out if there is an entry for a certain gamertag in a certain list, use the
containsEntryForGamertag() method:

	bool containsEntry = mScores.containsEntryForGamertag(listIndex, gamertag);
	
with the "listIndex" for the desired list and the "gamertag" you are interested in.


Read access to the lists happens page-wise: First you allocate an array of TopScoreEntry objects.
Then you call one of the "fill" methods of the score container to fill this array with entries.

Read access is done in this way (and for example not by implementing a method that returns
an Enumerator over all entries) because of synchronization concerns:
The OnlineDataSyncManager is running in its own worker thread and may write new entries (and
reorder the existing entries) in the score lists at any time. So if your main game thread would
get an Enumerator over all entries (for example to display the score list to the player), then
this Enumerator could become invalidated asynchronously if the data-synching thread happens to
run in parallel, causing all kinds of pain.
Therefore the page-based access: Each read access returns only one single page. While filling
this page, the score container is locked against write access by the data-synching thread (which
has to wait for a short moment).

There are two methods to get such a page with score entries:

Get a page by index - fillPageFromFullList():

	TopScoreEntry[] page = new TopScoreEntry[pageSize];
	mScores.fillPageFromFullList(listIndex, pageNumber, page);
	
with the desired "pageSize", the "listIndex" for the desired list, and the "pageNumber" for the
desired page (0-based). For example, if "pageSize" is "10", and "pageNumber" is "0", then the
"page" array would be filled with ranks 1-10. If "pageNumber" is instead "4", it would be filled
with ranks 41-50, etc. (See the method comment for more details.)

Get the page that contains a certain gamertag - fillPageThatContainsGamertagFromFullList():

	TopScoreEntry[] page = new TopScoreEntry[pageSize];
	mScores.fillPageThatContainsGamertagFromFullList(listIndex, gamertag, page);

with the desired "pageSize", the "listIndex" for the desired list, and the desired "gamertag".
For example, if "pageSize" is "10", and the gamertag happens to be at rank #37, then the "page"
array would be filled with ranks 31-40. (See the method comment for more details.)

Instead of
	getFullListSize(...)
	fillPageFromFullList(...)
	fillPageThatContainsGamertagFromFullList(...)
	
you can also use 
	getFilteredListSize(...)
	fillPageFromFilteredList(...)
	fillPageThatContainsGamertagFromFilteredList(...)

which work just like their "FullList" counterparts, only that they require an additional
SignedInGamer parameter for filtering. The score list will then behave as if it contains
only the entry for the given gamer, plus the entries for any friends on that gamer's friends
list. Which is useful if you want to display the scoreboard only with the entries of the
current player and his friends. (See the HOW-TO below for more details.)


*** Note: Please see also the comments in the source code
*** of TopScoreListContainer!

*** IMPORTANT ***

BEFORE you can use the current TopScoreListContainer, you need to implement one pending TODO, in
the endSynchronization() method:

This method is called by the OnlineDataSyncManager right after a synching session with a remote
instance has been completed, just after the network session has been closed.
At the location that is currently indicated by the TODO comment, you must add code which causes
the TopScoreListContainer to be saved to a storage device, so that the synched scores are actually
saved. How you do this depends on your game (this is different for each game, I guess).

For example, you may have a save manager with a separate worker thread which handles all saving, so here
you would need to signal the save manager that the score container needs to be saved. Or if saving is
done by the main game thread, then you may have to set a flag or something similar to signal the main
thread that the score container needs to be saved.
See above about how to actually save the container from that other thread (by calling the "save()" method).

Such methods rely on some other thread to do the actual saving (either a worker thread in a save manager,
or the main game thread, or similar), so the thread that calls endSynchronization() would just signal
the other thread that a save operation needs to be performed.

Another method would be do let the calling thread itself do the saving, i.e. call the saving-code
directly at the location that is currently marked with the TODO.


HOW TO:
-------
How to implement a UI to display the scores from the sample global scoreboard implementation (see above).
---------------------------------------------------------------------------------------------------------

Read access to the scoreboard happens page wise, so the best implementation for a UI is one where you
go up/down full pages, not row by row. For the sake of the example, I'll just assume that you want to
display pages with 10 scores at once. You would do this as follows:

When first showing the scoreboard UI, you would probably want to show the first page. Page indexes are
zero based, so you would retrieve page 0 from the score list you want to display. The score list you
want to display is specified by its list index, which is also zero based.
So to get the first page from the first score list, you first allocate an empty array with a size that
is equal to your desired page size, i.e. 10 in this example. Then you call the "fillPageFromFullList"
method, specifying the list index 0 and the page index (which would also be 0, for the first page):

	// Array to hold one page. Declare as a member, to avoid allocating a new one each time:
	TopScoreEntry[] page = new TopScoreEntry[10];	
	// Page index (probably also a member):
	int pageIndex = 0;

    mScores.fillPageFromFullList(0, pageIndex, page);

then you can go through this array to display the entries on this page. Note, that array entries can
also be "null", if there were not enough entries in the list to fill the page that you requested.
Something like this:

	foreach (TopScoreEntry entry in page) {
		if (entry != null) {
			// do something with the entry here, to display it
		}
	}

The entries have all the properties you need for display:

    TopScoreEntry.RankAtLastPageFill		// The current rank of this entry
    TopScoreEntry.Gamertag
    TopScoreEntry.PlayerScore

Now to allow the player to page up/down in the list, you would simply increment (=page down) or
decrement (=page up) the "pageIndex", and each time the pageIndex is changed, you would call the
topScores.fillPageFromFullList method again, to fill the array with the entries that correspond
to the new pageIndex.

When doing so, the pageIndex must stay in the range of:

	0 <= pageIndex <= (mScores.getFullListSize(0) - 1) / PAGE_SIZE)

The "0" arg for "getFullListSize" is the index of the list, and PAGE_SIZE would be "10" in our
example. You can either simply stop when the user reaches the top or bottom, or wrap around to the
other end.
If by accident you specify a pageIndex outside of this range, then the array will be filled with
only "null" entries.

Now, if there are a lot of gamertags on the list, it can be difficult for the player to find his own
entry. So you should probably add a button or something to the UI with the function "Show my own Rank".
When the user triggers this, use the "fillPageThatContainsGamertagFromFullList" method. It works just
like the other "fill" method described above, only this time you do not specify the pageIndex of the
page that you want to display, but instead you simply specify the gamertag of the player that you want
to display. The method automatically determines the correct page that contains this gamertag and fills
the array with that page. It also returns the pageIndex for that page, so that you can set your own
pageIndex member to that value, for further page up/down actions. Like this:

	pageIndex = mScores.fillPageThatContainsGamertagFromFullList(0, page, gamertag);

If the gamertag should not appear in the list at all, the method will simply return the very first
page. Note, that this method has two effects: It fills the array with the entries of the correct page
*and* returns the index of that page.

Of course you might only want to offer this special function if the current player actually *has* an
entry on the list. You can check this with:

	bool entryForGamerExists = mScores.containsEntryForGamertag(0, gamertag);

Finally, if you want, you can also add a "Show only myself and my friends" filter to the UI. If this
filter is activated, then instead of using the two fill methods described above, you use the filtered
fill methods. These filtered variants work just like the normal fill methods, only that they operate
on a virtual scoreboard that contains only the entries for the specified gamer himself and his friends.
For example, if normally the full scoreboard has 568 entries, then for the page size of 10 you would
have 57 pages (of which the last page only has 8 entries). Now let's assume that this list contains
the player's gamertag, and the tags of 36 people from the player's friends list. In this case, the
filtered scoreboard has 37 entries, i.e. 4 pages (of which the last page only has 7 entries).

To get a page by index from the filtered scoreboard, do a call like this:

	SignedInGamer gamer = .... // determine the gamer for which you want to filter the list
	mScores.fillPageFromFilteredList(0, pageIndex, page, gamer);

Remember that most likely the filtered list will be much shorter than the full list, i.e. pageIndex
now must stick to the following range:

	0 <= pageIndex <= (mScores.getFilteredListSize(0, gamer) - 1) / PAGE_SIZE)

(With the proper "gamer".)

Because of this, make sure to set the pageIndex into the proper range when the user toggles between
filtered and unfiltered!

To get the page that contains a certain gamer from the filtered scoreboard, do a call like this:

	SignedInGamer gamer = .... // determine the gamer for which you want to filter the list
	pageIndex = mScores.fillPageThatContainsGamertagFromFilteredList(0, page, gamer);

(As before, this fills the "page" array with the correct page that contains the specified gamer and
returns the pageIndex of that page.)

These filtered methods do of course also work if the "gamer" itself is not in the list at all. In
that case, only entries from the gamer's friends will be returned. If no friends are on the list
either, then the methods behave as if the list is empty (and the fill methods fill the array with
"null" entries).


HOW TO:
-------
How to test the score exchange with one Xbox and one PC
-------------------------------------------------------

I did most of my testing with an Xbox and a PC, using SystemLink networking. If you check the
comments in the class OnlineDataSyncManager, it describes a conditional compilation symbol called
SYSTEM_LINK_SESSION. If you define this in your project properties, then the class will be compiled
to use a local network SystemLink session, which you can use to test between Xbox and PC. I tested
this as follows:

I opened two instances of Visual Studio (VS C# Express in my case, but I guess that works with the
full version too) and loaded my project in both instances at the same time. I took care to only
change/edit my source files in one of the instances (the other instance then detects these changes
and asks if you want to reload the changed files, which you usually do). From one VS instance I ran
a copy of the game in debug mode on the PC, and from the other VS instance I ran a copy in remote
debug mode on the Xbox. My score exchange code logs debug messages to the output console, so I also
had the output windows of both instances open, arranged so that I could see both at the same time.

Both in the Xbox game and the Windows game you need to have an Xbox LIVE profile signed in, or
otherwise you can't start the OnlineDataSyncManager. On the Xbox, you of course must have your
CC-membership logged in anyway, to start the game in the first place, so no problem there. In the
Windows game, unless you have a second account with a CC-membership, you need to log in with a
local account. If you don't have one yet, see below on how to create one. Once the two instances
of the game are running, and in both instances a profile is signed in and was used to start the
OnlineDataSyncManager, you should see the debug output in the two output consoles showing you how
the two game instances connect to each other and share their scores. The debug output will also
show you if the score sharing was actually initialized and started correctly or not. You can now
generate some scores in one of the game instances and should see them show up in the other instance.

You can of course also test with two Xboxes, but then you also need two CC-membership accounts to
run your game on both Xboxes at the same time. If you have two Xboxes and two CC-memberships, you
can either test with SystemLink, or via "real" Xbox LIVE, with a PlayerMatch session. For system
link, the CC-membership profiles can be Silver profiles, if you want to test PlayerMatch/XboxLIVE,
they must be Gold profiles.
But if you don't have two Xboxes and CC-accounts, it should be enough to just get the score share
working between one Xbox and the PC, with SystemLink, as described above. Once that works, you just
remove the SYSTEM_LINK_SESSION symbol from your project settings and recompile the game to use
PlayerMatch/XboxLIVE, for a version to submit to Playtest (and later Review).

To create a local account on Windows: Start your game on Windows. When the game is running, press
the Guide button on the controller. Then select "New Profile" (or "New Account" - I'm not sure
about the English labels). Next you'll see a window with the title "Games for Windows LIVE" and a
lot of text. Do not click the "Continue" button, but instead use the scrollbar to scroll to the
bottom of the text. There should be a link at the end of the text to create a new local profile.

----= End =----