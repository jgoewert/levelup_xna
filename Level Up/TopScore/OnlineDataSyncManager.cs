#region Header
// --------------------------------
// <copyright file="OnlineDataSyncManager.cs" company="John Goewert">
//     Copyright John Goewert 2011.
// </copyright>
// <author>John Goewert</author>
// ---------------------------------

/*
Copyright (c) 2010-2011 Spyn Doctor Games (Johannes Hubert). All rights reserved.

Redistribution and use in binary forms, with or without modification, and for whatever
purpose (including commercial) are permitted. Atribution is not required. If you want
to give attribution, use the following text and URL (may be translated where required):
        Uses source code by Spyn Doctor Games - http://www.spyn-doctor.de

Redistribution and use in source forms, with or without modification, are permitted
provided that redistributions of source code retain the above copyright notice, this
list of conditions and the following disclaimer.

THIS SOFTWARE IS PROVIDED BY SPYN DOCTOR GAMES (JOHANNES HUBERT) "AS IS" AND ANY EXPRESS
OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
SPYN DOCTOR GAMES (JOHANNES HUBERT) OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Last change: 2011-01-20
*/

#endregion Header

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Net;

namespace SpynDoctor
{

    #region Delegates

    public delegate void AfterStopDelegate();

    #endregion Delegates

    public class OnlineDataSyncManager : GameComponent
    {
        #region Fields

        private const int MaximumServerTime = 7 * MillisINMinute;
        private const int MillisINMinute = 60000;
        private const int MinimumServerTime = 4 * MillisINMinute;

        // Number of milliseconds that the worker thread sleeps in various key situations and
        // after each transferred data record.
        // A higher value means "longer sleep" -> "slower data exchange".
        // A lower value means "shorter sleep" -> "faster data exchange".
        // Tweak this value to make sure that the worker thread does not hog the CPU too much.
        // If you have a CPU intensive game and see stuttering, increase this value to leave more
        // CPU for the main thread (although this will make the data exchange slower).
        // If your game is not very CPU intensive, you may be able to decrease the value (for a
        // faster data exchange) without making your main thread stutter.
        private const int SleepDuration = 12;
        private const double WaitTimeForRecentHost = 30 * MillisINMinute;

        private readonly object SYNC = new object();

        private Action mAction;
        private AfterStopDelegate mAfterStopDelegate;
        private bool mGameExiting;
        private bool mHasNewLocalEntry;
        private readonly List<SignedInGamer> mHostGamer;
        private string mHostGamertag;
        private LocalNetworkGamer mLocalGamerForSendingAndReceiving;
        private Mode mMode;
        private readonly Random mRandom;
        private readonly PacketReader mReader;
        private readonly List<HostInfo> mRecentHosts;
        private readonly Dictionary<string, string> mRecentHostsLookup;
        private NetworkGamer mRemoteGamerForSending;
        private double mServerMillisToLive;
        private NetworkSession mSession;
        private State mState;
        private IOnlineSyncTarget mSyncTarget;
        private readonly int mVersion;
        private readonly PacketWriter mWriter;

        #endregion Fields

#if SYSTEM_LINK_SESSION

        // Define SYSTEM_LINK_SESSION as a compilation symbol to compile a version that
        // uses NetworkSessionType.SystemLink (good for debugging, and tests between Xbox and PC).
        // Or leave the symbol undefined to compile a version that uses standard Xbox LIVE
        // (i.e. NetworkSessionType.PlayerMatch).
        public const NetworkSessionType SESSION_TYPE = NetworkSessionType.SystemLink;

#else

        public const NetworkSessionType SESSION_TYPE = NetworkSessionType.PlayerMatch;

#endif

        #region Constructors

        /* The "version" argument specifies the data version of the topscore data. This makes sure
         * that if you have different versions of the same game deployed, that only games with the
         * same version exchange data with each other.
         *
         * For example: In the very first implementation, simply pass "0" as the version. Then assume
         * that you have released this game to the marketplace. Now assume that after a while, you
         * decide to make some changes to the structure of the topscore lists. For example, in the
         * first version the list contained only the gamertag and score, now in the new version you
         * also store the maximum level reached with each score. This means that you have a new field
         * in your score entries.
         * When you release this game to the Xbox marketplace, it would be fatal if old versions of the
         * game (without the level field) would try to exchange Scores with new versions of the game
         * (with the level field). However, you can not guarantee that it will not happen that old and
         * new versions of the game are running at the same time, because with XBLIG, the player is
         * not required to upgrade to your latest version (he can upgrade, but is not forced to).
         * Therefore, when you prepare the new version for release, make sure to use a different
         * version number when calling this ctor. If in the old version you passed "0" as the version,
         * now pass "1" as the version.
         * If later you should have to make yet another incompatible change, increment the version
         * to "2", and so on.
         *
         * Of course you only have to do such a version increment in a new version of your game if
         * the topscore data format actually changed. If the new version has the same data format,
         * then you can just keep the previous version number. But if in doubt, it is better to
         * increment the number when releasing a new version. */

        public OnlineDataSyncManager(int version, Game game)
            : base(game)
        {
            mVersion = version;
            mHostGamer = new List<SignedInGamer>();
            mRecentHosts = new List<HostInfo>();
            mRecentHostsLookup = new Dictionary<string, string>();
            mRandom = new Random();
            mWriter = new PacketWriter();
            mReader = new PacketReader();
            Enabled = false;
            mAction = Action.NONE;
        }

        #endregion Constructors

        #region Enumerations

        public enum Mode
        {
            UNKNOWN,
            SERVER,
            CLIENT
        }

        public enum State
        {
            IDLE,
            SEND,
            RECEIVE,
            WAITForEnd
        }

        private enum Action
        {
            NONE,
            PROCESS,
            EXITTHREAD,
            DISABLE
        }

        private enum SessionProperty
        {
            VERSION
        }

        #endregion Enumerations

        #region Properties

        public SignedInGamer HostGamer
        {
            get
            {
                if (mHostGamer.Count > 0)
                    return mHostGamer[0];
                else
                    return null;
            }
        }

        public Mode MyMode
        {
            get { return mMode; }
        }

        public State MyState
        {
            get { return mState; }
        }

        public int MyVersion
        {
            get { return mVersion; }
        }

        #endregion Properties

        #region Methods

        private void clientSide_serverHasDisconnected(object sender, NetworkSessionEndedEventArgs args)
        {
            Debug.WriteLine(mMode + ": Server ended session, ending session on client side");
            EndSession();
        }

        public bool IsStopping()
        {
            return mAction == Action.EXITTHREAD || mAction == Action.DISABLE;
        }

        public void NotifyAboutNewLocalEntry()
        {
            mHasNewLocalEntry = true;
            Debug.WriteLine(mMode + ": New local entry");
            Global.SaveScores(true);
        }

        public void Run()
        {
#if XBOX360
            Thread.CurrentThread.SetProcessorAffinity(5);
#endif

            var timer = new Stopwatch();
            timer.Start();
            TimeSpan now = timer.Elapsed;
            TimeSpan lastTime;

            while (mAction == Action.PROCESS)
            {
                try
                {
                    lastTime = now;
                    now = timer.Elapsed;
                    double elapsedMillis = (now - lastTime).TotalMilliseconds;

                    if (mHasNewLocalEntry)
                    {
                        mHasNewLocalEntry = false;
                        mServerMillisToLive = 0; // this wakes up the server, should it currently be waiting
                        mRecentHosts.Clear(); // all recent hosts are now acceptable again
                        mRecentHostsLookup.Clear();
                        Debug.WriteLine(mMode + ": Waking up server and again accepting all hosts");
                    }
                    else
                    {
                        for (int i = mRecentHosts.Count - 1; i >= 0; i--)
                        {
                            HostInfo hostInfo = mRecentHosts[i];
                            hostInfo.MWaitTime -= elapsedMillis;
                            if (hostInfo.MWaitTime <= 0)
                            {
                                mRecentHosts.RemoveAt(i);
                                mRecentHostsLookup.Remove(hostInfo.MHostGamertag);
                                Debug.WriteLine(mMode + ": Excluded host accepted again: " + hostInfo.MHostGamertag);
                            }
                        }
                    }

                    switch (mState)
                    {
                        case State.IDLE:
                            switch (mMode)
                            {
                                case Mode.UNKNOWN:
                                    HandleIdleUnknown();
                                    break;
                                case Mode.SERVER:
                                    HandleIdleServer(elapsedMillis);
                                    break;
                                case Mode.CLIENT:
                                    HandleIdleClient();
                                    break;
                            }
                            break;

                        case State.SEND:
                            if (mSession != null)
                                SendNextEntry();
                            break;

                        case State.RECEIVE:
                            if (mSession != null)
                                ReceiveNextEntry();
                            break;

                        case State.WAITForEnd:
                            if (mSession != null)
                                Thread.Sleep(SleepDuration);
                            break;
                    }
                }
                catch
                {
                    Thread.Sleep(SleepDuration);
                }
            }

            if (mAction == Action.EXITTHREAD)
                mAction = Action.DISABLE;

            Debug.WriteLine(mMode + ": THREAD EXIT");
        }

        private void serverSide_clientHasDisconnected(object sender, GamerLeftEventArgs args)
        {
            Debug.WriteLine(mMode + ": Client ended session, ending session on server side");
            EndSession();
        }

        public void Start(SignedInGamer hostGamer, IOnlineSyncTarget syncTarget)
        {
            lock (SYNC)
            {
                if (mAction == Action.NONE && !mGameExiting)
                {
                    Debug.WriteLine(mMode + ": MANAGER ENABLED");
                    mAction = Action.PROCESS;
                    Enabled = true;

                    mHostGamer.Clear();
                    mHostGamer.Add(hostGamer);
                    mHostGamertag = hostGamer.Gamertag;
                    mSyncTarget = syncTarget;

                    mMode = Mode.UNKNOWN;
                    mState = State.IDLE;

                    new Thread(Run).Start();
                }
            }
        }

        public void Stop(AfterStopDelegate afterStopDelegate, bool gameExiting)
        {
            lock (SYNC)
            {
                mGameExiting = gameExiting;
                if (mAction == Action.PROCESS)
                {
                    Debug.WriteLine(mMode + ": Initiating stop");
                    mAfterStopDelegate = afterStopDelegate;
                    mAction = Action.EXITTHREAD;
                }
                else
                {
                    mAction = Action.NONE;
                    Enabled = false;
                    EndSession();
                    Debug.WriteLine(mMode + ": MANAGER DISABLED (via stop)");
                    if (afterStopDelegate != null)
                        afterStopDelegate.Invoke();
                }
            }
        }

        /* The standard Update method of a GameComponent. As for all game components, this method
         * is called once per frame by the main game thread.
         * All the other methods in this class are instead called by the worker thread which is
         * started in the "start" method (see below). */

        public override void Update(GameTime gameTime)
        {
            lock (SYNC)
            {
                if (mSession != null && !mSession.IsDisposed)
                {
                    try
                    {
                        mSession.Update();
                    }
                    catch
                    {
                    }
                }

                if (mAction == Action.DISABLE &&
                    (mSession == null || mSession.IsDisposed || mSession.BytesPerSecondSent == 0))
                {
                    mAction = Action.NONE;
                    Enabled = false;
                    EndSession();
                    Debug.WriteLine(mMode + ": MANAGER DISABLED");
                    if (mAfterStopDelegate != null)
                        mAfterStopDelegate.Invoke();
                }
            }
        }

        private void DisableVoice()
        {
            foreach (LocalNetworkGamer localGamer in mSession.LocalGamers)
            {
                foreach (NetworkGamer remoteGamer in mSession.RemoteGamers)
                    localGamer.EnableSendVoice(remoteGamer, false);
            }
            Debug.WriteLine(mMode + ", " + mState + ": Voice disabled");
        }

        private void EndSession()
        {
            // used both by server and client
            lock (SYNC)
            {
                if (mSession != null && !mSession.IsDisposed)
                {
                    mSession.Dispose();
                    mSyncTarget.EndSynchronization();
                    Debug.WriteLine(mMode + ": Session disposed");
                }
                mSession = null;
                mMode = Mode.UNKNOWN;
                mState = State.IDLE;
            }
        }

        private bool FindServer()
        {
            Debug.WriteLine(mMode + ": Looking for server");

            var searchProperties = new NetworkSessionProperties();
            searchProperties[(int)SessionProperty.VERSION] = mVersion;

            AvailableNetworkSessionCollection availableSessions;
            try
            {
                availableSessions = NetworkSession.Find(SESSION_TYPE, mHostGamer, searchProperties);
            }
            catch (Exception)
            {
                return false;
            }

            foreach (AvailableNetworkSession availableSession in availableSessions)
            {
                Debug.WriteLine(mMode + ": Server found: " + availableSession.HostGamertag);
                if (!mRecentHostsLookup.ContainsKey(availableSession.HostGamertag))
                {
                    NetworkSession session; // use local variable for sync reasons
                    try
                    {
                        session = NetworkSession.Join(availableSession);
                    }
                    catch (Exception)
                    {
                        Debug.WriteLine(mMode + ": Server went away during join");
                        return false;
                    }
                    session.SessionEnded += clientSide_serverHasDisconnected;
                    session.LocalGamers[0].IsReady = true;
                    mMode = Mode.CLIENT;
                    mSession = session; // only assign to mSession after all parameters are set
                    mLocalGamerForSendingAndReceiving = null;
                    mRemoteGamerForSending = null;
                    DisableVoice();
                    Debug.WriteLine(mMode + ": Server joined: " + availableSession.HostGamertag);
                    return true;
                }
                else
                    Debug.WriteLine(mMode + ": Server skipped as excluded: " + availableSession.HostGamertag);
            }
            return false;
        }

        private void HandleIdleClient()
        {
            if (mSession != null)
            {
                switch (mSession.SessionState)
                {
                    case NetworkSessionState.Lobby:
                        Thread.Sleep(SleepDuration);
                        break;
                    case NetworkSessionState.Playing:
                        Debug.WriteLine(mMode + ": Transfer started, switching to send mode");
                        mSyncTarget.StartSynchronization();
                        mSyncTarget.PrepareForSending();
                        mState = State.SEND;
                        break;
                    case NetworkSessionState.Ended:
                        Debug.WriteLine(mMode + ": Session ended externally");
                        EndSession();
                        break;
                }
            }
        }

        private void HandleIdleServer(double elapsedMillis)
        {
            if (mSession != null)
            {
                switch (mSession.SessionState)
                {
                    case NetworkSessionState.Lobby:
                        mServerMillisToLive -= elapsedMillis;
                        if (mServerMillisToLive > 0)
                        {
                            if (mSession.RemoteGamers.Count == 1 && mSession.IsEveryoneReady)
                            {
                                Debug.WriteLine(mMode + ": Client has connected, starting transfer");
                                mSyncTarget.StartSynchronization();
                                mState = State.RECEIVE;
                                DisableVoice();
                                mSession.StartGame();
                            }
                            else
                                Thread.Sleep(SleepDuration);
                        }
                        else
                        {
                            Debug.WriteLine(mMode + ": Server timed out");
                            EndSession();
                        }
                        break;
                    case NetworkSessionState.Ended:
                        Debug.WriteLine(mMode + ": Session ended externally");
                        EndSession();
                        break;
                    case NetworkSessionState.Playing:
                        // Never Happens
                        break;
                }
            }
        }

        private void HandleIdleUnknown()
        {
            if (!FindServer())
                StartServerSession();
        }

        private void ReceiveNextEntry()
        {
            if (mLocalGamerForSendingAndReceiving == null)
            {
                GamerCollection<LocalNetworkGamer> localGamers = mSession.LocalGamers;
                if (localGamers.Count > 0)
                    mLocalGamerForSendingAndReceiving = localGamers[0];
                return;
            }
            else if (mLocalGamerForSendingAndReceiving.IsDataAvailable)
            {
                NetworkGamer sender;
                mLocalGamerForSendingAndReceiving.ReceiveData(mReader, out sender);
                Debug.Assert(!sender.IsLocal);
                bool wasLastRecord = mSyncTarget.ReadTransferRecord(mReader);
                if (wasLastRecord)
                {
                    Debug.WriteLine(mMode + ": Excluding server " + sender.Gamertag + " for the next " +
                                    (WaitTimeForRecentHost / 1000.0) + "sec");
                    mRecentHosts.Add(new HostInfo(sender.Gamertag, WaitTimeForRecentHost));
                    mRecentHostsLookup[sender.Gamertag] = "";
                    if (mMode == Mode.SERVER)
                    {
                        Debug.WriteLine(mMode + ": Receiving completed, switching to send mode");
                        mSyncTarget.PrepareForSending();
                        mState = State.SEND;
                    }
                    else
                    {
                        Debug.WriteLine(mMode + ": Receiving completed, ending session on client side");
                        EndSession();
                    }
                }
            }

            // Sleep before returning
            Thread.Sleep(SleepDuration);
        }

        private void SendNextEntry()
        {
            if (mSession.BytesPerSecondSent == 0)
            {
                if (mLocalGamerForSendingAndReceiving == null)
                {
                    GamerCollection<LocalNetworkGamer> localGamers = mSession.LocalGamers;
                    if (localGamers.Count > 0)
                        mLocalGamerForSendingAndReceiving = localGamers[0];
                    return;
                }
                else if (mRemoteGamerForSending == null)
                {
                    GamerCollection<NetworkGamer> remoteGamers = mSession.RemoteGamers;
                    if (remoteGamers.Count > 0)
                    {
                        mRemoteGamerForSending = remoteGamers[0];
                        DisableVoice();
                    }
                    return;
                }
                else
                {
                    bool wasLastRecord = mSyncTarget.WriteTransferRecord(mWriter);
                    mLocalGamerForSendingAndReceiving.SendData(mWriter, SendDataOptions.ReliableInOrder,
                                                               mRemoteGamerForSending);
                    Thread.Sleep(5);

                    if (wasLastRecord)
                    {
                        if (mMode == Mode.SERVER)
                        {
                            Debug.WriteLine(mMode + ": Sending completed, waiting for end from client");
                            mState = State.WAITForEnd;
                        }
                        else
                        {
                            Debug.WriteLine(mMode + ": Sending completed, switching to receive mode");
                            mState = State.RECEIVE;
                        }
                    }
                }
            }

            // Sleep before returning
            Thread.Sleep(SleepDuration);
        }

        private void StartServerSession()
        {
            Debug.WriteLine(mMode + ": Starting session");

            var sessionProperties = new NetworkSessionProperties();
            sessionProperties[(int)SessionProperty.VERSION] = mVersion;

            NetworkSession session; // use local variable for sync reasons
            try
            {
                session = NetworkSession.Create(SESSION_TYPE, mHostGamer, 2, 0, sessionProperties);
            }
            catch (Exception)
            {
                return;
            }
            session.AllowHostMigration = false;
            session.AllowJoinInProgress = false;
            session.GamerLeft += serverSide_clientHasDisconnected;
            session.LocalGamers[0].IsReady = true;

            mServerMillisToLive = mRandom.Next(MinimumServerTime, MaximumServerTime);
            mMode = Mode.SERVER;
            mSession = session; // only assign to mSession after all parameters are set
            mLocalGamerForSendingAndReceiving = null;
            mRemoteGamerForSending = null;
            Debug.WriteLine(mMode + ": Server time to live: " + (mServerMillisToLive / 1000.0) + "sec");
        }

        #endregion Methods

        #region Nested Types

        private class HostInfo
        {
            #region Fields

            public readonly string MHostGamertag;
            public double MWaitTime;

            #endregion Fields

            #region Constructors

            public HostInfo(string hostGamertag, double waitTime)
            {
                MHostGamertag = hostGamertag;
                MWaitTime = waitTime;
            }

            #endregion Constructors
        }

        #endregion Nested Types
    }
}