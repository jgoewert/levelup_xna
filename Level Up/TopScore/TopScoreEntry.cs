#region Header
// --------------------------------
// <copyright file="TopScoreEntry.cs" company="John Goewert">
//     Copyright John Goewert 2011.
// </copyright>
// <author>John Goewert</author>
// ---------------------------------


/*
Copyright (c) 2010 Spyn Doctor Games (Johannes Hubert). All rights reserved.

Redistribution and use in binary forms, with or without modification, and for whatever
purpose (including commercial) are permitted. Atribution is not required. If you want
to give attribution, use the following text and URL (may be translated where required):
        Uses source code by Spyn Doctor Games - http://www.spyn-doctor.de

Redistribution and use in source forms, with or without modification, are permitted
provided that redistributions of source code retain the above copyright notice, this
list of conditions and the following disclaimer.

THIS SOFTWARE IS PROVIDED BY SPYN DOCTOR GAMES (JOHANNES HUBERT) "AS IS" AND ANY EXPRESS
OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
SPYN DOCTOR GAMES (JOHANNES HUBERT) OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Last change: 2010-09-16
*/

#endregion Header

using System.IO;
using System.Diagnostics;

namespace SpynDoctor
{
    public class TopScoreEntry
    {
        #region Fields

        private readonly string mClass;
        private readonly string mGamertag;

        // Local (true) means: This score was in the list before the latest score transfer.
        // Non-local (false) means: This score was freshly transferred into the list from another Xbox.
        private readonly string mItem;
        private readonly string mLevel;
        private readonly string mPlayerName;
        private readonly string mPlot;
        private readonly string mRace;
        private readonly string mSpell;
        private readonly string mStat;
        private readonly long mTimeStamp;
        // TODO Add Motto
        // private string mMotto;

        #endregion Fields

        #region Constructors

        public TopScoreEntry(string gamertag, long timestamp, string playername, string race, string playerclass,
                             string level, string stat, string plot, string item, string spell)
        {
            // freshly created local entry
            mGamertag = gamertag;
            mTimeStamp = timestamp;
            mPlayerName = playername;
            mRace = race;
            mClass = playerclass;
            mLevel = level;
            mStat = stat;
            mPlot = plot;
            mItem = item;
            mSpell = spell;
            // TODO Add Motto
            // mMotto = motto
            IsLocalEntry = true;
        }

        public TopScoreEntry(BinaryReader reader, bool isLocalEntry)
        {
            // isLocalEntry == true: local entry read from storage
            // isLocalEntry == false: entry from remote source read from online transfer
            try
            {
                mGamertag = reader.ReadString();
                mTimeStamp = reader.ReadInt64();
                mPlayerName = reader.ReadString();
                mRace = reader.ReadString();
                mClass = reader.ReadString();
                mLevel = reader.ReadString();
                mStat = reader.ReadString();
                mPlot = reader.ReadString();
                mItem = reader.ReadString();
                mSpell = reader.ReadString();
                // TODO Add Motto
                // mMotto = reader.ReadString();
                IsLocalEntry = isLocalEntry;
            }
            catch (IOException e)
            {
                Debug.WriteLine(e.Message);
            }
    }

        #endregion Constructors

        #region Properties

        public string Class
        {
            get { return mClass; }
        }

        public string Gamertag
        {
            get { return mGamertag; }
        }

        public bool IsLocalEntry { get; set; }

        public string Item
        {
            get { return mItem; }
        }

        public string Level
        {
            get { return mLevel; }
        }

        // TODO Add Motto
        /*
        public string Motto
        {
            get { return mMotto; }
        }
                 * */
        public string PlayerName
        {
            get { return mPlayerName; }
        }


        public string Plot
        {
            get { return mPlot; }
        }

        public string Race
        {
            get { return mRace; }
        }

        public int RankAtLastPageFill { get; set; }

        public string Spell
        {
            get { return mSpell; }
        }

        public string Stat
        {
            get { return mStat; }
        }

        public long TimeStamp
        {
            get { return mTimeStamp; }
        }

        #endregion Properties

        #region Methods

        public int CompareTo(TopScoreEntry other)
        {
            if (mTimeStamp < other.mTimeStamp) // lower score is lower in rank
                return -1;
            else if (mTimeStamp > other.mTimeStamp) // higher score is higher in rank
                return 1;
            else // same score, same rank
                return 0;
        }

        public bool IsLegal()
        {
            if (mGamertag == null || mGamertag.Length < 1 || mGamertag.Length > 15 || mTimeStamp < 0)
                return false;
            for (int i = 0; i < mGamertag.Length; i++)
            {
                char c = mGamertag[i];
                if (!(c == ' ' || c >= 'A' && c <= 'Z' || c >= 'a' && c <= 'z' || c >= '0' && c <= '9'))
                    return false;
            }
            return true;
        }

        public void Write(BinaryWriter writer)
        {
            writer.Write(mGamertag);
            writer.Write(mTimeStamp);
            writer.Write(mPlayerName);
            writer.Write(mRace);
            writer.Write(mClass);
            writer.Write(mLevel);
            writer.Write(mStat);
            writer.Write(mPlot);
            writer.Write(mItem);
            writer.Write(mSpell);
            // mRankAtLastPageFill is not saved by design!
        }

        #endregion Methods
    }
}