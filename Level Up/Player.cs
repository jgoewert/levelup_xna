﻿#region Header
// --------------------------------
// <copyright file="Player.cs" company="John Goewert">
//     Copyright John Goewert 2011.
// </copyright>
// <author>John Goewert</author>
// ---------------------------------
#endregion

using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using System.Globalization;
#if DEBUG || XBOX
using Microsoft.Xna.Framework.GamerServices;
#endif

namespace Level_Up
{
    public sealed class Player
    {
        #region Fields

        private static volatile Player instance;
        private static readonly object syncRoot = new Object();

        private readonly Global global;
        private SaveGameData _data; // ENCAPSULATE FIELD BY CODEIT.RIGHT

        public SaveGameData Data
        {
            get
            {
                return _data;
            }
            set
            {
                _data = value;
            }
        }
        private GameObject _gameObject; // ENCAPSULATE FIELD BY CODEIT.RIGHT

        public GameObject GameObject
        {
            get
            {
                return _gameObject;
            }
            set
            {
                _gameObject = value;
            }
        }

        // string bestplot;
        private string _questmonster; // ENCAPSULATE FIELD BY CODEIT.RIGHT

        public string Questmonster
        {
            get
            {
                return _questmonster;
            }
            set
            {
                _questmonster = value;
            }
        }
        private int _questmonsterindex; // ENCAPSULATE FIELD BY CODEIT.RIGHT

        public int Questmonsterindex
        {
            get
            {
                return _questmonsterindex;
            }
            set
            {
                _questmonsterindex = value;
            }
        }
        private List<QueueItem> _queue = new List<QueueItem>(); // ENCAPSULATE FIELD BY CODEIT.RIGHT

        public List<QueueItem> Queue
        {
            get
            {
                return _queue;
            }
            set
            {
                _queue = value;
            }
        }

        #endregion Fields

        #region Constructors

        private Player()
        {
        }

        #endregion Constructors

        #region Properties

        public static Player Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncRoot)
                        if (instance == null)
                            instance = new Player();
                }
                return instance;
            }
        }

        #endregion Properties

        #region Methods

        public int GetMax(int[] thearray)
        {
            int max = 0;
            for (int i = 1; i < thearray.Length; i++)
            {
                if (thearray[i] > thearray[max]) max = i;
            }
            return max;
        }

        public void NewCharacter(string name, string race, string Class, int[] stats, PlayerIndex index)
        {
            Data = new SaveGameData();
            Data.Birth = DateTime.Now;
            Data.LastSave = DateTime.Now;
#if DEBUG || XBOX
            Data.Gamertag = Gamer.SignedInGamers[Global.CurrentPlayer].Gamertag;
#else
            Data.Gamertag = "LocalPlayer";
#endif
            Data.Traits = new[] { name, race, Class, "1" };

            Data.Stats = new string[Global.GetEnumNames<Global.Stat>().Length];
            for (int statCounter = 0; statCounter < Global.GetEnumNames<Global.Stat>().Length; statCounter++)
            {
                Data.Stats[statCounter] = stats[statCounter].ToString(CultureInfo.CurrentCulture);
            }

            Data.Equipment = new[] { "Sharp Rock" };

            Data.Spells = null;
            Data.Spellsvalue = null;

            Data.Plot = new[] { "Prologue" };

            Data.Quests = null;

            Data.Items = new[] { "Gold" };
            Data.Itemsvalue = new[] { "0" };
            Data.Itemcount = 0;

            Data.ExperiencePercentage = 0;
            Data.ExperienceMax = 120;
            Data.ExperienceTotal = 0;

            // QuestBar: { position: 0, max: 1 },
            Data.QuestsPercentage = 0;
            Data.QuestsTotal = 0;
            Data.QuestsMax = 50 + Global.RandomValue.Next(1000);

            // PlotBar: { position: 0, max: 26 },
            Data.PlotPercentage = 0;
            Data.PlotTotal = 0;
            Data.PlotMax = 20; // Prologue is short

            Data.Questmonster = "";

            Queue.Add(new QueueItem("task", 10, "Experiencing an enigmatic and foreboding night vision"));
            Queue.Add(new QueueItem("task", 6, "Much is revealed about that wise old bastard you'd underestimated"));
            Queue.Add(new QueueItem("task", 6,
                                    "A shocking series of events leaves you alone and bewildered, but resolute"));
            Queue.Add(new QueueItem("task", 4,
                                    "Drawing upon an unrealized reserve of courage, you set out on a long and dangerous journey"));

            Queue.Add(new QueueItem("task", 2, "Loading"));
            Queue.Add(new QueueItem("quest", 0, "Advance"));
        }

        #endregion Methods

        #region Nested Types

        #region Nested type: Item

        public struct Item
        {
            #region Fields

            public string Extra;
            public string Name;
            public string Type;
            public int Value;

            #endregion Fields

            #region Constructors

            public Item(string name, int value, string extra, string type)
            {
                this.Name = name;
                this.Value = value;
                this.Extra = extra;
                this.Type = type;
            }

            #endregion Constructors

            public override int GetHashCode()
            {
                throw new NotImplementedException();
            }

            public override bool Equals(Object obj)
            {
                throw new NotImplementedException();
            }

            public static bool operator ==(Item x, Item y)
            {
                throw new NotImplementedException();
            }

            public static bool operator !=(Item x, Item y)
            {
                throw new NotImplementedException();
            }
        }

        #endregion

        #region Nested type: QueueItem

        public struct QueueItem
        {
            #region Fields

            public string Action;
            public string Text;
            public int Value;

            #endregion Fields

            #region Constructors

            public QueueItem(string action, int value, string text)
            {
                this.Action = action;
                this.Value = value;
                this.Text = text;
            }

            #endregion Constructors

            public override int GetHashCode()
            {
                throw new NotImplementedException();
            }

            public override bool Equals(Object obj)
            {
                throw new NotImplementedException();
            }

            public static bool operator ==(QueueItem x, QueueItem y)
            {
                throw new NotImplementedException();
            }

            public static bool operator !=(QueueItem x, QueueItem y)
            {
                throw new NotImplementedException();
            }
        }

        #endregion

        #region Nested type: Task

        public struct Task
        {
            #region Fields

            public string Name;
            public bool Value;

            #endregion Fields

            #region Constructors

            public Task(string name, bool value)
            {
                this.Name = name;
                this.Value = value;
            }

            #endregion Constructors

            public override int GetHashCode()
            {
                throw new NotImplementedException();
            }

            public override bool Equals(Object obj)
            {
                throw new NotImplementedException();
            }

            public static bool operator ==(Task x, Task y)
            {
                throw new NotImplementedException();
            }

            public static bool operator !=(Task x, Task y)
            {
                throw new NotImplementedException();
            }
        }

        #endregion

        #endregion Nested Types
    }
}