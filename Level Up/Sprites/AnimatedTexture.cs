﻿#region Header
// --------------------------------
// <copyright file="AnimatedTexture.cs" company="John Goewert">
//     Copyright John Goewert 2011.
// </copyright>
// <author>John Goewert</author>
// ---------------------------------
#endregion

namespace Sprites
{
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Content;
    using Microsoft.Xna.Framework.Graphics;

    /// <summary>
    /// Animated Textures
    /// </summary>
    public class AnimatedTexture
    {
        #region Fields

        /// <summary>
        /// The Current Frame to Draw
        /// </summary>
        private int frame;

        /// <summary>
        /// Is the Sprite paused?
        /// </summary>
        private bool paused;

        /// <summary>
        /// Time since the last frame to determine when to move to the next frame
        /// </summary>
        private float timePerFrame;

        /// <summary>
        /// Total time elapsed
        /// </summary>
        private float totalElapsed;

        /// <summary>
        /// Number of frames in the animation
        /// </summary>
        private int frameCount;

        #endregion Fields

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="AnimatedTexture"/> class.
        /// </summary>
        /// <param name="origin">The origin.</param>
        /// <param name="rotation">The rotation.</param>
        /// <param name="scale">The scale.</param>
        /// <param name="depth">The depth.</param>
        public AnimatedTexture(Vector2 origin, float rotation, float scale, float depth)
        {
            this.Origin = origin;
            this.Rotation = rotation;
            this.Scale = scale;
            this.Depth = depth;
        }

        #endregion Constructors

        #region Properties

        /// <summary>
        /// Gets a value indicating whether this instance is paused.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is paused; otherwise, <c>false</c>.
        /// </value>
        public bool IsPaused
        {
            get { return this.paused; }
        }

        /// <summary>
        /// Gets or sets the depth.
        /// </summary>
        /// <value>
        /// The depth.
        /// </value>
        public float Depth { get; set; }

        /// <summary>
        /// Gets or sets the height.
        /// </summary>
        /// <value>
        /// The height.
        /// </value>
        public int Height { get; set; }

        /// <summary>
        /// Gets or sets the origin.
        /// </summary>
        /// <value>
        /// The origin.
        /// </value>
        public Vector2 Origin { get; set; }

        /// <summary>
        /// Gets or sets the scale.
        /// </summary>
        /// <value>
        /// The scale.
        /// </value>
        public float Scale { get; set; }

        /// <summary>
        /// Gets or sets the width.
        /// </summary>
        /// <value>
        /// The width.
        /// </value>
        public int Width { get; set; }

        /// <summary>
        /// Gets or sets the rotation.
        /// </summary>
        /// <value>
        /// The rotation.
        /// </value>
        public float Rotation { get; set; }

        /// <summary>
        /// Gets or sets the rectangle.
        /// </summary>
        /// <value>
        /// The rectangle.
        /// </value>
        public Rectangle Rect { get; set; }

        /// <summary>
        /// Gets or sets the texture.
        /// </summary>
        /// <value>
        /// The texture.
        /// </value>
        public Texture2D Texture { get; set; }

        #endregion Properties

        #region Methods

        /// <summary>
        /// Draws the frame.
        /// </summary>
        /// <param name="batch">The batch.</param>
        /// <param name="screenPos">The screen pos.</param>
        /// <param name="rotation">The rotation.</param>
        public void DrawFrame(SpriteBatch batch, Vector2 screenPos, float rotation)
        {
            this.DrawFrame(batch, this.frame, screenPos, rotation);
        }

        /// <summary>
        /// Draws the frame.
        /// </summary>
        /// <param name="batch">The batch.</param>
        /// <param name="frame">The frame.</param>
        /// <param name="screenPos">The screen pos.</param>
        /// <param name="rotation">The rotation.</param>
        public void DrawFrame(SpriteBatch batch, int frame, Vector2 screenPos, float rotation)
        {
            int frameWidth = this.Texture.Width / this.frameCount;
            var sourcerect = new Rectangle(frameWidth * frame, 0, frameWidth, this.Texture.Height);
            batch.Draw(this.Texture, screenPos, sourcerect, Color.White, rotation, this.Origin, this.Scale, SpriteEffects.None, this.Depth);
        }

        /// <summary>
        /// Loads the specified content.
        /// </summary>
        /// <param name="content">The content.</param>
        /// <param name="asset">The asset.</param>
        /// <param name="frameTotal">The frame count.</param>
        /// <param name="framesPerSec">The frames per sec.</param>
        public void Load(ContentManager content, string asset, int frameTotal, int framesPerSec)
        {
            this.frameCount = frameTotal;
            this.Texture = content.Load<Texture2D>(asset);
            this.timePerFrame = (float)1 / framesPerSec;
            this.frame = 0;
            this.totalElapsed = 0;
            this.paused = false;
            this.Origin = new Vector2((this.Texture.Width / 2) / this.frameCount, this.Texture.Height / 2);
            this.Width = this.Texture.Width / this.frameCount;
            this.Height = this.Texture.Height;
        }

        /// <summary>
        /// Pauses this instance.
        /// </summary>
        public void Pause()
        {
            this.paused = true;
        }

        /// <summary>
        /// Plays this instance.
        /// </summary>
        public void Play()
        {
            this.paused = false;
        }

        /// <summary>
        /// Resets this instance.
        /// </summary>
        public void Reset()
        {
            this.frame = 0;
            this.totalElapsed = 0f;
        }

        /// <summary>
        /// Stops this instance.
        /// </summary>
        public void Stop()
        {
            this.Pause();
            this.Reset();
        }

        /// <summary>
        /// Updates the frame.
        /// </summary>
        /// <param name="elapsed">The elapsed.</param>
        public void UpdateFrame(float elapsed)
        {
            if (this.paused)
            {
                return;
            }

            this.totalElapsed += elapsed;
            if (this.timePerFrame < this.totalElapsed)
            {
                this.frame++;
                
                // Keep the Frame between 0 and the total frames, minus one.
                this.frame = this.frame % this.frameCount;
                this.totalElapsed -= this.timePerFrame;
            }
        }

        #endregion Methods        
    }
}