﻿#region Header
// --------------------------------
// <copyright file="ScrollingBackground.cs" company="John Goewert">
//     Copyright John Goewert 2011.
// </copyright>
// <author>John Goewert</author>
// ---------------------------------
#endregion

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace LevelUp
{
    public class ScrollingBackground
    {
        #region Fields

        // class ScrollingBackground

        private Texture2D mytexture;
        private Vector2 _origin; // ENCAPSULATE FIELD BY CODEIT.RIGHT

        public Vector2 Origin
        {
            get
            {
                return _origin;
            }
            set
            {
                _origin = value;
            }
        }
        private int screenheight;
        private Vector2 _screenpos; // ENCAPSULATE FIELD BY CODEIT.RIGHT

        public Vector2 Screenpos
        {
            get
            {
                return _screenpos;
            }
            set
            {
                _screenpos = value;
            }
        }
        private int screenwidth;
        private Vector2 _texturesize; // ENCAPSULATE FIELD BY CODEIT.RIGHT

        public Vector2 Texturesize
        {
            get
            {
                return _texturesize;
            }
            set
            {
                _texturesize = value;
            }
        }

        #endregion Fields

        #region Methods

        // ScrollingBackground.Draw
        public void Draw(SpriteBatch batch)
        {
            // Draw the texture, if it is still onscreen.
            if (Screenpos.X < screenwidth / 2)
            {
                batch.Draw(mytexture, Screenpos, null,
                           Color.White, 0, Origin, 1, SpriteEffects.None, 0f);
            }
            // Draw the texture a second time, behind the first,
            // to create the scrolling illusion.
            batch.Draw(mytexture, Screenpos + Texturesize, null,
                       Color.White, 0, Origin, 1, SpriteEffects.None, 0f);
        }

        public void Load(GraphicsDevice device, Texture2D backgroundTexture)
        {
            mytexture = backgroundTexture;
            screenheight = device.Viewport.Height;
            screenwidth = device.Viewport.Width;
            // Set the origin so that we're drawing from the
            // center of the top edge.
            Origin = new Vector2(mytexture.Width / 2, mytexture.Height / 2);
            // Set the screen position to the center of the screen.
            // screenpos = new Vector2(screenwidth / 2, screenheight / 2);
            // Offset to draw the second texture, when necessary.
            Texturesize = new Vector2(mytexture.Width, 0);
        }

        // ScrollingBackground.Update
        public void Update(float deltaX)
        {
            Screenpos = new Vector2(Screenpos.X - deltaX, Screenpos.Y);
            if (Screenpos.X < (-mytexture.Width / 2))
            {
                Screenpos = new Vector2(Screenpos.X + mytexture.Width, Screenpos.Y);
            }

        }

        #endregion Methods
    }
}