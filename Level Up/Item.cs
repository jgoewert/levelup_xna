﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Level_Up
{
    public class Item
    {

        public string[] Extra { get; set; }
        public string Name { get; set; }
        public int Value { get; set; }
        
        public  Item()
        {
            Name = "";
            /*Value = 0*/
            ;
            Extra = new[] { "" };
        }

        public  Item(string name, int value)
        {
            this.Name = name;
            this.Value = value;
            Extra = new[] { "" };
        }

        public  Item(string name, int value, string[] extra)
        {
            this.Name = name;
            this.Value = value;
            this.Extra = extra;
        }
    }
}
