﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Media;
#if DEBUG || XBOX
using Microsoft.Xna.Framework.GamerServices;
#endif

namespace GameStateManagement
{

    /// <summary>
    /// class for all splash-screens like background, credits,
    /// instructions, winning and so on
    /// </summary>
    internal class CreditsScreen : GameScreen
    {
#if DEBUG
        private float maxTime = 0.0f;
#else
        private float maxTime = 2.0f;
#endif
        private float timer = 0.0f;
        private List<Texture2D> backgrounds;
        int counter = 0;

        //value by default: half-transparent white
        public Color backgroundColor;

        public CreditsScreen()
        {
            timer = maxTime;
            backgrounds = new List<Texture2D>();
            backgroundColor = new Color(255.0f, 255.0f, 255.0f);
        }

        #region Methods
        public override void Update(GameTime gameTime, bool otherScreenHasFocus, bool coveredByOtherScreen)
        {
            
            base.Update(gameTime, otherScreenHasFocus, coveredByOtherScreen);
            var elapsed = (float) gameTime.ElapsedGameTime.TotalSeconds;
            timer -= elapsed;
            if (timer <= 0)
            {
                timer = maxTime;
                if (counter < backgrounds.Count)
                {
                    counter++;
                }
                else
                {
                    LoadingScreen.Load(ScreenManager, false, null, new BackgroundScreen(), new TitleScreen());
                    this.ExitScreen();
                }
            }
        }

        public override void Draw(GameTime gameTime)
        {
            if (counter < backgrounds.Count)
            {
                ScreenManager.SpriteBatch.Begin();
                ScreenManager.SpriteBatch.Draw(backgrounds[counter],
                                               new Rectangle(0, 0, ScreenManager.GraphicsDevice.Viewport.Width,
                                                             ScreenManager.GraphicsDevice.Viewport.Height), null,
                                               backgroundColor);
                ScreenManager.SpriteBatch.End();
            }
            return;
        }

        public override void LoadContent()
        {
            MediaPlayer.IsMuted = true;
            //ContentManager content = new ContentManager(ScreenManager.Game.Services, "Content");
            backgrounds.Add(Global.content.Load<Texture2D>("Images/splash0"));
            backgrounds.Add(Global.content.Load<Texture2D>("Images/splash1"));
            backgrounds.Add(Global.content.Load<Texture2D>("Images/splash2"));
#if DEBUG || XBOX
            if (Guide.IsTrialMode)
            {
                backgrounds.Add(Global.content.Load<Texture2D>("Images/purchase"));
            }
#endif
        }


        #endregion
    }
}