﻿#region Header
// --------------------------------
// <copyright file="NewGameScreen.cs" company="John Goewert">
//     Copyright John Goewert 2011.
// </copyright>
// <author>John Goewert</author>
// ---------------------------------
#endregion


using System;
using System.Collections.Generic;
using System.Diagnostics;
using Level_Up;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using OrbUI.Components;
using OrbUI.Components.GUI;
using OrbUI.Components.GUI.Controls;
using OrbUI.Components.Input.Controller;
using System.Globalization;
#if DEBUG || XBOX
using Microsoft.Xna.Framework.GamerServices;
#endif

namespace GameStateManagement
{
    /// <summary>
    /// This screen implements the actual game logic. It is just a
    /// placeholder to get the idea across: you'll probably want to
    /// put some more interesting gameplay in here!
    /// </summary>

    internal class NewGameScreen : MenuScreen
    {
        #region Fields

        // private Button btnUnRoll;
        private double ignoreControlTimer = 0.25;
        private bool ignoreControls;
        private float pauseAlpha;
        private bool setfocus = true;

        private Form frmNewChar;
        private Button btnGenerateName;
        private Button btnRoll;
        private Button btnSold;
        private ListView lvNewStats;
        private Label lblName;
        private RadioButtonGroup optClass;
        private RadioButtonGroup optRace;
        private TextBox txtName;
        private OnScreenKeyboard frmNewGameKeyboard;
        private Button btnExit;

        #endregion Fields

       
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the NewGameScreen class.
        /// </summary>
        public NewGameScreen() : base("New Game")
        {
            TransitionOnTime = TimeSpan.FromSeconds(1.5);
            TransitionOffTime = TimeSpan.FromSeconds(0.5);
            
            if (frmNewChar.IsNullOrDisposed())
            {
                InitNewGameMenu();
            }


            frmNewGameKeyboard.Controls["btnEnter"].CursorUp += CloseKeyboard;
            btnSold.CursorUp += btnSoldPressed;

            CloseKeyboard(null);

            frmNewChar.Visible = true;
            frmNewChar.Enabled = true;

        }

        #endregion Constructors
        
        #region Methods

        protected override void OnSelectEntry(int entryIndex, PlayerIndex playerIndex)
        {
        }

        private void InitNewGameMenu()
        {
            frmNewChar = new Form("frmNewChar", "", new Point(245, 100), new Point(810, 530),
                                          FormBorderStyle.None);
            frmNewChar.DragAnywhere = false;
            frmNewChar.ShowTitleBar = false;
            frmNewChar.FontName = "gamefont";
            frmNewChar.TitleTextColor = Color.White;
            frmNewChar.Show();

            lblName = new Label("lblName", new Point(15, 20), new Point(100, 30), "Name", Color.White);
            frmNewChar.Controls.Add(lblName);

            txtName = new TextBox("txtName", new Point(115, 20),
                                  new Point(300, frmNewChar.Font.Height() + 5));
            txtName.Text = Global.GenerateName();
            txtName.TextChanged += delegate
            {
                if (txtName.Text.Length > 10)
                {
                    txtName.Text = txtName.Text.Remove(10);
                }
            };
            frmNewChar.Controls.Add(txtName);

            btnGenerateName = new Button("btnGenerateName", new Point(430, 20), new Point(30, 30),
                                         "?", Color.Black);
            btnGenerateName.CursorUp += delegate { txtName.Text = Global.GenerateName(); };

            frmNewChar.Controls.Add(btnGenerateName);

            List<RadioButton> lstRaceButtons;
            OrbListManager.CheckOut(out lstRaceButtons);
            int counter = 0;
            foreach (Item race in Global.Races)
            {
                lstRaceButtons.Add(new RadioButton("btnRace" + counter, race.Name, new Point(0, 20 * counter),
                                                   new Point(320, 30), false));
                counter++;
            }
            optRace = new RadioButtonGroup("optRace", new Point(15, 50),
                                           new Point(320, 20 * counter + counter), lstRaceButtons,
                                           lstRaceButtons[Global.RandomValue.Next(Global.Races.Length)]);
            frmNewChar.Controls.Add(optRace);
            OrbListManager.CheckIn(ref lstRaceButtons);

            List<RadioButton> lstClassButtons;
            OrbListManager.CheckOut(out lstClassButtons);
            counter = 0;
            foreach (Item currentClass1 in Global.Classes)
            {
                lstClassButtons.Add(new RadioButton("btnClass" + currentClass1.Name, currentClass1.Name,
                                                    new Point(0, 20 * counter), new Point(255, 30), false));
                counter++;
            }
            optClass = new RadioButtonGroup("optClass", new Point(330, 50),
                                            new Point(255, 20 * counter + counter), lstClassButtons,
                                            lstClassButtons[Global.RandomValue.Next(Global.Classes.Length)]);
            frmNewChar.Controls.Add(optClass);

            OrbListManager.CheckIn(ref lstRaceButtons);

            lvNewStats = new ListView("lvNewStats", new Point(595, 50), new Point(190, 195));
            lvNewStats.Columns.Add(new ListViewHeader("Stat", 113));
            lvNewStats.Columns.Add(new ListViewHeader("Val", 75));
            lvNewStats.FontName = "gamefont";
            lvNewStats.Enabled = false;
            frmNewChar.Controls.Add(lvNewStats);

            string[] row = null;

            foreach (string stat in Global.GetEnumNames<Global.Stat>())
            {
                row = new string[2];
                row[0] = stat;
                row[1] = ((Global.RandomValue.Next(6) + 1) + (Global.RandomValue.Next(6) + 1) + (Global.RandomValue.Next(6) + 1)).ToString(CultureInfo.CurrentCulture);
                lvNewStats.Rows.Add(row);
            }

            lvNewStats.EndUpdate();

            btnRoll = new Button("btnRoll", new Point(595, 260), "Roll", Color.Black);
            btnRoll.CursorUp += delegate
            {
                lvNewStats.BeginUpdate();

                for (int newStatCounter = 0;
                     newStatCounter < lvNewStats.Rows.Count;
                     newStatCounter++)
                {
                    lvNewStats.Rows[newStatCounter][1] =
                        ((Global.RandomValue.Next(6) + 1) + (Global.RandomValue.Next(6) + 1) + (Global.RandomValue.Next(6) + 1))
                            .ToString(CultureInfo.CurrentCulture);
                }
                lvNewStats.EndUpdate();
            };
            frmNewChar.Controls.Add(btnRoll);

            // btnUnRoll = new Button(btnUnRoll_InstanceName, new Point(645, 260), btnUnRoll_Text, Color.Black);
            // frmNewChar.Controls.Add(btnUnRoll);

            btnSold = new Button("btnSold", new Point(595, 300), "Sold!", Color.Black);
            frmNewChar.Controls.Add(btnSold);
            
            btnExit = new Button("btnExit", new Point(595, 360), 156, "Main Menu", Color.Black);
            frmNewChar.Controls.Add(btnExit);
            btnExit.CursorUp += delegate
            {
                LoadingScreen.Load(ScreenManager, false, Global.CurrentPlayer, new BackgroundScreen(), new MainMenuScreen());
            };

            frmNewGameKeyboard = new OnScreenKeyboard("frmNewGameKeyboard", "",
                                               new Point(frmNewChar.X + txtName.X - (txtName.Width / 2),
                                                         frmNewChar.Y + txtName.Y + txtName.Height));
            frmNewGameKeyboard.ShowTitleBar = false;
            frmNewGameKeyboard.HasMaximizeButton = false;
            frmNewGameKeyboard.HasCloseButton = false;
            frmNewGameKeyboard.HasMinimizeButton = false;
            frmNewGameKeyboard.DragAnywhere = false;
            frmNewGameKeyboard.ShowDialog();
            for (int buttoncount = 0; buttoncount < frmNewGameKeyboard.Controls.Count; buttoncount++)
            {
                frmNewGameKeyboard.Controls.ValueAtIndex(buttoncount).X += 10;
                frmNewGameKeyboard.Controls.ValueAtIndex(buttoncount).Y += 20;
                string test = frmNewGameKeyboard.Controls.ValueAtIndex(buttoncount).InstanceName;
                switch (test)
                {
                    case "btnOemPlus":
                        frmNewGameKeyboard.Controls.ValueAtIndex(buttoncount).Enabled = false;
                        frmNewGameKeyboard.Controls.ValueAtIndex(buttoncount).Visible = false;
                        break;
                    case "btnTab":
                        frmNewGameKeyboard.Controls.ValueAtIndex(buttoncount).Enabled = false;
                        frmNewGameKeyboard.Controls.ValueAtIndex(buttoncount).Visible = false;
                        break;

                    case "btnOemOpenBrackets":
                        frmNewGameKeyboard.Controls.ValueAtIndex(buttoncount).Enabled = false;
                        frmNewGameKeyboard.Controls.ValueAtIndex(buttoncount).Visible = false;
                        break;

                    case "btnOemCloseBrackets":
                        frmNewGameKeyboard.Controls.ValueAtIndex(buttoncount).Enabled = false;
                        frmNewGameKeyboard.Controls.ValueAtIndex(buttoncount).Visible = false;
                        break;

                    case "btnOemBackslash":
                        frmNewGameKeyboard.Controls.ValueAtIndex(buttoncount).Enabled = false;
                        frmNewGameKeyboard.Controls.ValueAtIndex(buttoncount).Visible = false;
                        break;

                    case "btnOemQuestion":
                        frmNewGameKeyboard.Controls.ValueAtIndex(buttoncount).Enabled = false;
                        frmNewGameKeyboard.Controls.ValueAtIndex(buttoncount).Visible = false;
                        break;
                    case "btnOemSemicolon":
                        frmNewGameKeyboard.Controls.ValueAtIndex(buttoncount).Enabled = false;
                        frmNewGameKeyboard.Controls.ValueAtIndex(buttoncount).Visible = false;
                        break;
                }
            }

        }

        void btnSoldPressed(object sender)
        {
            var newStats = new int[lvNewStats.Rows.Count];
            for (int newStatCounter = 0; newStatCounter < lvNewStats.Rows.Count; newStatCounter++)
                newStats[newStatCounter] = int.Parse(lvNewStats.Rows[newStatCounter][1], CultureInfo.CurrentCulture);
            Player player = Player.Instance;
            object test = sender;
#if DEBUG || XBOX
            player.NewCharacter(txtName.Text, optRace.Checked.Text,
                                optClass.Checked.Text, newStats, Global.CurrentPlayer);
#else
            player.NewCharacter(txtName.Text, optRace.Checked.Text, optClass.Checked.Text, newStats, PlayerIndex.One);
#endif
            LoadingScreen.Load(ScreenManager, true, ControllingPlayer.Value,
                               new GameplayScreen());
            this.ExitScreen();
        }

        public void CloseKeyboard(object sender)
        {
            frmNewGameKeyboard.Hide();
            frmNewGameKeyboard.Focused = false;
            frmNewChar.Focus();
            frmNewGameKeyboard.Position = new Point(1280, 720);
            txtName.Focused = false;
            ignoreControls = true;
            ignoreControlTimer = 0.25f;
        }

        /// <summary>
        /// Draws the gameplay screen.
        /// </summary>
        public override void Draw(GameTime gameTime)
        {


            // Our player and enemy are both actually just text strings.
            SpriteBatch spriteBatch = ScreenManager.SpriteBatch;

            spriteBatch.Begin();

            OrbFormCollection.Draw(ScreenManager.GraphicsDevice, spriteBatch);
            spriteBatch.End();
            
            // If the game is transitioning on or off, fade it out to black.
            if (TransitionPosition > 0 || pauseAlpha > 0)
            {
                float alpha = MathHelper.Lerp(1f - TransitionAlpha, 1f, pauseAlpha / 2);
                ScreenManager.FadeBackBufferToBlack(alpha);
            }
        }

        /// <summary>
        /// Lets the game respond to player input. Unlike the Update method,
        /// this will only be called when the gameplay screen is active.
        /// </summary>
        public override void HandleInput(InputState input)
        {
            if (input == null)
                throw new ArgumentNullException("input");

            // Look up inputs for the active player profile.
            var playerIndex = (int)ControllingPlayer.Value;

            KeyboardState keyboardState = input.CurrentKeyboardStates[playerIndex];
            GamePadState gamePadState = input.CurrentGamePadStates[playerIndex];

            // The game pauses either if the user presses the pause button, or if
            // they unplug the active gamepad. This requires us to keep track of
            // whether a gamepad was ever plugged in, because we don't want to pause
            // on PC if they are playing with a keyboard and have no gamepad at all!
            bool gamePadDisconnected = !gamePadState.IsConnected &&
                                       input.GamePadWasConnected[playerIndex];

            // Mouse.SetPosition(btnRoll.X + frmNewChar.X + btnRoll.Width / 2 , btnRoll.Y + frmNewChar.Y + btnRoll.Height / 2);
            if (input.IsPauseGame(ControllingPlayer) || gamePadDisconnected)
            {
                ScreenManager.AddScreen(new PauseMenuScreen(), ControllingPlayer);
            }
        }

        public void OpenKeyboard()
        {
            frmNewGameKeyboard.Position = new Point(frmNewChar.X + txtName.X - (txtName.Width / 2),
                                             frmNewChar.Y + txtName.Y + txtName.Height);
            frmNewGameKeyboard.Show();
            optClass.Enabled = false;
            optRace.Enabled = false;
            btnRoll.Enabled = false;
            btnSold.Enabled = false;
        }

        public override void UnloadContent()
        {
            if (frmNewChar.IsNotNullOrDisposed())
                OrbFormCollection.Remove(frmNewChar);
            if (frmNewGameKeyboard.IsNotNullOrDisposed())
                OrbFormCollection.Remove(frmNewGameKeyboard);
            base.UnloadContent();
        }
        public override void OnCancel(PlayerIndex playerIndex)
        {
            //Global.frmNewChar.Visible = false;
            //Global.frmNewChar.Enabled = false;
            //Global.frmNewGameKeyboard.Visible = false;
            //Global.frmNewGameKeyboard.Enabled = false;
            //Global.frmNewGameKeyboard.Controls["btnEnter"].CursorUp -= CloseKeyboard;
            //Global.btnSold.CursorUp -= btnSoldPressed;
            base.OnCancel(playerIndex);
        }

        /// <summary>
        /// Updates the state of the game. This method checks the GameScreen.IsActive
        /// property, so the game will stop updating when the pause menu is active,
        /// or if you tab away to a different application.
        /// </summary>
        public override void Update(GameTime gameTime, bool otherScreenHasFocus,
                                    bool coveredByOtherScreen)
        {
            base.Update(gameTime, otherScreenHasFocus, false);
            if (setfocus)
            {
                setfocus = false;
                txtName.Focused = true;
            }

            if (txtName.Focused)
            {
                OpenKeyboard();
            }

            if (ignoreControls)
            {
                ignoreControlTimer -= gameTime.ElapsedGameTime.TotalSeconds;
                if (ignoreControlTimer <= 0.0f)
                {
                    optClass.Enabled = true;
                    optRace.Enabled = true;
                    btnRoll.Enabled = true;
                    btnSold.Enabled = true;
                    ignoreControls = false;
                }
            }

            // Gradually fade in or out depending on whether we are covered by the pause screen.
            if (coveredByOtherScreen)
            {
                pauseAlpha = Math.Min(pauseAlpha + 1f/32, 1);
                frmNewChar.Enabled = false;
            }
            else
            {
                pauseAlpha = Math.Max(pauseAlpha - 1f/32, 0);
            }
            if (pauseAlpha == 0)
            {
                frmNewChar.Enabled = true;
            }
        }
    
        #endregion Methods
    }
}