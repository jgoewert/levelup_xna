#region Header
// --------------------------------
// <copyright file="PlayerIndexEventArgs.cs" company="John Goewert">
//     Copyright John Goewert 2011.
// </copyright>
// <author>John Goewert</author>
// ---------------------------------
#endregion

using System;
using Microsoft.Xna.Framework;

namespace GameStateManagement
{
    /// <summary>
    /// Initializes a new instance of the PlayerIndexEventArgs class.
    /// Custom event argument which includes the index of the player who
    /// triggered the event. This is used by the MenuEntry.Selected event.
    /// </summary>
    internal class PlayerIndexEventArgs : EventArgs
    {
        #region Fields

        private readonly PlayerIndex playerIndex;

        #endregion Fields

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the PlayerIndexEventArgs class.
        /// </summary>
        public PlayerIndexEventArgs(PlayerIndex playerIndex)
        {
            this.playerIndex = playerIndex;
        }

        #endregion Constructors

        #region Properties

        /// <summary>
        /// Gets the index of the player who triggered this event.
        /// </summary>
        public PlayerIndex PlayerIndex
        {
            get { return playerIndex; }
        }

        #endregion Properties
    }
}