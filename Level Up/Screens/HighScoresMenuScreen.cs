#if DEBUG || XBOX
#region Header
// --------------------------------
// <copyright file="MainMenuScreen.cs" company="John Goewert">
//     Copyright John Goewert 2011.
// </copyright>
// <author>John Goewert</author>
// ---------------------------------
#endregion


using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using OrbUI.Components;
using OrbUI.Components.GUI;
using OrbUI.Components.GUI.Controls;
using OrbUI.Components.Input.Controller;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;

namespace GameStateManagement
{
    /// <summary>
    /// The main menu screen is the first thing displayed when the game starts up.
    /// </summary>
    internal class HighScoresMenuScreen : MenuScreen

    {
        #region Fields
        private float pauseAlpha;

        private ContentManager content;

        private Form frmHighScoreMenu;
        private Label lblSearch;
        private TextBox txtSearch;
        private Button btnFriends;
        private Button btnMine;
        private Button btnReset;
        private ListView lvCharacters;
        private Button btnNextPage;
        private Button btnBackPage;
        private Button btnHighScoreExit;

        private OnScreenKeyboard frmNewGameKeyboard;

        int currentPage = 0;
        bool friendsOnly = false;
        bool mineOnly = false;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the MainMenuScreen class.
        /// </summary>
        public HighScoresMenuScreen()
            : base("High Scores")
        {
            if (frmHighScoreMenu.IsNullOrDisposed())
            {
                InitOHighScoreMenu();
            }
            btnHighScoreExit.CursorUp += HighScoreExitSelected;

        }

        #endregion Constructors

#region Methods

        protected override void OnSelectEntry(int entryIndex, PlayerIndex playerIndex)
        {
        }


        private static int CompareLevelReverse(string[] x, string[] y)
        {
            int xval = Int16.Parse(x[0]);
            int yval = Int16.Parse(y[0]);

            if (xval > yval)
            {
                return 1;
            }
            else
                if (xval < yval)
                {
                    return -1;
                }
                else
                {
                    return 0;
                }
        }

        private static int CompareLevel(string[] x, string[] y)
        {
            int xval = Int16.Parse(x[0]);
            int yval = Int16.Parse(y[0]);

            if (xval > yval)
            {
                return -1;
            } else
                if (xval < yval)
                {
                    return 1;
                }
                else {
                    return 0;
                }
        }

        private void InitOHighScoreMenu()
        {
            frmHighScoreMenu = new Form("frmHighScoreMenu", "", new Point(0, 0), new Point(1280, 720), FormBorderStyle.None);
            frmHighScoreMenu.DragAnywhere = false;
            frmHighScoreMenu.ShowTitleBar = false;
            frmHighScoreMenu.FontName = "gamefont";
            frmHighScoreMenu.Font.LineSpacing = 0;
            frmHighScoreMenu.TitleTextColor = Color.White;
            frmHighScoreMenu.Show();
            
            /* lblSearch = new Label("lblSearch", new Point(5, 20), new Point(380, 20), "Search:",
                                       Color.Yellow);
            frmHighScoreMenu.Controls.Add(lblSearch);

            txtSearch = new TextBox("txtSearch", new Point(115, 20),
                                  new Point(300, frmHighScoreMenu.Font.Height() + 5));
            frmHighScoreMenu.Controls.Add(txtSearch);
            */
            btnFriends = new Button("btnFriends", new Point(5, 50), new Point(240, 50), "Friends Only", Color.Black);
            frmHighScoreMenu.Controls.Add(btnFriends);
            btnFriends.CursorUp += ButtonFriendsSelected;

            btnMine = new Button("btnMine", new Point(250, 50), new Point(240, 50), "Mine Only", Color.Black);
            frmHighScoreMenu.Controls.Add(btnMine);
            btnMine.CursorUp += ButtonMineSelected;

            btnReset = new Button("btnReset", new Point(500, 50), new Point(240, 50), "Reset", Color.Black);
            frmHighScoreMenu.Controls.Add(btnReset);
            btnReset.CursorUp += ButtonResetSelected;

            lvCharacters = new ListView("lvCharacters", new Point(5, 110), new Point(1260, 520));
            lvCharacters.Columns.Add(new ListViewHeader("Level", 50));
            lvCharacters.Columns.Add(new ListViewHeader("Gamertag", 150));
            lvCharacters.Columns.Add(new ListViewHeader("Race/Class", 300));
            lvCharacters.Columns.Add(new ListViewHeader("Prime Stat", 110));
            lvCharacters.Columns.Add(new ListViewHeader("Plot Stage", 90));
            lvCharacters.Columns.Add(new ListViewHeader("Prized Item", 270));
            lvCharacters.Columns.Add(new ListViewHeader("Speciality", 300));
            lvCharacters.FontName = "gamefont";
            lvCharacters.Enabled = true;
            frmHighScoreMenu.Controls.Add(lvCharacters);

            btnBackPage = new Button("btnBackPage", new Point(5, 640), new Point(240, 50), "Back Page", Color.Black);
            frmHighScoreMenu.Controls.Add(btnBackPage);
            btnBackPage.CursorUp += ButtonBackPressed;

            btnNextPage = new Button("btnNextPage", new Point(205, 640), new Point(240, 50), "Next Page", Color.Black);
            frmHighScoreMenu.Controls.Add(btnNextPage);
            btnNextPage.CursorUp += ButtonNextPressed;

            btnHighScoreExit = new Button("btnHighScoreExit", new Point(500, 640), new Point(240, 50), "Close", Color.Black);
            frmHighScoreMenu.Controls.Add(btnHighScoreExit);

            if (Global.Scores == null)
            {
                Global.CreateScoresTable();
            }
            GetScores(0);

            
        }

        private void ButtonBackPressed(object sender)
        {
            currentPage++;
            GetScores(currentPage);
        }

        private void ButtonNextPressed(object sender)
        {
            currentPage--;
            GetScores(currentPage);
        }

        private void GetScores(int page_number)
        {

            if (friendsOnly)
            {
                lvCharacters.Rows.Clear();
                SpynDoctor.TopScoreEntry[] page = new SpynDoctor.TopScoreEntry[20];
                Global.Scores.FillPageFromFilteredList(0, currentPage, page, Gamer.SignedInGamers[Global.CurrentPlayer]);
                lvCharacters.BeginUpdate();
                int offset = 0;
                foreach (SpynDoctor.TopScoreEntry score in page)
                {
                    if (score != null)
                    {
                        var row = new string[7];
                        row[0] = score.Level;
                        row[1] = score.Gamertag;
                        row[2] = score.Race + " " + score.Class;
                        row[3] = score.Stat;
                        row[4] = score.Plot;
                        row[5] = score.Item;
                        row[6] = score.Spell;
                        lvCharacters.Rows.Add(row);
                        offset++;
                    }
                }
                lvCharacters.Rows.Sort(CompareLevel);
                lvCharacters.EndUpdate();

            }
            else if (mineOnly)
            {
                lvCharacters.BeginUpdate();
                lvCharacters.Rows.Clear();
                SpynDoctor.TopScoreEntry[] page = new SpynDoctor.TopScoreEntry[20];
                Global.Scores.FillPageWithGamertagFromFullList(0, page, Gamer.SignedInGamers[Global.CurrentPlayer].Gamertag);
                int offset = 0;
                foreach (SpynDoctor.TopScoreEntry score in page)
                {
                    if (score != null)
                    {
                        var row = new string[7];
                        row[0] = score.Level;
                        row[1] = score.Gamertag;
                        row[2] = score.Race + " " + score.Class;
                        row[3] = score.Stat;
                        row[4] = score.Plot;
                        row[5] = score.Item;
                        row[6] = score.Spell;
                        lvCharacters.Rows.Add(row);
                        offset++;
                    }
                }
                lvCharacters.Rows.Sort(CompareLevel);
                lvCharacters.EndUpdate();
            }
            else
            {
                lvCharacters.Rows.Clear();
                currentPage = page_number;
                SpynDoctor.TopScoreEntry[] page = new SpynDoctor.TopScoreEntry[20];
                Global.Scores.FillPageFromFullList(0, page_number, page);
                lvCharacters.BeginUpdate();
                int offset = 0;
                foreach (SpynDoctor.TopScoreEntry score in page)
                {
                    if (score != null)
                    {
                        var row = new string[7];
                        row[0] = score.Level;
                        row[1] = score.Gamertag;
                        row[2] = score.Race + " " + score.Class;
                        row[3] = score.Stat;
                        row[4] = score.Plot;
                        row[5] = score.Item;
                        row[6] = score.Spell;
                        lvCharacters.Rows.Add(row);
                        offset++;
                    }
                }
                lvCharacters.Rows.Sort(CompareLevel);
                lvCharacters.EndUpdate();
                if (page.Length >= 20)
                {
                    btnNextPage.Visible = false;
                    btnNextPage.Enabled = false;
                } else {
                    btnNextPage.Visible = true;
                    btnNextPage.Enabled = true;
                }
                if (page_number > 0)
                {
                    btnBackPage.Visible = true;
                    btnBackPage.Enabled = true;
                } else {
                    btnBackPage.Visible = false;
                    btnBackPage.Enabled = false;
                }
            }
        }
        
        private void ButtonResetSelected(object sender)
        {
            friendsOnly = false;
            btnFriends.TextColor = Color.Black;
            mineOnly = false;
            btnMine.TextColor = Color.Black;
            lvCharacters.Rows.Clear();
            GetScores(0);
        }

        private void ButtonFriendsSelected(object sender)
        {
            mineOnly = false;
            btnMine.TextColor = Color.Black;
            friendsOnly = !friendsOnly;
            if (friendsOnly)
            {
                btnFriends.TextColor = Color.Black;
            }
            else
            {
                btnFriends.TextColor = Color.Black;
            }
            currentPage = 0;
            GetScores(currentPage);
        }

        private void ButtonMineSelected(object sender)
        {
            friendsOnly = false;
            btnFriends.TextColor = Color.Black;
            mineOnly = !mineOnly;
            if (mineOnly)
            {
                btnMine.TextColor = Color.Black;
            }
            else
            {
                btnMine.TextColor = Color.Black;
            }
            currentPage = 0;
            GetScores(currentPage);           
        }

        /// <summary>
        /// Draws the gameplay screen.
        /// </summary>
        public override void Draw(GameTime gameTime)
        {
            // Darken down any other screens that were drawn beneath the popup.
            ScreenManager.FadeBackBufferToBlack(TransitionAlpha * 2 / 3);
            
            ScreenManager.SpriteBatch.Begin();
            if (frmHighScoreMenu.IsNotNullOrDisposed())
                frmHighScoreMenu.Draw(ScreenManager.GraphicsDevice, ScreenManager.SpriteBatch);
            
            OrbFormCollection.Cursor.Draw(ScreenManager.GraphicsDevice, ScreenManager.SpriteBatch);

            ScreenManager.SpriteBatch.End();
        }

        public override void OnCancel(PlayerIndex playerIndex)
        {
            this.ExitScreen();
        }

        public void HighScoreExitSelected(object sender)
        {
            this.ExitScreen();
        }

        public override void UnloadContent()
        {
            base.UnloadContent();
            if (frmHighScoreMenu.IsNotNullOrDisposed())
                OrbFormCollection.Remove(frmHighScoreMenu);
        }

#endregion Methods

    }
}
#endif