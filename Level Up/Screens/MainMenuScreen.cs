#region Header
// --------------------------------
// <copyright file="MainMenuScreen.cs" company="John Goewert">
//     Copyright John Goewert 2011.
// </copyright>
// <author>John Goewert</author>
// ---------------------------------
#endregion


using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using OrbUI.Components;
using OrbUI.Components.GUI;
using OrbUI.Components.GUI.Controls;
using OrbUI.Components.GUI.Cursors;
using OrbUI.Components.Input.Controller;
using Microsoft.Xna.Framework.Content;
#if DEBUG || XBOX
using Microsoft.Xna.Framework.GamerServices;
#endif
using EasyStorage;

namespace GameStateManagement
{

    /// <summary>
    /// The main menu screen is the first thing displayed when the game starts up.
    /// </summary>
    internal class MainMenuScreen : MenuScreen
    {
        #region Fields
        private float pauseAlpha = 1.0f;

        private Form frmMainMenu;
        private Button btnLoad;
        private Button btnNew;
        private Button btnHighScores;
        private Button btnBuyNow;
        private Button btnOptions;
        private Button btnMainExit;

        #endregion
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the MainMenuScreen class.
        /// </summary>
        public MainMenuScreen()
            : base("MainMenu")
        {
            if (frmMainMenu.IsNullOrDisposed())
            {
                InitMainMenu();

            }

        }

        public override void LoadContent()
        {
            base.LoadContent();

        }

        private void InitMainMenu()
        {
            frmMainMenu = new Form("frmMainMenu", "" , new Point(500, 270), new Point(280, 340), FormBorderStyle.None);
            frmMainMenu.DragAnywhere = false;
            frmMainMenu.ShowTitleBar = false;
            frmMainMenu.FontName = "gamefont";
            frmMainMenu.Font.LineSpacing = 0;
            frmMainMenu.TitleTextColor = Color.White;
            frmMainMenu.Enabled = false;
            frmMainMenu.Show();
            frmMainMenu.Hide();

            btnNew = new Button("btnNew", new Point(5, 20), new Point(260, 50), "New Character", Color.Black);
            frmMainMenu.Controls.Add(btnNew);
            btnNew.CursorUp += NewGameMenuEntrySelected;

            btnLoad = new Button("btnLoad", new Point(5, 80), new Point(260, 50), "Load Character", Color.Black);
            frmMainMenu.Controls.Add(btnLoad);
            btnLoad.CursorUp += LoadGameMenuEntrySelected;
#if XBOX
            CursorState.CursorPosition = new Point(635, 375);
#endif

#if DEBUG || XBOX
#if XBOX
            btnBuyNow = new Button("btnBuyNow", new Point(5, 140), new Point(260, 50), "Buy Full Version", Color.Black);
            btnBuyNow.CursorUp += BuyNowMenuEntrySelected;
#endif
            btnHighScores = new Button("btnHighScores", new Point(5, 140), new Point(260, 50), "High Scores", Color.Black);
            btnHighScores.CursorUp += HighScoresMenuEntrySelected;
            
            if ((Guide.IsTrialMode) &&
                (Gamer.SignedInGamers[Global.CurrentPlayer] != null))
            {
#if XBOX
                if (Gamer.SignedInGamers[Global.CurrentPlayer].Privileges.AllowPurchaseContent)
                {
                    frmMainMenu.Controls.Add(btnBuyNow);
                }
#endif 
            }
            else
            {
                frmMainMenu.Controls.Add(btnHighScores);
            }
#endif
            btnOptions = new Button("btnOptions", new Point(5, 200), new Point(260, 50), "Options", Color.Black);
            frmMainMenu.Controls.Add(btnOptions);
            btnOptions.CursorUp += OptionsMenuEntrySelected;

            btnMainExit = new Button("btnMainExit", new Point(5, 260), new Point(260, 50), "Exit", Color.Black);
            frmMainMenu.Controls.Add(btnMainExit);
            btnMainExit.CursorUp += MainExitSelected;
        }
        
        protected override void OnSelectEntry(int entryIndex, PlayerIndex playerIndex)
        {
        }
        
        public override void UnloadContent()
        {
            if (frmMainMenu.IsNotNullOrDisposed())
                OrbFormCollection.Remove(frmMainMenu);
            base.UnloadContent();
        }

        #endregion Constructors

        #region Methods

        /// <summary>
        /// Draws the gameplay screen.
        /// </summary>
        public override void Draw(GameTime gameTime)
        {
            ScreenManager.SpriteBatch.Begin();
            OrbFormCollection.Draw(ScreenManager.GraphicsDevice, ScreenManager.SpriteBatch);
            ScreenManager.SpriteBatch.End();

            // If the game is transitioning on or off, fade it out to black.
            if (TransitionPosition > 0 || pauseAlpha > 0)
            {
                float alpha = MathHelper.Lerp(1f - TransitionAlpha, 1f, pauseAlpha / 2);

                ScreenManager.FadeBackBufferToBlack(alpha);
            }
        }

        /// <summary>
        /// Update the gameplay screen.
        /// </summary>
        public override void Update(GameTime gameTime, bool otherScreenHasFocus, bool coveredByOtherScreen)
        {
            base.Update(gameTime, otherScreenHasFocus, coveredByOtherScreen);
            if (!this.IsExiting)
            {
                // prompt for a device on the first Update we can

#if XBOX
                if ((!Guide.IsTrialMode) && (!frmMainMenu.Controls.ContainsKey("btnHighScores")))
                {
                    frmMainMenu.Controls.Remove(btnBuyNow);
                    frmMainMenu.Controls.Add(btnHighScores);
                }
#endif

                // Gradually fade in or out depending on whether we are covered by the pause screen.
                if (coveredByOtherScreen)
                {
                    pauseAlpha = Math.Min(pauseAlpha + 1f / 32, 1);
                    frmMainMenu.Enabled = false;
                }
                else
                {
                    pauseAlpha = Math.Max(pauseAlpha - 1f / 32, 0);
                    frmMainMenu.Visible = true;
                    if (pauseAlpha <= 0)
                    {
                        if (!frmMainMenu.Visible)
                        {
                            frmMainMenu.Show();
                        }

                        frmMainMenu.Enabled = true;
                        if (Global.SaveDevice != null)
                        {
                            if (Global.SaveDevice.Enabled)
                            {
                                frmMainMenu.Enabled = true;
                            }
                        }
                        else
                        {
                            frmMainMenu.Enabled = false;
                            // create and add our SaveDevice
                            var sharedSaveDevice = new SharedSaveDevice();
                            ScreenManager.Game.Components.Add(sharedSaveDevice);

                            // make sure we hold on to the device
                            Global.SaveDevice = sharedSaveDevice;

                            // hook two event handlers to force the user to choose a new device if they cancel the
                            // device selector or if they disconnect the storage device after selecting it
                            sharedSaveDevice.DeviceSelectorCanceled += (s, e) => e.Response = SaveDeviceEventResponse.Force;
                            sharedSaveDevice.DeviceDisconnected += (s, e) => e.Response = SaveDeviceEventResponse.Force;

                            // hook an event so we can see that it does fire
                            Global.SaveDevice.SaveCompleted += Global.saveDeviceSaveCompleted;

                            Global.SaveDevice.PromptForDevice();
                            Global.SaveDevice.DeviceSelected += (s, e) =>
                            {
                                Global.LoadSettings();
                            };
                        }
                    }
                }
            }
        }

        public override void OnCancel(PlayerIndex playerIndex)
        {
 	        ScreenManager.AddScreen(new GameStateManagement.TitleScreen(), null);
            base.OnCancel(playerIndex);
        }
        /// <summary>
        /// When the user cancels the main menu, ask if they want to exit the sample.
        /// </summary>
        public void MainExitSelected(object sender)
        {
            frmMainMenu.Enabled = false;
            const string message = "Are you sure you want to exit?";
            var confirmExitMessageBox = new MessageBoxScreen(message);
            confirmExitMessageBox.Accepted += ConfirmExitMessageBoxAccepted;
            ScreenManager.AddScreen(confirmExitMessageBox, Global.CurrentPlayer);
        }

#if XBOX
        public void BuyNowMenuEntrySelected(object sender)
        {
            MessageBoxScreen confirmMarketplaceMessageBox =
                                    new MessageBoxScreen("Are you sure you want to purchase this now?");

            confirmMarketplaceMessageBox.Accepted += delegate
            {
                Guide.ShowMarketplace(ControllingPlayer.Value);
                confirmMarketplaceMessageBox.ExitScreen();
            };

            ScreenManager.AddScreen(confirmMarketplaceMessageBox, Global.CurrentPlayer);
        }
#endif

        /// <summary>
        /// Event handler for when the user selects ok on the "are you sure
        /// you want to exit" message box.
        /// </summary>
        private void ConfirmExitMessageBoxAccepted(object sender, PlayerIndexEventArgs e)
        {
            try
            {
                Global.music.Stop();
                ScreenManager.Game.Exit();
            }
            catch { 
            }
        }

        /// <summary>
        /// Event handler for when the Play Game menu entry is selected.
        /// </summary>
        private void LoadGameMenuEntrySelected(object sender)
        {
            LoadingScreen.Load(ScreenManager, true, Global.CurrentPlayer, new BackgroundScreen(), new LoadGameMenuScreen());
            
        }

        /// <summary>
        /// Event handler for when the Play Game menu entry is selected.
        /// </summary>
        private void NewGameMenuEntrySelected(object sender)
        {
            LoadingScreen.Load(ScreenManager, true, Global.CurrentPlayer, new BackgroundScreen(), new NewGameScreen());
        }
        
#if DEBUG || XBOX
        /// <summary>
        /// Event handler for when the Options menu entry is selected.
        /// </summary>
        private void HighScoresMenuEntrySelected(object sender)
        {
            frmMainMenu.Enabled = false;
            ScreenManager.AddScreen(new HighScoresMenuScreen(), Global.CurrentPlayer);

            //            LoadingScreen.Load(ScreenManager, false, null, new OptionsMenuScreen());
        }
#endif

        /// <summary>
        /// Event handler for when the Options menu entry is selected.
        /// </summary>
        private void OptionsMenuEntrySelected(object sender)
        {
            frmMainMenu.Enabled = false;
            ScreenManager.AddScreen(new OptionsMenuScreen(), Global.CurrentPlayer);
            
//            LoadingScreen.Load(ScreenManager, false, null, new OptionsMenuScreen());
        }

        #endregion Methods
    }
}