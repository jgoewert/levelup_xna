#region Header
// --------------------------------
// <copyright file="GameplayScreen.cs" company="John Goewert">
//     Copyright John Goewert 2011.
// </copyright>
// <author>John Goewert</author>
// ---------------------------------
#endregion


using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Text;
using System.Xml.Serialization;
using EasyStorage;
using LevelUp;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
#if DEBUG || XBOX
using Microsoft.Xna.Framework.GamerServices;
#endif
#if WINDOWS
using System.Web.Script.Serialization;
#endif
using OrbUI;
using OrbUI.Components;
using OrbUI.Components.GUI;
using OrbUI.Components.GUI.Controls;
using OrbUI.Components.Input.Controller;
using System.Globalization;
using Level_Up;

namespace GameStateManagement
{
    /// <summary>
    /// This screen implements the actual game logic. It is just a
    /// placeholder to get the idea across: you'll probably want to
    /// put some more interesting gameplay in here!
    /// </summary>
    internal class GameplayScreen : GameScreen
    {
        #region Fields

        protected const string BarActionInstanceName = "_barAction";
        private const string BarEncumbranceInstanceName = "_barEncumbrance";
        private const string BarExperienceInstanceName = "_barExperience";
        private const string BarPlotInstanceName = "_barPlot";
        private const string BarQuestsInstanceName = "_barQuests";
        private const string FrmCharInstanceName = "frmChar";
        private const string FrmCharTitle = "Character Sheet";
        private const string FrmPlotInstanceName = "frmPlot";
        private const string FrmPlotTitle = "Plot";
        private const string FrmStatusInstanceName = "frmStatus";
        private const string FrmStatusTitle = "Status";
        private const string GsFontNameGamefont = "gamefont";
        private const string LblActionInstanceName = "lblAction";
        private const string LblActionText = "Doing Something...";
        private const string LblExperienceInstanceName = "lblExperience";
        private const string LblExperienceText = "Experience";
        private const string LVEquipmentInstanceName = "lvEquipment";
        private const string LVInventoryInstanceName = "lvInventory";
        private const string LVPlotInstanceName = "lvPlot";
        private const string LVQuestsInstanceName = "lvQuests";
        private const string lvSpellbookInstanceName = "lvSpellbook";
        private const string LVStatInstanceName = "lvStat";
        private const string LVTraitColumnName = "Trait";
        private const string LVTraitInstanceName = "lvTrait";

        private  Dictionary<string, Rectangle> _itemsTexture;
        private Texture2D itemsSheet;

        private  Dictionary<string, Rectangle> _monstersTexture;
        private Texture2D monsterSheet;

        private Dictionary<string, Rectangle> damageTexture;
        private Texture2D damageSheet;

        private Dictionary<string, Rectangle> weaponTexture;
        private Texture2D weaponSheet;

        private Dictionary<string, Rectangle> shieldTexture;
        private Texture2D shieldSheet;

        private Dictionary<string, Rectangle> brassairtsTexture;
        private Texture2D brassairtsSheet;

        private Dictionary<string, Rectangle> vambraceTexture;
        private Texture2D vambraceSheet;

        private Dictionary<string, Rectangle> gauntletsTexture;
        private Texture2D gauntletsSheet;

        private Dictionary<string, Rectangle> cuissesTexture;
        private Texture2D cuissesSheet;

        private Dictionary<string, Rectangle> greavesTexture;
        private Texture2D greavesSheet;

        private Dictionary<string, Rectangle> solleretTexture;
        private Texture2D solleretSheet;
        
        private Dictionary<string, Rectangle> helmTexture;
        private Texture2D helmSheet;

        private Dictionary<string, Rectangle> hauberkTexture;
        private Texture2D hauberkSheet;

        private Dictionary<string, Rectangle> gambesonTexture;
        private Texture2D gambesonSheet;

        private Dictionary<string, Rectangle> UniquesTexture;

        private readonly Player player = Player.Instance;

        private struct Terrain
        {
            public Texture2D sky;
            public Texture2D background;
            public Texture2D path;
            public Terrain(Texture2D path, Texture2D background, Texture2D sky)
            {
                this.sky = sky;
                this.background = background;
                this.path = path;
            }
        }

        // Bug Fix
        private bool firstTimeHide = true;

        private Dictionary<string, Terrain> terrainStructs;
        private Terrain townTerrain;

        private List<GameObject> friendObject;
        private GameObject damageObject;
        private float damageObjectAliveCounter;
        private GameObject goldObject;
        private GameObject ItemObject;
        private GameObject equipObject;
        private GameObject monsterObject;

        #region Main Bar and Status Box
        private Form frmStatus;
        private Button btnChar;
        private Button btnEquipment;
        private Button btnSpellbook;
        private Label lblAction;
        private ProgressBar _barAction;
        private float actionTimer = 0;
        private float actionTimerMax = 0;
        private Button btnPlot;
        private Button btnQuests;
        private Button btnInventory;
        #endregion

        #region Character Sheet
        private Form frmChar;
        private ListView lvStat;
        private ListView lvTrait;
        private Label lblExperience;
        private ProgressBar _barExperience;
        #endregion

        #region Plots Sheet
        private Form frmPlot;
        private ListView lvPlot;
        private Label lblPlot;
        private ProgressBar _barPlot;
        #endregion

        #region Quests Sheet
        private Form frmQuests;
        private ListView lvQuests;
        private Label lblQuests;
        private ProgressBar _barQuests;
        #endregion

        #region Equipment
        private Form frmEquipment;
        private ListView lvEquipment;
        #endregion

        #region Inventory
        private Form frmInventory;
        private ListView lvInventory;
        private Label lblEncumbrance;
        private ProgressBar _barEncumbrance;
        #endregion
        
        #region Spellbook
        private Form frmSpellbook;
        private ListView lvSpellbook;
        private DateTime nextSave = DateTime.Now;
        #endregion

        #region Tutorial
        private Form frmTutorial;
        private Label lblTutorial;
        private PictureBox pbTutorial;
        #endregion

        #region XBLive
        private Form frmXBLive;
        private ListView lvSessions;
        private Button btnFriendsOnly;
        private Button btnConnect;
        private bool Connected;
        #endregion

        private float pauseAlpha;
        
        private KeyboardState previousKeyboardState;

        /// <summary>
        /// Scrolling Background tile for parallax world
        /// </summary>
        private ScrollingBackground _scrBackground;
        private ScrollingBackground _scrBackgroundClouds;
        private ScrollingBackground _scrBackgroundTrees;

        #endregion Fields

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the GameplayScreen class.
        /// </summary>
        public GameplayScreen()
        {
            TransitionOnTime = TimeSpan.FromSeconds(1.5);
            TransitionOffTime = TimeSpan.FromSeconds(0.5);
            Global.SaveDevice.SaveCompleted += SaveDeviceSaveCompleted;
#if DEBUG || XBOX
            // if (!Guide.IsTrialMode && mPlayer != null && mPlayer.IsSignedInToLive && mPlayer.Privileges.AllowOnlineSessions)

            if (Global.Scores == null)
            {
                Global.CreateScoresTable();
            }
            if ((!Guide.IsTrialMode) && (Global.CurrentPlayer != null) && (Gamer.SignedInGamers[Global.CurrentPlayer].IsSignedInToLive) && (Gamer.SignedInGamers[Global.CurrentPlayer].Privileges.AllowOnlineSessions))
                Global.SyncManager.Start(Gamer.SignedInGamers[Global.CurrentPlayer], Global.Scores);
#endif

            LoadGame();
        }

        #endregion Constructors

        #region Methods

        /// <summary>
        /// Draws the gameplay screen.
        /// </summary>
        public override void Draw(GameTime gameTime)
        {
            // This game has a blue background. Why? Because!
            ScreenManager.GraphicsDevice.Clear(ClearOptions.Target,
                                               Color.Black, 0, 0);

            // Our player and enemy are both actually just text strings.
            SpriteBatch spriteBatch = ScreenManager.SpriteBatch;

            spriteBatch.Begin(SpriteSortMode.Deferred, null, SamplerState.PointClamp, null, null);

            _scrBackgroundClouds.Draw(spriteBatch);
            _scrBackgroundTrees.Draw(spriteBatch);
            _scrBackground.Draw(spriteBatch);

            player.GameObject.DrawFrame(spriteBatch);
#if XBOX
            if (Connected)
            {
                foreach (GameObject friend in friendObject)
                {
                    friend.DrawFrame(spriteBatch);
                }
            }
#endif
            if (equipObject.Alive)
                equipObject.DrawFrame(spriteBatch);
            if (ItemObject.Alive)
                // ItemObject.DrawFrame(spriteBatch);
                spriteBatch.Draw(itemsSheet, ItemObject.Position, ItemObject.Sprite.Rect, Color.White,
                                 ItemObject.Rotation, ItemObject.Sprite.Origin, ItemObject.Sprite.Scale,
                                 SpriteEffects.None, ItemObject.Sprite.Depth);
            // todo: Create function in Gameobject to draw per rect
            if (monsterObject.Alive)
               spriteBatch.Draw(monsterSheet, monsterObject.Position, monsterObject.Sprite.Rect, Color.White,
                                 monsterObject.Rotation, monsterObject.Sprite.Origin, monsterObject.Sprite.Scale,
                                 SpriteEffects.None, monsterObject.Sprite.Depth);
            if (damageObject.Alive)
                spriteBatch.Draw(damageSheet, damageObject.Position, damageObject.Sprite.Rect, Color.White,
                                 damageObject.Rotation, damageObject.Sprite.Origin, damageObject.Sprite.Scale,
                                 SpriteEffects.None, damageObject.Sprite.Depth);
            if (goldObject.Alive)
                goldObject.DrawFrame(spriteBatch);

            OrbFormCollection.Draw(ScreenManager.GraphicsDevice, spriteBatch);

            spriteBatch.End();

            // If the game is transitioning on or off, fade it out to black.
            if (TransitionPosition > 0 || pauseAlpha > 0)
            {
                float alpha = MathHelper.Lerp(1f - TransitionAlpha, 1f, pauseAlpha / 2);

                ScreenManager.FadeBackBufferToBlack(alpha);
            }
        }

        /// <summary>
        /// Lets the game respond to player input. Unlike the Update method,
        /// this will only be called when the gameplay screen is active.
        /// </summary>
        public override void HandleInput(InputState input)
        {
            if (input == null)
                throw new ArgumentNullException("input");

            // Look up inputs for the active player profile.
            var playerIndex = (int)ControllingPlayer.Value;

            KeyboardState keyboardState = input.CurrentKeyboardStates[playerIndex];
            GamePadState gamePadState = input.CurrentGamePadStates[playerIndex];

            // The game pauses either if the user presses the pause button, or if
            // they unplug the active gamepad. This requires us to keep track of
            // whether a gamepad was ever plugged in, because we don't want to pause
            // on PC if they are playing with a keyboard and have no gamepad at all!
            bool gamePadDisconnected = !gamePadState.IsConnected &&
                                       input.GamePadWasConnected[playerIndex];

            if (input.IsPauseGame(ControllingPlayer) || gamePadDisconnected)
            {
                ScreenManager.AddScreen(new PauseMenuScreen(), ControllingPlayer);
            }
        }

        /// <summary>
        /// This method opens a file using System.IO classes and the
        /// TitleLocation property.  It presumes that a file named
        /// ship.dds has been deployed alongside the game.
        /// </summary>
        private Dictionary<string, Rectangle> GetSpriteSheet(string sheet)
        {
            Dictionary<string, Rectangle> tempDictionary = new Dictionary<string, Rectangle>();

            try
            {
                System.IO.Stream stream = TitleContainer.OpenStream("Content/" + sheet);
                System.IO.StreamReader sreader = new System.IO.StreamReader(stream);
                // use StreamReader.ReadLine or other methods to read the file data
                while (sreader.Peek() >= 0)
                {
                   // split at the equals sign
                    string[] sides = sreader.ReadLine().Split('=');

                    // trim the right side and split based on spaces
                    string[] rectParts = sides[1].Trim().Split(' ');

                    // create a rectangle from those parts
                    var r = new Rectangle(
                        int.Parse(rectParts[0], CultureInfo.CurrentCulture),
                        int.Parse(rectParts[1], CultureInfo.CurrentCulture),
                        int.Parse(rectParts[2], CultureInfo.CurrentCulture),
                        int.Parse(rectParts[3], CultureInfo.CurrentCulture));

                    // add the name and rectangle to the dictionary
                    tempDictionary.Add(sides[0].Trim().ToLower(CultureInfo.CurrentCulture), r);
            
                }

                stream.Close();
            }
            catch (System.IO.FileNotFoundException)
            {
                // this will be thrown by OpenStream if gamedata
                // doesn't exist in the title storage location
            }
            return tempDictionary;
        }

        /// <summary>
        /// Load graphics Content for the game.
        /// </summary>
        public override void LoadContent()
        {
            var viewport = new Vector2(Global.Graphics.PreferredBackBufferWidth,
                                       Global.Graphics.PreferredBackBufferHeight);

            monsterSheet = Global.content.Load<Texture2D>("MonsterSheet");
            _monstersTexture = GetSpriteSheet("MonsterSheet.txt");

            UniquesTexture = ( from entry in _monstersTexture
                where (entry.Key.Contains("unique"))
                select entry).ToDictionary(kvp => kvp.Key, kvp => kvp.Value);

            damageSheet = Global.content.Load<Texture2D>("Damagefx");
            damageTexture = GetSpriteSheet("Damagefx.txt");

            itemsSheet = Global.content.Load<Texture2D>("ItemsSheet");
            _itemsTexture = GetSpriteSheet("ItemsSheet.txt");

            weaponSheet = Global.content.Load<Texture2D>("WeaponSheet");
            weaponTexture = GetSpriteSheet("WeaponSheet.txt");

            shieldSheet = Global.content.Load<Texture2D>("ShieldSheet");
            shieldTexture = GetSpriteSheet("ShieldSheet.txt");

            helmSheet = Global.content.Load<Texture2D>("HelmSheet");
            helmTexture = GetSpriteSheet("HelmSheet.txt");

            hauberkSheet = Global.content.Load<Texture2D>("hauberkSheet");
            hauberkTexture = GetSpriteSheet("HauberkSheet.txt");
            
            brassairtsSheet = Global.content.Load<Texture2D>("brassairtsSheet");
            brassairtsTexture = GetSpriteSheet("brassairtsSheet.txt");
            
            vambraceSheet = Global.content.Load<Texture2D>("vambracesSheet");
            vambraceTexture = GetSpriteSheet("vambracesSheet.txt");
            
            gauntletsSheet = Global.content.Load<Texture2D>("gauntletsSheet");
            gauntletsTexture = GetSpriteSheet("gauntletsSheet.txt");

            gambesonSheet = Global.content.Load<Texture2D>("GambesonSheet");
            gambesonTexture = GetSpriteSheet("GambesonSheet.txt");
            
            cuissesSheet = Global.content.Load<Texture2D>("cuissesSheet");
            cuissesTexture = GetSpriteSheet("cuissesSheet.txt");
            
            greavesSheet = Global.content.Load<Texture2D>("greavesSheet");
            greavesTexture = GetSpriteSheet("greavessheet.txt");
            
            solleretSheet = Global.content.Load<Texture2D>("solleretsSheet");
            solleretTexture = GetSpriteSheet("solleretsSheet.txt");

            player.GameObject = new GameObject(Global.content, "Monsters/" + player.Data.Traits[(int)Global.Trait.Class], 1,
                                               viewport);
            player.GameObject.Position = new Vector2(640, 360);
            player.GameObject.Sprite.Scale = 1.0f;

//#if XBOX
//            GameObject friend = new GameObject(Global.content, "Monsters/Ur-Paladin", 1, viewport);
//            friend.Position = new Vector2(640, 400);
//            friend.Sprite.Scale = 1.0f;
//            friend.Alive = false;
//            friendObject.Add(friend);
//#endif

            terrainStructs = new Dictionary<string, Terrain>();

            terrainStructs.Add("forest",
                               new Terrain(Global.content.Load<Texture2D>("Terrain/forestpath"),
                                           Global.content.Load<Texture2D>("Terrain/forestback"),
                                           Global.content.Load<Texture2D>("Terrain/sky")));

            terrainStructs.Add("swamp",
                    new Terrain(Global.content.Load<Texture2D>("Terrain/swamppath"),
                                Global.content.Load<Texture2D>("Terrain/swampback"),
                                Global.content.Load<Texture2D>("Terrain/sky")));

            terrainStructs.Add("desert",
                               new Terrain(Global.content.Load<Texture2D>("Terrain/desertpath"),
                                           Global.content.Load<Texture2D>("Terrain/desertback"),
                                           Global.content.Load<Texture2D>("Terrain/sky")));
            
            Terrain NextTerrain = terrainStructs[(string)Global.Pick(terrainStructs.Keys.ToArray())];

            townTerrain = new Terrain(Global.content.Load<Texture2D>("Terrain/townpath"),
                                      Global.content.Load<Texture2D>("Terrain/townback"),
                                      Global.content.Load<Texture2D>("Terrain/sky"));

            _scrBackground = new ScrollingBackground();
            _scrBackground.Load(ScreenManager.GraphicsDevice, NextTerrain.path);
            _scrBackground.Screenpos = new Vector2(_scrBackground.Screenpos.X, 360);
            _scrBackground.Update(1);

            _scrBackgroundTrees = new ScrollingBackground();
            _scrBackgroundTrees.Load(ScreenManager.GraphicsDevice, NextTerrain.background);
            _scrBackgroundTrees.Screenpos = new Vector2(_scrBackgroundTrees.Screenpos.X, 220);
            _scrBackgroundTrees.Update(1);

            _scrBackgroundClouds = new ScrollingBackground();
            _scrBackgroundClouds.Load(ScreenManager.GraphicsDevice, terrainStructs["forest"].sky);
            _scrBackgroundClouds.Screenpos = new Vector2(_scrBackgroundClouds.Screenpos.X, 110);
            _scrBackgroundClouds.Update(1);

            equipObject = new GameObject(Global.content, "Monsters/Cow", 1, viewport);
            equipObject.Position = new Vector2(Global.Graphics.PreferredBackBufferWidth / 2,
                                               Global.Graphics.PreferredBackBufferHeight / 2);

            goldObject = new GameObject(Global.content, "Items/Gold", 1, viewport);
            goldObject.Position = new Vector2(Global.Graphics.PreferredBackBufferWidth / 2,
                                              Global.Graphics.PreferredBackBufferHeight / 2);

            damageObject = new GameObject(Global.content, "Monsters/Cow", 1, viewport);
/*             damageObject.Position = new Vector2(Global.Graphics.PreferredBackBufferWidth / 2,
                                              Global.Graphics.PreferredBackBufferHeight / 2);
 */
            damageObject.Position = new Vector2(688, 350);
            damageObject.Sprite.Scale = 1.0f;
            damageObject.Alive = false;

            ItemObject = new GameObject(Global.content, "Monsters/Cow", 1, viewport);
            ItemObject.Position = new Vector2(Global.Graphics.PreferredBackBufferWidth / 2,
                                              Global.Graphics.PreferredBackBufferHeight / 2);
            ItemObject.Alive = false;

            monsterObject = new GameObject(Global.content, "Monsters/Cow", 1, viewport);
            monsterObject.Position = new Vector2(Global.Graphics.PreferredBackBufferWidth / 2,
                                                 Global.Graphics.PreferredBackBufferHeight / 2);

#if DEBUG
            // Test all images to types to make sure their are no image exceptions in runtime
            foreach (Item x in Global.Monsters)
            {
                monsterObject.Sprite.Rect = _monstersTexture[x.Name.ToLower()];
                if (!x.Extra[0].ToLower().Equals("*"))
                    ItemObject.Sprite.Rect = _itemsTexture[x.Extra[0].ToLower()];
            }
            foreach (Item x in Global.Classes)
            {
                monsterObject.Sprite.Rect = _monstersTexture[x.Name.ToLower()];
            }
#endif


            // once the load has finished, we use ResetElapsedTime to tell the game's
            // timing mechanism that we have just finished a very long frame, and that
            // it should not try to catch up.
            ScreenManager.Game.ResetElapsedTime();
        }

        public string findTopInt(ListView lvList)
        {
            //int topIndex = 0;
            int max = 0;
            string returnstring = "";
            foreach (string[] row in lvList.Rows)
            {
                if (int.Parse(row[1]) > max)
                {
                    max = int.Parse(row[1]);
                    returnstring = row[0] + ": " + row[1];
                }
            }

            return returnstring;
        }

        public void SaveGame()
        {
#if DEBUG || XBOX
            string topStat = findTopInt(lvStat);
            int randEquip = Global.RandomValue.Next(lvEquipment.Rows.Count);
            string topEquip = lvEquipment.Rows[randEquip][0] + ": " + lvEquipment.Rows[randEquip][1];
            string topSpell = "None";
            if (lvSpellbook.Rows.Count > 0)
            {
                int randSpell = Global.RandomValue.Next(lvSpellbook.Rows.Count);
                topSpell = lvSpellbook.Rows[randSpell][0] + ": " + lvSpellbook.Rows[randSpell][1];
            }

            SpynDoctor.TopScoreEntry entry = new SpynDoctor.TopScoreEntry(
                Gamer.SignedInGamers[Global.CurrentPlayer].Gamertag,
                DateTime.Now.Ticks,
                lvTrait.Rows[(int) Global.Trait.Name][1],
                lvTrait.Rows[(int) Global.Trait.Race][1],
                lvTrait.Rows[(int) Global.Trait.Class][1],
                lvTrait.Rows[(int) Global.Trait.Level][1],
                topStat,
                lvPlot.Rows[lvPlot.Rows.Count - 1][1],
                topEquip,
                topSpell
                );

            Global.Scores.AddEntry(0, entry, Global.SyncManager);
#endif

            // make sure the device is ready
            if (Global.SaveDevice.IsReady)
            {
                // save a file asynchronously. this will trigger IsBusy to return true
                // for the duration of the save process.
                Global.SaveDevice.SaveAsync(
                    "LevelUp",
                    lvTrait.Rows[0][1] + ".sav",
                    stream =>
                    {
                        // simulate a really, really long save operation so we can visually see that
                        // IsBusy stays true while we're saving
                        // Thread.Sleep(3000);
                        // Convert the object to XML data and put it in the stream.
                        // Stream xmlStream = new Stream();
                        var data = new SaveGameData();
                        var serializer = new System.Xml.Serialization.XmlSerializer(typeof(SaveGameData));

                        int counter = 0;
                        data.LastSave = DateTime.Now;
                        data.Birth = player.Data.Birth;
#if DEBUG || XBOX
                        data.Gamertag = Gamer.SignedInGamers[Global.CurrentPlayer].Gamertag;
#else
                        data.Gamertag = "LocalPlayer";
#endif
                        data.Traits = new string[lvTrait.Rows.Count];
                        for (counter = 0; counter < lvTrait.Rows.Count; counter++)
                            data.Traits[counter] = lvTrait.Rows[counter][1];

                        data.Stats = new string[lvStat.Rows.Count];
                        for (counter = 0; counter < lvStat.Rows.Count; counter++)
                            data.Stats[counter] = lvStat.Rows[counter][1];

                        data.ExperienceTotal = player.Data.ExperienceTotal;
                        data.ExperienceMax = player.Data.ExperienceMax;
                        data.ExperiencePercentage = player.Data.ExperiencePercentage;

                        data.Equipment = new string[lvEquipment.Rows.Count];
                        for (counter = 0; counter < lvEquipment.Rows.Count; counter++)
                            data.Equipment[counter] = lvEquipment.Rows[counter][1];

                        data.Spells = new string[lvSpellbook.Rows.Count];
                        data.Spellsvalue = new string[lvSpellbook.Rows.Count];
                        for (counter = 0; counter < lvSpellbook.Rows.Count; counter++)
                        {
                            data.Spells[counter] = lvSpellbook.Rows[counter][0];
                            data.Spellsvalue[counter] = lvSpellbook.Rows[counter][1];
                        }

                        data.Plot = new string[lvPlot.Rows.Count];
                        for (counter = 0; counter < lvPlot.Rows.Count; counter++)
                            data.Plot[counter] = lvPlot.Rows[counter][1];
                        data.PlotTotal = player.Data.PlotTotal;
                        data.PlotMax = player.Data.PlotMax;
                        data.PlotPercentage = player.Data.PlotPercentage;

                        data.Quests = new string[lvQuests.Rows.Count];
                        for (counter = 0; counter < lvQuests.Rows.Count; counter++)
                            data.Quests[counter] = lvQuests.Rows[counter][1];
                        data.QuestsTotal = player.Data.QuestsTotal;
                        data.QuestsMax = player.Data.QuestsMax;
                        data.QuestsPercentage = player.Data.QuestsPercentage;

                        data.Questmonster = player.Data.Questmonster;

                        data.Items = new string[lvInventory.Rows.Count];
                        data.Itemsvalue = new string[lvInventory.Rows.Count];
                        for (counter = 0; counter < lvInventory.Rows.Count; counter++)
                        {
                            data.Items[counter] = lvInventory.Rows[counter][0];
                            data.Itemsvalue[counter] = lvInventory.Rows[counter][1];
                        }
                        data.Itemcount = player.Data.Itemcount;
                        serializer.Serialize(stream, data);
                    });
#if WINDOWS
                SendWebRequest();
#endif
            }
        }

        public void SendWebRequest()
        {
            var data = new SaveGameData();

            int counter = 0;
            data.LastSave = DateTime.Now;
            data.Birth = player.Data.Birth;
#if DEBUG || XBOX
            data.Gamertag = Gamer.SignedInGamers[Global.CurrentPlayer].Gamertag;
#else
            data.Gamertag = "LocalPlayer";
#endif
            data.Traits = new string[lvTrait.Rows.Count];
            for (counter = 0; counter < lvTrait.Rows.Count; counter++)
                data.Traits[counter] = lvTrait.Rows[counter][1];

            data.Stats = new string[lvStat.Rows.Count];
            for (counter = 0; counter < lvStat.Rows.Count; counter++)
                data.Stats[counter] = lvStat.Rows[counter][1];

            data.ExperienceTotal = player.Data.ExperienceTotal;
            data.ExperienceMax = player.Data.ExperienceMax;
            data.ExperiencePercentage = player.Data.ExperiencePercentage;

            data.Equipment = new string[lvEquipment.Rows.Count];
            for (counter = 0; counter < lvEquipment.Rows.Count; counter++)
                data.Equipment[counter] = lvEquipment.Rows[counter][1];

            data.Spells = new string[lvSpellbook.Rows.Count];
            data.Spellsvalue = new string[lvSpellbook.Rows.Count];
            for (counter = 0; counter < lvSpellbook.Rows.Count; counter++)
            {
                data.Spells[counter] = lvSpellbook.Rows[counter][0];
                data.Spellsvalue[counter] = lvSpellbook.Rows[counter][1];
            }

            data.Plot = new string[lvPlot.Rows.Count];
            for (counter = 0; counter < lvPlot.Rows.Count; counter++)
                data.Plot[counter] = lvPlot.Rows[counter][1];
            data.PlotTotal = player.Data.PlotTotal;
            data.PlotMax = player.Data.PlotMax;
            data.PlotPercentage = player.Data.PlotPercentage;

            data.Quests = new string[lvQuests.Rows.Count];
            for (counter = 0; counter < lvQuests.Rows.Count; counter++)
                data.Quests[counter] = lvQuests.Rows[counter][1];
            data.QuestsTotal = player.Data.QuestsTotal;
            data.QuestsMax = player.Data.QuestsMax;
            data.QuestsPercentage = player.Data.QuestsPercentage;

            data.Questmonster = player.Data.Questmonster;

            data.Items = new string[lvInventory.Rows.Count];
            data.Itemsvalue = new string[lvInventory.Rows.Count];
            for (counter = 0; counter < lvInventory.Rows.Count; counter++)
            {
                data.Items[counter] = lvInventory.Rows[counter][0];
                data.Itemsvalue[counter] = lvInventory.Rows[counter][1];
            }
            data.Itemcount = player.Data.Itemcount;

#if WINDOWS
            try
            {
            JavaScriptSerializer jss = new JavaScriptSerializer();
            string json = "data=" + jss.Serialize(data);
            Debug.Print(json);
            // Create a request using a URL that can receive a post. 
            HttpWebRequest request = (HttpWebRequest) WebRequest.Create("http://www.saigumi.com/test.php");
            // Set the Method property of the request to POST.
            // Create POST data and convert it to a byte array.
            byte[] buffer = Encoding.ASCII.GetBytes(json);
            request.ContentLength = buffer.Length;
            request.ContentType = "application/x-www-form-urlencoded";
            request.AllowAutoRedirect = false;
            request.AllowWriteStreamBuffering = false;
            request.Timeout = 10000;
            request.PreAuthenticate = true;
            request.Credentials = new NetworkCredential("john", "win32api");
            request.Method = WebRequestMethods.Http.Post;
            Stream PostData = request.GetRequestStream();
            PostData.Write(buffer, 0, buffer.Length);
            // Get the response.
            HttpWebResponse response = (HttpWebResponse) request.GetResponse();
            // Display the status.
            Console.WriteLine(((HttpWebResponse) response).StatusDescription);
            // Get the stream containing content returned by the server.
            Stream dataStream = response.GetResponseStream();
            // Open the stream using a StreamReader for easy access.    
            StreamReader reader = new StreamReader(dataStream);
            // Read the content.
            string responseFromServer = reader.ReadToEnd();
            // Display the content.
            Console.WriteLine(responseFromServer);
            reader.Close();
            response.Close();
            // Clean up the streams.
            PostData.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }


#endif
        }

        public void SaveGameOnExit()
        {
            // make sure the device is ready
            if (Global.SaveDevice.IsReady)
            {
                // save a file asynchronously. this will trigger IsBusy to return true
                // for the duration of the save process.
                Global.SaveDevice.Save(
                    "LevelUp",
                    lvTrait.Rows[0][1] + ".sav",
                    stream =>
                    {
                        // Convert the object to XML data and put it in the stream.
                        var data = new SaveGameData();
                        var serializer = new XmlSerializer(typeof(SaveGameData));

                        int counter = 0;
                        data.LastSave = DateTime.Now;
                        data.Birth = player.Data.Birth;
#if DEBUG || XBOX
                        data.Gamertag = Gamer.SignedInGamers[Global.CurrentPlayer].Gamertag;
#else
                        data.Gamertag = "LocalPlayer";
#endif
                        data.Traits = new string[lvTrait.Rows.Count];
                        for (counter = 0; counter < lvTrait.Rows.Count; counter++)
                            data.Traits[counter] = lvTrait.Rows[counter][1];

                        data.Stats = new string[lvStat.Rows.Count];
                        for (counter = 0; counter < lvStat.Rows.Count; counter++)
                            data.Stats[counter] = lvStat.Rows[counter][1];

                        data.ExperienceTotal = player.Data.ExperienceTotal;
                        data.ExperienceMax = player.Data.ExperienceMax;
                        data.ExperiencePercentage = player.Data.ExperiencePercentage;

                        data.Equipment = new string[lvEquipment.Rows.Count];
                        for (counter = 0; counter < lvEquipment.Rows.Count; counter++)
                            data.Equipment[counter] = lvEquipment.Rows[counter][1];

                        data.Spells = new string[lvSpellbook.Rows.Count];
                        data.Spellsvalue = new string[lvSpellbook.Rows.Count];
                        for (counter = 0; counter < lvSpellbook.Rows.Count; counter++)
                        {
                            data.Spells[counter] = lvSpellbook.Rows[counter][0];
                            data.Spellsvalue[counter] = lvSpellbook.Rows[counter][1];
                        }

                        data.Plot = new string[lvPlot.Rows.Count];
                        for (counter = 0; counter < lvPlot.Rows.Count; counter++)
                            data.Plot[counter] = lvPlot.Rows[counter][1];
                        data.PlotTotal = player.Data.PlotTotal;
                        data.PlotMax = player.Data.PlotMax;
                        data.PlotPercentage = player.Data.PlotPercentage;

                        data.Quests = new string[lvQuests.Rows.Count];
                        for (counter = 0; counter < lvQuests.Rows.Count; counter++)
                            data.Quests[counter] = lvQuests.Rows[counter][1];
                        data.QuestsTotal = player.Data.QuestsTotal;
                        data.QuestsMax = player.Data.QuestsMax;
                        data.QuestsPercentage = player.Data.QuestsPercentage;

                        data.Questmonster = player.Data.Questmonster;

                        data.Items = new string[lvInventory.Rows.Count];
                        data.Itemsvalue = new string[lvInventory.Rows.Count];
                        for (counter = 0; counter < lvInventory.Rows.Count; counter++)
                        {
                            data.Items[counter] = lvInventory.Rows[counter][0];
                            data.Itemsvalue[counter] = lvInventory.Rows[counter][1];
                        }
                        data.Itemcount = player.Data.Itemcount;

                        serializer.Serialize(stream, data);
                    });
            }
        }

        /// <summary>
        /// Unload graphics Content used by the game.
        /// </summary>
        public override void UnloadContent()
        {
            SaveGameOnExit();
            player.Queue.Clear();
#if DEBUG || XBOX
            if ((Global.SyncManager.Enabled == true) && !Global.SyncManager.IsStopping())
                Global.SyncManager.Stop(null, false);
#endif
            GameTime time = new GameTime(new TimeSpan(1), new TimeSpan(1));
            OrbFormCollection.Remove(frmStatus);
            OrbFormCollection.Remove(frmEquipment);
            OrbFormCollection.Remove(frmSpellbook);
            OrbFormCollection.Remove(frmInventory);
            OrbFormCollection.Remove(frmPlot);
            OrbFormCollection.Remove(frmQuests);
            OrbFormCollection.Remove(frmChar);
            OrbFormCollection.Remove(frmTutorial);
            Global.content.Unload();
        }

        /// <summary>
        /// Updates the state of the game. This method checks the GameScreen.IsActive
        /// property, so the game will stop updating when the pause menu is active,
        /// or if you tab away to a different application.
        /// </summary>
        public override void Update(GameTime gameTime, bool otherScreenHasFocus,
                                    bool coveredByOtherScreen)
        {
            //otherScreenHasFocus = false;
            base.Update(gameTime, otherScreenHasFocus, false);

            float elapsed = (float)gameTime.ElapsedGameTime.TotalSeconds;
            //            float elapsed = (float)gameTime.ElapsedGameTime.TotalMilliseconds; //Superspeed!
            
            if (firstTimeHide)
            {
                firstTimeHide = false;
                frmChar.Hide();
                frmEquipment.Hide();
                frmSpellbook.Hide();
                frmInventory.Hide();
                frmPlot.Hide();
                frmQuests.Hide();
            }
            if (frmTutorial.Visible)
            {
                frmTutorial.TopMost = true;
            }
            else
            {
                frmStatus.TopMost = true;
            }

//#if DEBUG || XBOX
//            if (Global.SyncComplete)
//           {
//                Global.SyncComplete = false;
//                this.AddTask("task", "A new friend joins you on your adventure.", 1);
//            }
//#endif
            // Gradually fade in or out depending on whether we are covered by the pause screen.
            if (coveredByOtherScreen)
            {
                pauseAlpha = Math.Min(pauseAlpha + 1f / 32, 1);
                frmStatus.Enabled = false;
                frmChar.Hide();
                frmEquipment.Hide();
                frmSpellbook.Hide();
                frmInventory.Hide();
                frmPlot.Hide();
                frmQuests.Hide();
            }
            else
            {
                pauseAlpha = Math.Max(pauseAlpha - 1f / 32, 0);
            }

            if (pauseAlpha == 0)
            {
                frmStatus.Enabled = true;
            }

            this.actionTimer -= elapsed;

            if (nextSave < DateTime.Now)
            {
                SaveGame();
                nextSave = DateTime.Now.AddMinutes(15);
            }
            if (this.actionTimer <= 0)
            {
                Dequeue();
            }
            float progress = (1.0f - (this.actionTimer/this.actionTimerMax))*100.0f;
            if (this.actionTimer <= 0)
            {
                this._barAction.Value = 100;
            }
            else
            {
                this._barAction.Value = (int) progress;
            }
            this._barAction.EndUpdate();

            // Game Object Graphics
            player.GameObject.Update(elapsed);

            if (equipObject.Alive)
                equipObject.Update(elapsed);
            if (ItemObject.Alive)
                ItemObject.Update(elapsed);
            if (monsterObject.Alive)
            {
                if (monsterObject.Dieing)
                    monsterObject.Velocity = new Vector2(monsterObject.Velocity.X,
                                                            monsterObject.Velocity.Y + (512.0f*elapsed));
                if (monsterObject.SpriteRectangle.Intersects(player.GameObject.SpriteRectangle))
                {
                    if (monsterObject.Sprite.Rect == _monstersTexture["portal"])
                    {
                        pauseAlpha = 1.0f;
                        monsterObject.Position = new Vector2(Global.Graphics.PreferredBackBufferWidth,
                                                        Global.Graphics.PreferredBackBufferHeight / 2); 
                        monsterObject.Alive = false;
                        if (lblAction.Text.IndexOf("market") > 0)
                        {
                            _scrBackground.Load(ScreenManager.GraphicsDevice, townTerrain.path);
                            _scrBackgroundTrees.Load(ScreenManager.GraphicsDevice, townTerrain.background);
                        }
                        else
                        {
                            Terrain NextTerrain = terrainStructs[(string) Global.Pick(terrainStructs.Keys.ToArray())];
                            _scrBackground.Load(ScreenManager.GraphicsDevice, NextTerrain.path);
                            _scrBackgroundTrees.Load(ScreenManager.GraphicsDevice, NextTerrain.background);
                        }
                    } else
                    {
                        monsterObject.RotationSpeed = MathHelper.ToDegrees(-20);
                        monsterObject.Velocity = new Vector2(-256.0f, -256.0f);
                        monsterObject.Dieing = true;
                        damageObject.Sprite.Rect = damageTexture["bang1"];
                        damageObjectAliveCounter = 0.5f;
                        damageObject.RotationSpeed = MathHelper.ToDegrees(-20);
                        damageObject.Alive = true;
                    }
                }
                monsterObject.Update(elapsed);
            }
                
            if (damageObject.Alive)
            {
                damageObject.Update(elapsed);
                damageObjectAliveCounter -= elapsed;
                if (damageObjectAliveCounter <= 0)
                {
                    damageObject.Alive = false;
                }
            }

            if (goldObject.Alive)
            {
                goldObject.Update(elapsed);
            }

            _scrBackground.Update(128.0f * elapsed);
            _scrBackgroundTrees.Update(64.0f * elapsed);
            _scrBackgroundClouds.Update(32.0f * elapsed);
        }

        private void AddEquip(ListView list, int key, string value)
        {
            list.BeginUpdate();
            list.Rows[key][1] = value;
            list.EndUpdate();
        }

        private void AddItem(ListView list, string key, int value)
        {
            bool found = false;
            list.BeginUpdate();
            for (int rowCounter = 0; rowCounter < list.Rows.Count; rowCounter++)
            {
                if (list.Rows[rowCounter][0].Equals(key))
                {
                    string listValue = (int.Parse(list.Rows[rowCounter][1], CultureInfo.CurrentCulture) + value).ToString(CultureInfo.CurrentCulture);
                    list.Rows[rowCounter][1] = listValue;
                    found = true;
                    if (int.Parse(list.Rows[rowCounter][1], CultureInfo.CurrentCulture) <= 0)
                        list.Rows.RemoveAt(rowCounter);
                    break;
                }
            }

            if (found == false)
            {
                var row = new string[2];
                row[0] = key;
                row[1] = value.ToString(CultureInfo.CurrentCulture);
                list.Rows.Add(row);
            }

            list.EndUpdate();

            if (list.InstanceName.Equals("lvInventory"))
            {
                if (!key.Equals("Gold", StringComparison.CurrentCulture))
                {
                    player.Data.Itemcount += value;
                }

                float encValue = ((float)player.Data.Itemcount / (int.Parse(lvStat.Rows[(int)Global.Stat.Str][1], CultureInfo.CurrentCulture) + 10)) *
                                 100;
                if (encValue > 100)
                {
                    encValue = 100;
                }

                if (encValue < 0)
                {
                    encValue = 0;
                }
                this._barEncumbrance.BeginUpdate();
                this._barEncumbrance.Value = (int)encValue;
                this._barEncumbrance.EndUpdate();
            }
        }

        private void AddRoman(ListView list, string key, int value)
        {
            bool found = false;
            list.BeginUpdate();
            for (int rowCounter = 0; rowCounter < list.Rows.Count; rowCounter++)
            {
                if (list.Rows[rowCounter][0].Equals(key))
                {
                    string listValue = ToRoman((ToArabic(list.Rows[rowCounter][1]) + value));
                    list.Rows[rowCounter][1] = listValue;
                    found = true;
                    list.EndUpdate();
                    break;
                }
            }

            if (found == false)
            {
                var row = new string[2];
                row[0] = key;
                row[1] = ToRoman(value);
                list.Rows.Add(row);
                list.EndUpdate();
            }
            list.EndUpdate();
        }

        private void AddTask(string action, string text, int value)
        {
            var queueItem = new Player.QueueItem(action, value, text);
            player.Queue.Add(queueItem);
        }

        private string Big(int m, string s)
        {
            return Prefix(new object[] { "greater", "massive", "enormous", "giant", "titanic" }, m, s, " ");
        }

        private string BoringItem()
        {
            return (string)Pick(Global.BoringItems);
        }

        private void CompleteQuest()
        {
            lvQuests.BeginUpdate();
            player.Data.QuestsMax = 50 + RandomLow(1000);
            player.Data.QuestsTotal = 0;
            this._barQuests.BeginUpdate();
            this._barQuests.Value = 0;
            this._barQuests.EndUpdate();

            if (lvQuests.Rows.Count > 0)
            {
                lvQuests.Rows[lvQuests.Rows.Count - 1][0] = "X";
                switch (Global.RandomValue.Next(4))
                {
                    case 0:
                        WinSpell();
                        break;
                    case 1:
                        WinEquip();
                        break;
                    case 2:
                        WinStat();
                        break;
                    case 3:
                        WinItem();
                        break;
                }
            }
            while (lvQuests.Rows.Count > 99)
                lvQuests.Rows.RemoveAt(0);

            player.Questmonster = "";
            string caption = "";
            int level;
            int montag;
            Item monster;
            int l;
            switch (Global.RandomValue.Next(5))
            {
                case 0:
                    level = int.Parse(lvTrait.Rows[(int)Global.Trait.Level][1], CultureInfo.CurrentCulture);
                    int lev = 0;
                    for (int i = 1; i <= 4; ++i)
                    {
                        montag = Global.RandomValue.Next(Global.Monsters.Length);
                        monster = Global.Monsters[montag];

                        if (i == 1 || Math.Abs(monster.Value - level) < Math.Abs(lev - level))
                        {
                            lev = monster.Value;
                            player.Questmonster = monster.Name;
                            player.Questmonsterindex = montag;
                        }
                    }
                    caption = "Exterminate " + this.Definite(player.Questmonster.Split('|')[0], 2);
                    break;
                case 1:
                    caption = "Seek " + Definite(this.InterestingItem(), 1);
                    break;
                case 2:
                    caption = "Deliver this " + this.BoringItem();
                    break;
                case 3:
                    caption = "Fetch me " + this.Indefinite(this.BoringItem(), 1);
                    break;
                case 4:
                    int mlev = 0;
                    level = int.Parse(lvTrait.Rows[(int)Global.Trait.Level][1], CultureInfo.CurrentCulture);
                    for (int ii = 1; ii <= 2; ++ii)
                    {
                        montag = Global.RandomValue.Next(Global.Monsters.Length);
                        monster = Global.Monsters[montag];
                        l = monster.Value;
                        if ((ii == 1) || (Math.Abs(l - level) < Math.Abs(mlev - level)))
                        {
                            mlev = l;
                            player.Questmonster = monster.Name;
                        }
                    }
                    caption = "Placate " + this.Definite(player.Questmonster, 2);
                    player.Questmonster = ""; // We're trying to placate them, after all
                    break;
            }

            var row = new string[2];
            row[0] = " ";
            row[1] = caption;
            lvQuests.Rows.Add(row);
            lvQuests.EndUpdate();
        }

        private string Definite(string s, int qty)
        {
            if (qty > 1)
                s = Formatting.Pluralize(qty, s);
            return "the " + s;
        }

        private void Dequeue()
        {
            this.actionTimer = 0;
            this.actionTimerMax = 0;

            if (player.Queue.Count > 0)
            {
                switch (player.Queue[0].Action)
                {
                    case "equip":
                        lvEquipment.BeginUpdate();
                        lvEquipment.Rows[player.Queue[0].Value][1] = player.Queue[0].Text;
                        lvEquipment.EndUpdate();
                        break;
                    case "kill":
                        this._barExperience.BeginUpdate();
                        if (this._barExperience.Value == 100)
                        {
                            LevelUp();
                        }
                        else
                        {
                            player.Data.ExperienceTotal += 4;
                            float value = (player.Data.ExperienceTotal / (float)player.Data.ExperienceMax) * 100;
                            if (value > 100)
                            {
                                value = 100;
                            }
                            this.lblExperience.Text = "Experience - " + player.Data.ExperienceTotal.ToString() + "/" + player.Data.ExperienceMax.ToString();
                            this._barExperience.Value = (int)value;
                        }
                        this._barExperience.EndUpdate();

                        // advance plot
                        this._barPlot.BeginUpdate();
                        if (this._barPlot.Value == 100)
                        {
                            InterplotCinematic();
                        }
                        else
                        {
                            player.Data.PlotTotal += 1;
                            float value = (player.Data.PlotTotal / (float)player.Data.PlotMax) * 100;
                            if (value > 100)
                            {
                                value = 100;
                            }
                            this.lblPlot.Text = "Plot - " + player.Data.PlotTotal.ToString() + "/" + player.Data.PlotMax.ToString();

                            this._barPlot.Value = (int)value;
                        }
                        this._barPlot.EndUpdate();

                        // advance quest
                        this._barQuests.BeginUpdate();
                        if (this._barQuests.Value >= 100)
                        {
                            CompleteQuest();
                        }
                        else
                        {
                            this._barQuests.BeginUpdate();
                            player.Data.QuestsTotal += 1;
                            float value = (player.Data.QuestsTotal / (float)player.Data.QuestsMax) * 100;
                            if (value > 100)
                            {
                                value = 100;
                            }

                            this.lblQuests.Text = "Quests - " + player.Data.QuestsTotal.ToString() + "/" + player.Data.QuestsMax.ToString();
                            this._barQuests.Value = (int)value;
                            this._barQuests.EndUpdate();
                        }
                        break;
                    case "give":
                        if (player.Queue[0].Text.IndexOf("*") > -1)
                        {
                            WinItem();
                        }
                        else
                        {
                            if (!player.Queue[0].Text.Equals("Gold"))
                            {
                                if (player.Queue[0].Text.IndexOf(" of ") > -1)
                                {
                                    ItemObject.Sprite.Rect = _itemsTexture["acorn"];
                                }
                                else
                                {
                                    string[] temp = player.Queue[0].Text.Split(' ');
                                    ItemObject.Sprite.Rect = _itemsTexture[temp[temp.Length - 1].ToLower(CultureInfo.CurrentCulture)];
                                }

                                if (player.Queue[0].Value < 0)
                                {
                                    ItemObject.Position = new Vector2(Global.Graphics.PreferredBackBufferWidth / 2,
                                                                      Global.Graphics.PreferredBackBufferHeight / 2);
                                    ItemObject.Velocity = new Vector2(-256.0f, -256.0f);
                                    ItemObject.RotationSpeed = MathHelper.ToDegrees(-20);
                                    ItemObject.Rotation = 0;
                                    ItemObject.Alive = true;
                                }
                                else
                                {
                                    ItemObject.Position = new Vector2(Global.Graphics.PreferredBackBufferWidth / 2,
                                                                      Global.Graphics.PreferredBackBufferHeight / 2);
                                    ItemObject.Velocity = new Vector2(256.0f, 128.0f);
                                    ItemObject.RotationSpeed = 0;
                                    ItemObject.Rotation = 0;
                                    ItemObject.Alive = true;
                                }
                            }
                            else
                            {
                                if (player.Queue[0].Value < 0)
                                {
                                    goldObject.Position = new Vector2(Global.Graphics.PreferredBackBufferWidth / 2,
                                                                      Global.Graphics.PreferredBackBufferHeight / 2);
                                    goldObject.Velocity = new Vector2(-256.0f, -256.0f);
                                    goldObject.RotationSpeed = MathHelper.ToDegrees(-20);
                                    goldObject.Rotation = 0;
                                    goldObject.Alive = true;
                                }
                                else
                                {
                                    goldObject.Position = new Vector2(Global.Graphics.PreferredBackBufferWidth / 2,
                                                                      Global.Graphics.PreferredBackBufferHeight / 2);
                                    goldObject.Velocity = new Vector2(256.0f, 128.0f);
                                    goldObject.RotationSpeed = 0;
                                    goldObject.Rotation = 0;
                                    goldObject.Alive = true;
                                }
                            }
                            AddItem(lvInventory, player.Queue[0].Text, player.Queue[0].Value);
                        }

                        break;
                    case "buy":
                        // buy some equipment
                        this.AddItem(lvInventory, "Gold", -EquipPrice());
                        WinEquip();
                        monsterObject.Position = new Vector2(Global.Graphics.PreferredBackBufferWidth,
                                            Global.Graphics.PreferredBackBufferHeight / 2); 
                        monsterObject.Velocity = new Vector2(-256.0f, 0.0f);
                        monsterObject.Rotation = 0;
                        monsterObject.RotationSpeed = 0;
                        monsterObject.Alive = true;
                        monsterObject.Dieing = false;
                        monsterObject.Sprite.Rect = _monstersTexture["portal"];
                        this.AddTask("task", "Heading to the killing fields", 4);
                    break;
                    case "sell":
                        int amt = 2 * player.Queue[0].Value * int.Parse(lvTrait.Rows[(int)Global.Trait.Level][1], CultureInfo.CurrentCulture);
                        if (lvInventory.Rows[lvInventory.Rows.Count - 1][0].IndexOf(" of ") > 0)
                            amt *= (1 + RandomLow(10)) *
                                   (1 + RandomLow(int.Parse(lvTrait.Rows[(int)Global.Trait.Level][1], CultureInfo.CurrentCulture)));
                        this.AddTask("give", "Gold", amt);
                        this.AddTask("market", "Market", 0);
                        break;
                    case "market":
                        if (lvInventory.Rows.Count > 1)
                        {
                            lvInventory.SelectedColumn = 0;
                            this.AddTask("give", lvInventory.Rows[lvInventory.Rows.Count - 1][0],
                                    -int.Parse(lvInventory.Rows[lvInventory.Rows.Count - 1][1], CultureInfo.CurrentCulture));
                            if (lvInventory.Rows[lvInventory.Rows.Count - 1][1].Equals("1"))
                            {
                                this.AddTask("task",
                                        "Selling " +
                                        Indefinite(lvInventory.Rows[lvInventory.Rows.Count - 1][0],
                                                   int.Parse(lvInventory.Rows[lvInventory.Rows.Count - 1][1], CultureInfo.CurrentCulture)), 1);
                            }
                            else
                            {
                                this.AddTask("task",
                                        "Selling " + lvInventory.Rows[lvInventory.Rows.Count - 1][1] + " " +
                                        Indefinite(lvInventory.Rows[lvInventory.Rows.Count - 1][0],
                                                   int.Parse(lvInventory.Rows[lvInventory.Rows.Count - 1][1], CultureInfo.CurrentCulture)), 1);
                            }
                            this.AddTask("sell",
                                    "Selling " +
                                    Indefinite(lvInventory.Rows[lvInventory.Rows.Count - 1][0],
                                               int.Parse(lvInventory.Rows[lvInventory.Rows.Count - 1][1], CultureInfo.CurrentCulture)),
                                    int.Parse(lvInventory.Rows[lvInventory.Rows.Count - 1][1], CultureInfo.CurrentCulture));
                        }
                        else
                        {
                            if (int.Parse(lvInventory.Rows[0][1], CultureInfo.CurrentCulture) > EquipPrice())
                            {
                                player.Data.Itemcount = 0;
                                this.AddTask("task", "Negotiating purchase of better equipment", 5);
                                this.AddTask("buy", "Negotiating purchase of better equipment", 0);
                            }
                        }
                        break;
                    case "task":
                        this.actionTimer = player.Queue[0].Value;
                        this.actionTimerMax = player.Queue[0].Value;

                        // actionTimer = 1; // Debug
                        // actionTimerMax = 1; //Debug
                        lblAction.Text = player.Queue[0].Text + " ";
                        break;
                    case "quest":
                        CompleteQuest();
                        break;
                    case "plot":
                        var row = new string[2];
                        row[0] = " ";
                        row[1] = player.Queue[0].Text;
                        lvPlot.Rows.Add(row);
                        break;
                }

                Player.QueueItem old = player.Queue[0];
                player.Queue.RemoveAt(0);
            }
            else if (this._barEncumbrance.Value >= 100)
            {
                monsterObject.Position = new Vector2(Global.Graphics.PreferredBackBufferWidth,
                                                         Global.Graphics.PreferredBackBufferHeight / 2);
                monsterObject.Velocity = new Vector2(-256.0f, 0.0f);
                monsterObject.Rotation = 0;
                monsterObject.RotationSpeed = 0;
                monsterObject.Alive = true;
                monsterObject.Dieing = false;
                monsterObject.Sprite.Rect = _monstersTexture["portal"];
                this.AddTask("task", "Heading to market to sell loot", 4);
                this.AddTask("market", "Selling", 0);
            }
            else
            {
#if XBOX
                if (Guide.IsTrialMode && (int.Parse(lvTrait.Rows[(int)Global.Trait.Level][1]) >= 3))
                {
                    this.AddTask("task", "Congratulations! You are level 3! Please purchase 'Level Up!' to continue your adventure.", 9999);
                }
                else
                {
#endif
                Item monster = MonsterTask(int.Parse(lvTrait.Rows[(int)Global.Trait.Level][1], CultureInfo.CurrentCulture));
                    this.AddTask("task", "Executing " + monster.Name, 4);
                    monsterObject.Position = new Vector2(Global.Graphics.PreferredBackBufferWidth,
                                                         Global.Graphics.PreferredBackBufferHeight / 2);
                    monsterObject.Velocity = new Vector2(-256.0f, 0.0f);
                    monsterObject.Rotation = 0;
                    monsterObject.RotationSpeed = 0;
                    monsterObject.Alive = true;
                    monsterObject.Dieing = false;

                    if (monster.Extra[1].Contains("Unique"))
                    {
                        string[] rects = UniquesTexture.Keys.ToArray();
                        monsterObject.Sprite.Rect = _monstersTexture[((string)Pick(rects)).ToLower(CultureInfo.CurrentCulture)];
                    }
                    else
                    {
                        monsterObject.Sprite.Rect = _monstersTexture[monster.Extra[1].ToLower(CultureInfo.CurrentCulture)];
                    }
                    this.AddTask("kill", monster.Name, 0);
                    if (monster.Name.IndexOfAny("123456789".ToCharArray()) > -1)
                    {
                        string[] numbers = Regex.Split(monster.Name, @"\D+");
                        foreach (string value in numbers)
                        {
                            if (!string.IsNullOrEmpty(value))
                            {
                                this.AddTask("give", monster.Extra[0], int.Parse(value, CultureInfo.CurrentCulture));
                            }
                        }
                    }
                    else
                    {
                        this.AddTask("give", monster.Extra[0], 1);
                    }
#if XBOX
                }
#endif
            }
        }

        private int EquipPrice()
        {
            return 5 * int.Parse(lvTrait.Rows[(int)Global.Trait.Level][1], CultureInfo.CurrentCulture) *
                   int.Parse(lvTrait.Rows[(int)Global.Trait.Level][1], CultureInfo.CurrentCulture) +
                   10 * int.Parse(lvTrait.Rows[(int)Global.Trait.Level][1], CultureInfo.CurrentCulture) + 20;
        }

        private string ImpressiveGuy()
        {
            return Pick(Global.ImpressiveTitles) +
                   ((Global.RandomValue.Next(2) > 0)
                        ? " of the " + ((Item)Pick(Global.Races)).Name
                        : " of " + Global.GenerateName());
        }

        private string Indefinite(string s, int qty)
        {
            if (qty == 1)
            {
                if (s.Substring(0).IndexOfAny("AEIOU�aeiou�".ToCharArray()) > 0)
                    return "a " + s;
                else
                    return "an " + s;
            }
            else
            {
                return Formatting.Pluralize(qty, s);
            }
        }

        private string InterestingItem()
        {
            return Pick(Global.ItemAttrib) + " " + Pick(Global.Specials);
        }

        private void InterplotCinematic()
        {
            switch (Global.RandomValue.Next(3))
            {
                case 0:
                    player.Queue.Add(new Player.QueueItem("task", 1,
                                                          "Exhausted, you arrive at a friendly oasis in a hostile land"));
                    player.Queue.Add(new Player.QueueItem("task", 2, "You greet old friends and meet new allies"));
                    player.Queue.Add(new Player.QueueItem("task", 2, "You are privy to a council of powerful do-gooders"));
                    player.Queue.Add(new Player.QueueItem("task", 1, "There is much to be done. You are chosen!"));
                    break;
                case 1:
                    player.Queue.Add(new Player.QueueItem("task", 1,
                                                          "Your quarry is in sight, but a mighty enemy bars your path!"));
                    string nemesis = NamedMonster(int.Parse(lvTrait.Rows[(int)Global.Trait.Level][1], CultureInfo.CurrentCulture) + 3);
                    player.Queue.Add(new Player.QueueItem("task", 4, "A desperate struggle commences with " + nemesis));
                    int s = Global.RandomValue.Next(3);
                    for (int i = 1; i <= Global.RandomValue.Next(1 + lvPlot.Rows.Count); ++i)
                    {
                        s += 1 + Global.RandomValue.Next(2);
                        switch (s % 3)
                        {
                            case 0:
                                player.Queue.Add(new Player.QueueItem("task", 2, "Locked in grim combat with " + nemesis));
                                break;
                            case 1:
                                player.Queue.Add(new Player.QueueItem("task", 2,
                                                                      nemesis + " seems to have the upper hand"));
                                break;
                            case 2:
                                player.Queue.Add(new Player.QueueItem("task", 2,
                                                                      "You seem to gain the advantage over " + nemesis));
                                break;
                        }
                    }
                    player.Queue.Add(new Player.QueueItem("task", 3,
                                                          "Victory! " + nemesis +
                                                          " is slain! Exhausted, you lose conciousness"));
                    player.Queue.Add(new Player.QueueItem("task", 2,
                                                          "You awake in a friendly place, but the road awaits"));
                    break;
                case 2:
                    string nemesis2 = ImpressiveGuy();
                    player.Queue.Add(new Player.QueueItem("task", 2,
                                                          "Oh sweet relief! You've reached the protection of the good " +
                                                          nemesis2));
                    player.Queue.Add(new Player.QueueItem("task", 3,
                                                          "There is rejoicing, and an unnerving encouter with " +
                                                          nemesis2 + " in private"));
                    player.Queue.Add(new Player.QueueItem("task", 2,
                                                          "You forget your " + BoringItem() + " and go back to get it"));
                    player.Queue.Add(new Player.QueueItem("task", 2, "What's this!? You overhear something shocking!"));
                    player.Queue.Add(new Player.QueueItem("task", 2, "Could " + nemesis2 + " be a dirty double-dealer?"));
                    player.Queue.Add(new Player.QueueItem("task", 3,
                                                          "Who can possibly be trusted with this news!? ... Oh yes, of course"));
                    break;
            }

            player.Queue.Add(new Player.QueueItem("task", 1, "Loading"));
            lvQuests.BeginUpdate();
            lvQuests.Rows.Clear();
            CompleteQuest();
            lvQuests.EndUpdate();
            lvPlot.BeginUpdate();
            lvPlot.Rows[lvPlot.Rows.Count - 1][0] = "X";
            player.Data.PlotTotal = 0;
            player.Data.PlotMax = (60 * 60 * (1 + 5 * (lvPlot.Rows.Count - 1)));
            this._barPlot.BeginUpdate();
            this._barPlot.Value = 0;
            this._barPlot.EndUpdate();
            var row = new string[2];
            row[0] = " ";
            row[1] = "Act " + ToRoman(lvPlot.Rows.Count);
            lvPlot.Rows.Add(row);
            lvPlot.EndUpdate();
            if (lvPlot.Rows.Count > 2)
            {
                WinItem();
                WinEquip();
            }
        }

        private void LevelUp()
        {
            this.AddItem(lvTrait, "Level", 1);
            WinStat();
            WinStat();
            this.AddItem(lvStat, "HP Max", (int.Parse(lvStat.Rows[(int)Global.Stat.Con][1], CultureInfo.CurrentCulture) / 3) + 1 + Global.RandomValue.Next(4));
            this.AddItem(lvStat, "MP Max", (int.Parse(lvStat.Rows[(int)Global.Stat.Int][1], CultureInfo.CurrentCulture) / 3) + 1 + Global.RandomValue.Next(4));

            WinSpell();
            player.Data.ExperienceTotal = 0;
            player.Data.ExperienceMax = LevelUpTime(int.Parse(lvTrait.Rows[(int)Global.Trait.Level][1], CultureInfo.CurrentCulture));
            this._barExperience.Value = 0;
            // Brag('level');
        }

        private int LevelUpTime(int level)
        {
            // seconds
            // 20 minutes per level
            return 20 * level * 60;
        }

        private void buttonLeftSideUp(object sender)
        {
            Form form = null;
            switch (((Button)sender).InstanceName)
            {
                case "btnChar":
                    form = frmChar;
                    break;
                case "btnEquipment":
                    form = frmEquipment;
                    break;
                case "btnSpellbook":
                    form = frmSpellbook;
                    break;
            }

            if (form.Visible)
            {
                form.Hide();
            }
            else
            {
                frmChar.Hide();
                frmEquipment.Hide();
                frmSpellbook.Hide();
                form.Show(); 
            }
        }
        private void buttonRightSideUp(object sender)
        {
            Form form = null;
            switch (((Button)sender).InstanceName)
            {
                case "btnPlot":
                    form = frmPlot;
                    break;
                case "btnQuests":
                    form = frmQuests;
                    break;
                case "btnInventory":
                    form = frmInventory;
                    break;
            }

            if (form.Visible)
            {
                form.Hide();
            }
            else
            {
                frmPlot.Hide();
                frmQuests.Hide();
                frmInventory.Hide();
                form.Show();
            }
        }
        private void NextTutorial(Object sender)
        {
            switch (Global.TutorialCounter)
            {
                case 0:
                    lblTutorial.Text = "I'm here to give you a quick rundown of what all of these boxes and bars are. ";
                    break;
                case 1:
                    lblTutorial.Text = "If you accidentally close me or want to read me again, go to the Options menu and check the 'Show Tutorial' button. ";
                    break;
                case 2:
                    lblTutorial.Text = "LU! is based on Progress Quest which was released about 10 years ago. For more info on it, check out  http://www.progressquest.com. ";
                    break;
                case 3:
                     lblTutorial.Text = "Down there is your status bar. It has buttons to open up info tabs about what your player is doing. ";
                    break;
                case 4:
                    lblTutorial.Text = "Let me just open up your character sheet... Really, you named your character '" + lvTrait.Rows[(int)Global.Trait.Name][1] + "'? ";
                    buttonLeftSideUp(btnChar);
                    break;
                case 5:
                    lblTutorial.Text = "No matter. In this box, you can see your current statistics. Just your average character panel that you have seen a few hundred times by now. ";
                    break;
                case 6:
                    lblTutorial.Text = "Here is your current equipment. I see you have a " + lvEquipment.Rows[0][1] + ". That must have been a family heirloom. ";
                    buttonLeftSideUp(btnEquipment);
                    break;
                case 7:
                    lblTutorial.Text = "This would be your spellbook. Pretty tall column. You will be getting a lot of these. ";
                    buttonLeftSideUp(btnSpellbook);
                    break;
                case 8:
                    lblTutorial.Text = "Before we go on, did you notice how your character is just kind of doing his own thing? ";
                    frmSpellbook.Hide();
                    break;
                case 9:
                    lblTutorial.Text = "We felt that most MMO's have devolved into a cycle of kill-loot-repeat. It is so hard to keep up with    unemployed friends... ";
                    break;
                case 10:
                    lblTutorial.Text = "Or make it to class, or raise a family. So, we just decided to axe the middleman and just take care of  _all that muckity-muck for you. ";
                    break;
                case 11:
                    lblTutorial.Text = "You can still show off your virtual items on the leaderboard. If you want to check that out, just   press the Back button on your controller. ";
                    break;
                case 12:
                    lblTutorial.Text = "Where were we? Oh, that's right. This is the current overarching storyline that you are engaged in. ";
                    buttonRightSideUp(btnPlot);
                    break;
                case 13:
                    lblTutorial.Text = "And this is the current list of quests. Seriously, can't these NPC's walk across the road to do it?     What a timesink! ";
                    buttonRightSideUp(btnQuests);
                    break;
                case 14:
                    lblTutorial.Text = "This is your inventory. I never really understood how you can lug around 255 goblin torsos, but you   go to pick up a pebble and your sack is full. ";
                    buttonRightSideUp(btnInventory);
                    break;
                case 15:
                    lblTutorial.Text = "That is about all there is for now. So get out there and do your best. ";
                    frmInventory.Hide();
                    Global.Tutorial = false;
                    Global.SaveSettings();
                    break;
                case 16:
                    frmTutorial.Hide();
                    Global.TutorialCounter = 0;
                    break;
            }
            frmTutorial.TopMost = true;
            Global.TutorialCounter++;
        }

        private void LoadGame()
        {

            Viewport viewport = Global.Graphics.GraphicsDevice.Viewport;
            decimal ScaleX = (decimal)(viewport.TitleSafeArea.Width) / 1280;
            decimal ScaleY = (decimal)(viewport.TitleSafeArea.Height) / 720;

            int buttonSpace = (int)(10 * ScaleX);

            #region Main/Status Form

            Point buttonSize = new Point((int)(64 * ScaleX), (int)(64 * ScaleX));
            int statusHeight = (int)(116 * ScaleY);

            frmTutorial = new Form("frmTutorial", "", new Point(viewport.TitleSafeArea.Left + 30, viewport.TitleSafeArea.Top + 30), new Point(800, 100), FormBorderStyle.None);
            frmTutorial.DragAnywhere = true;
            if (Global.Tutorial)
                frmTutorial.Show();
            else
                frmTutorial.Hide();

            lblTutorial = new Label("lblTutorial", new Point(10, 0), new Point(frmTutorial.Width - 114, frmTutorial.Height - 20), "Welcome to Level Up!", Color.Wheat, HorizontalTextAlignment.Left, VerticalTextAlignment.Top);
            frmTutorial.Controls.Add(lblTutorial);

            pbTutorial = new PictureBox("pictureBack", new Point(frmTutorial.Width - 94, 0), "Images/nextButton", 64, 64, 1, false);
            frmTutorial.Controls.Add(pbTutorial);
            pbTutorial.CursorUp += NextTutorial;

            frmStatus = new Form(FrmStatusInstanceName, "", new Point(viewport.TitleSafeArea.Left, viewport.TitleSafeArea.Bottom - statusHeight),
                                 new Point(viewport.TitleSafeArea.Width, statusHeight), FormBorderStyle.None);
            frmStatus.DragAnywhere = false;
            frmStatus.ShowTitleBar = false;
            frmStatus.FontName = GsFontNameGamefont;
            frmStatus.Show();

            #region Left Buttons
            btnChar = new Button("btnChar", new Point(buttonSpace, buttonSpace), buttonSize, "Images/btnChar");
            btnChar.CursorUp += new OrbEventHandler(buttonLeftSideUp);
            frmStatus.Controls.Add(this.btnChar);
            btnEquipment = new Button("btnEquipment", new Point(btnChar.Position.X + buttonSize.X + buttonSpace, buttonSpace), buttonSize, "Images/btnEquipment");
            btnEquipment.CursorUp += new OrbEventHandler(buttonLeftSideUp); 
            frmStatus.Controls.Add(this.btnEquipment);
            btnSpellbook = new Button("btnSpellbook", new Point(btnEquipment.Position.X + buttonSize.X + buttonSpace, buttonSpace), buttonSize, "Images/btnSpellbook");
            btnSpellbook.CursorUp += new OrbEventHandler(buttonLeftSideUp);
            frmStatus.Controls.Add(this.btnSpellbook);
            #endregion
            
            #region Right Buttons
            btnPlot = new Button("btnPlot", new Point(viewport.TitleSafeArea.Width - buttonSize.X - buttonSpace, buttonSpace), buttonSize, "Images/btnPlot");
            btnPlot.CursorUp += new OrbEventHandler(buttonRightSideUp);
            frmStatus.Controls.Add(this.btnPlot);
            btnQuests = new Button("btnQuests", new Point(btnPlot.Position.X - buttonSize.X - buttonSpace, buttonSpace), buttonSize, "Images/btnQuests");
            btnQuests.CursorUp += new OrbEventHandler(buttonRightSideUp);
            frmStatus.Controls.Add(this.btnQuests);
            btnInventory = new Button("btnInventory", new Point(btnQuests.Position.X - buttonSize.X - buttonSpace, buttonSpace), buttonSize, "Images/btnInventory");
            btnInventory.CursorUp += new OrbEventHandler(buttonRightSideUp);
            frmStatus.Controls.Add(this.btnInventory);
            #endregion
            
            #region Stats Bar inbetweeen
            lblAction = new Label(LblActionInstanceName, new Point(btnSpellbook.Position.X + buttonSize.X + buttonSpace + 4, buttonSpace + 4),
                new Point(btnInventory.Position.X - (btnSpellbook.Position.X + buttonSize.X + ((buttonSpace) * 2)) + 4, 56), 
                LblActionText, Color.Black, HorizontalTextAlignment.Left, VerticalTextAlignment.Top);
            frmStatus.Controls.Add(lblAction);
            if (player.Queue.Count > 0)
                lblAction.Text = player.Queue[0].Text;
            this._barAction = new ProgressBar(BarActionInstanceName, new Point(btnSpellbook.Position.X + buttonSize.X + buttonSpace, buttonSpace),
                btnInventory.Position.X - (btnSpellbook.Position.X + buttonSize.X + (buttonSpace * 2)), 64 ,
                ProgressBarStyle.Continuous);
            this._barAction.Value = 0;
            frmStatus.Controls.Add(this._barAction);
            #endregion

            #endregion

            int columnwidth = 400;
            int innercolumnwidth = 380;
            
            #region Traits

            int traitFontHeight = (frmStatus.Font.Height() + OrbResources.Control_ListView_BorderWidth) *
                                  (Global.GetEnumCount<Global.Trait>() + 1) -
                                  (Global.GetEnumCount<Global.Trait>() - 1);
            int statFontHeight = (frmStatus.Font.Height() + OrbResources.Control_ListView_BorderWidth) *
                                 (Global.GetEnumCount<Global.Stat>() + 1) - (Global.GetEnumCount<Global.Stat>() - 1);

            lvTrait = new ListView(LVTraitInstanceName, new Point(buttonSpace, buttonSpace), new Point(innercolumnwidth, traitFontHeight));
            lvTrait.Columns.Add(new ListViewHeader("Trait", 130));
            lvTrait.Columns.Add(new ListViewHeader("Value", innercolumnwidth - 132));
            lvTrait.FontName = GsFontNameGamefont;
            lvTrait.HeaderStyle = ListViewHeaderStyle.Fixed;
            lvTrait.FullRowSelect = true;

            int traitCounter = 0;
            string[] row = null;
            foreach (string trait in Global.GetEnumNames<Global.Trait>())
            {
                row = new string[2];
                row[0] = trait;
                row[1] = player.Data.Traits[traitCounter];
                lvTrait.Rows.Add(row);
                traitCounter++;
            }
            lvTrait.HeaderStyle = ListViewHeaderStyle.Fixed;

            #endregion

            #region Stats

            lvStat = new ListView(LVStatInstanceName, new Point(buttonSpace, lvTrait.Y + traitFontHeight),
                                  new Point(innercolumnwidth, statFontHeight));
            lvStat.Columns.Add(new ListViewHeader("Stat", 130));
            lvStat.Columns.Add(new ListViewHeader("Value", innercolumnwidth - 132));
            lvStat.FontName = GsFontNameGamefont;
            lvStat.HeaderStyle = ListViewHeaderStyle.Fixed;
            lvStat.FullRowSelect = true;

            int statCounter = 0;
            foreach (string stat in Global.GetEnumNames<Global.Stat>())
            {
                row = new string[2];
                row[0] = stat;
                row[1] = player.Data.Stats[statCounter];
                lvStat.Rows.Add(row);
                statCounter++;
            }
            lvStat.HeaderStyle = ListViewHeaderStyle.Fixed;
            #endregion

            #region Experience
            lblExperience = new Label(LblExperienceInstanceName, new Point(buttonSpace + 2, lvStat.Y + statFontHeight + 2),
                                      new Point(innercolumnwidth - 4, frmStatus.Font.Height()), LblExperienceText, Color.Black,
                                      HorizontalTextAlignment.Left);
            lblExperience.FontName = GsFontNameGamefont;
            this._barExperience = new ProgressBar(BarExperienceInstanceName, new Point(buttonSpace, lvStat.Y + statFontHeight),
                                            innercolumnwidth, frmStatus.Font.Height() + 4, ProgressBarStyle.Continuous);
            this._barExperience.Value = player.Data.ExperiencePercentage;
            #endregion

            #region Character Form
            int formHeight = this._barExperience.Y + this._barExperience.Height + (buttonSpace * 3);
            frmChar = new Form(FrmCharInstanceName, "", new Point(viewport.TitleSafeArea.X, frmStatus.Y - formHeight), new Point(columnwidth, formHeight), FormBorderStyle.None);
            frmChar.DragAnywhere = false;
            frmChar.ShowTitleBar = false;
            frmChar.FontName = GsFontNameGamefont;
            frmChar.Font.LineSpacing = 0;
            frmChar.TitleTextColor = Color.White;
            
            frmChar.Controls.Add(lvTrait);
            frmChar.Controls.Add(lvStat);
            frmChar.Controls.Add(lblExperience); 
            frmChar.Controls.Add(this._barExperience);
            frmChar.Show();
            #endregion
            
            #region Equipment
            int equipFontHeight = (frmChar.Font.Height() + OrbResources.Control_ListView_BorderWidth) *
                                 (Global.Equips.Length + 1) - (Global.Equips.Length - 1);
            
            frmEquipment = new Form("frmEquipment", "", new Point(viewport.TitleSafeArea.X, frmStatus.Y - equipFontHeight - 40),
                new Point(columnwidth + 200, equipFontHeight + 40), FormBorderStyle.None);
            frmEquipment.DragAnywhere = false;
            frmEquipment.ShowTitleBar = false;
            frmEquipment.FontName = GsFontNameGamefont;
            frmEquipment.Font.LineSpacing = 0;
            frmEquipment.TitleTextColor = Color.White;

            lvEquipment = new ListView(LVEquipmentInstanceName, new Point(buttonSpace, buttonSpace),
                                       new Point(innercolumnwidth + 200, equipFontHeight));
            lvEquipment.Columns.Add(new ListViewHeader("Part", 180));
            lvEquipment.Columns.Add(new ListViewHeader("Name", innercolumnwidth + 18));
            lvEquipment.FontName = GsFontNameGamefont;
            lvEquipment.HeaderStyle = ListViewHeaderStyle.Fixed;
            lvEquipment.FullRowSelect = true;

            int equipCount = 0;

            foreach (string part in Global.Equips)
            {
                row = new string[2];
                row[0] = part;
                if (equipCount < player.Data.Equipment.Length)
                    row[1] = player.Data.Equipment[equipCount];
                else
                    row[1] = "";
                lvEquipment.Rows.Add(row);
                equipCount++;
            }
            frmEquipment.Controls.Add(lvEquipment);
            frmEquipment.Show();

            #endregion

            #region Spellbook
            frmSpellbook = new Form("frmSpellbook", "", new Point(viewport.TitleSafeArea.Left, viewport.TitleSafeArea.Top),
                new Point(columnwidth + 200, viewport.TitleSafeArea.Height - statusHeight), FormBorderStyle.None);
            frmSpellbook.DragAnywhere = false;
            frmSpellbook.ShowTitleBar = false;
            frmSpellbook.FontName = GsFontNameGamefont;
            frmSpellbook.Font.LineSpacing = 0;
            frmSpellbook.TitleTextColor = Color.White;

            lvSpellbook = new ListView(lvSpellbookInstanceName, new Point(buttonSpace, buttonSpace),
                                       new Point(innercolumnwidth + 200, frmSpellbook.Height - 40));
            lvSpellbook.Columns.Add(new ListViewHeader("Spell", innercolumnwidth));
            lvSpellbook.Columns.Add(new ListViewHeader("Level", 196));
            lvSpellbook.FontName = GsFontNameGamefont;
            lvSpellbook.HeaderStyle = ListViewHeaderStyle.Fixed;
            lvSpellbook.FullRowSelect = true;

            if (player.Data.Spells != null)
            {
                for (int spellCounter = 0; spellCounter < player.Data.Spells.Length; spellCounter++)
                {
                    row = new string[2];
                    row[0] = player.Data.Spells[spellCounter];
                    row[1] = player.Data.Spellsvalue[spellCounter];
                    lvSpellbook.Rows.Add(row);
                }
            }
            lvSpellbook.HeaderStyle = ListViewHeaderStyle.Fixed;

            frmSpellbook.Controls.Add(lvSpellbook);
            frmSpellbook.Show();

            #endregion

            #region Plot

            frmPlot = new Form("frmPlot", "", new Point(viewport.TitleSafeArea.Right - 600, viewport.TitleSafeArea.Top),
                new Point(600, viewport.TitleSafeArea.Height - frmStatus.Height), FormBorderStyle.None);
            frmPlot.DragAnywhere = false;
            frmPlot.ShowTitleBar = false;
            frmPlot.FontName = GsFontNameGamefont;
            frmPlot.Font.LineSpacing = 0;
            frmPlot.TitleTextColor = Color.White;
            
            lvPlot = new ListView(LVPlotInstanceName, new Point(10, 10), new Point(580, frmPlot.Height - 60));
            lvPlot.Columns.Add(new ListViewHeader("", 40));
            lvPlot.Columns.Add(new ListViewHeader("Plot", 538));
            lvPlot.FontName = GsFontNameGamefont;
            lvPlot.HeaderStyle = ListViewHeaderStyle.Fixed;
            lvPlot.FullRowSelect = true;

            row = new string[2];
            for (int plotCounter = 0; plotCounter < player.Data.Plot.Length; plotCounter++)
            {
                row = new string[2];
                if (plotCounter == (player.Data.Plot.Length - 1))
                    row[0] = " ";
                else
                    row[0] = "X";
                row[1] = player.Data.Plot[plotCounter];
                lvPlot.Rows.Add(row);
            }
            lvPlot.HeaderStyle = ListViewHeaderStyle.Fixed;
            frmPlot.Controls.Add(lvPlot);

            lblPlot = new Label("lblPlot", new Point(10, lvPlot.Y + lvPlot.Height + 2),
                                    new Point(555, 20), "Plotline Completed", Color.Black);
            frmPlot.Controls.Add(lblPlot);
            this._barPlot = new ProgressBar(BarPlotInstanceName, new Point(10, lvPlot.Y + lvPlot.Height), 555,
                                      20, ProgressBarStyle.Continuous);
            this._barPlot.Value = player.Data.PlotPercentage;
            frmPlot.Controls.Add(this._barPlot);
            frmPlot.Show();

            #endregion
            
            #region Quests
            frmQuests = new Form("frmQuests", "", new Point(viewport.TitleSafeArea.Right - 600, viewport.TitleSafeArea.Top),
                new Point(600, viewport.TitleSafeArea.Height - frmStatus.Height), FormBorderStyle.None);
            frmQuests.DragAnywhere = false;
            frmQuests.ShowTitleBar = false;
            frmQuests.FontName = GsFontNameGamefont;
            frmQuests.Font.LineSpacing = 0;
            frmQuests.TitleTextColor = Color.White;

            lvQuests = new ListView("lvQuests", new Point(10, 10), new Point(580, frmQuests.Height - 60));
            lvQuests.Columns.Add(new ListViewHeader("", 40));
            lvQuests.Columns.Add(new ListViewHeader("Quest", 538));
            lvQuests.FontName = GsFontNameGamefont;
            lvQuests.HeaderStyle = ListViewHeaderStyle.Fixed; 
            lvQuests.FullRowSelect = true;

            row = new string[2];
            if (player.Data.Quests != null)
            {
                for (int questCounter = 0; questCounter < player.Data.Quests.Length; questCounter++)
                {
                    row = new string[2];
                    if (questCounter == (player.Data.Quests.Length - 1))
                        row[0] = " ";
                    else
                        row[0] = "X";
                    row[1] = player.Data.Quests[questCounter];
                    lvQuests.Rows.Add(row);
                }
            }
            frmQuests.Controls.Add(lvQuests);

            lblQuests = new Label("lblQuests", new Point(10, lvQuests.Y + lvQuests.Height + 2),
                                    new Point(555, 20), "Quests Completed", Color.Black);
            frmQuests.Controls.Add(lblQuests);
            this._barQuests = new ProgressBar(BarPlotInstanceName, new Point(10, lvQuests.Y + lvQuests.Height), 555,
                                      20, ProgressBarStyle.Continuous);
            this._barQuests.Value = player.Data.QuestsPercentage;
            frmQuests.Controls.Add(this._barQuests);
            frmQuests.Show();

            #endregion

            #region Inventory
            frmInventory = new Form("frmInventory", "", new Point(viewport.TitleSafeArea.Right - 600, viewport.TitleSafeArea.Top),
                new Point(600, viewport.TitleSafeArea.Height - frmStatus.Height), FormBorderStyle.None);
            frmInventory.DragAnywhere = false;
            frmInventory.ShowTitleBar = false;
            frmInventory.FontName = GsFontNameGamefont;
            frmInventory.Font.LineSpacing = 0;
            frmInventory.TitleTextColor = Color.White;

            lvInventory = new ListView("lvInventory", new Point(10, 10), new Point(580, frmInventory.Height - 60));
            lvInventory.Columns.Add(new ListViewHeader("Item", 453));
            lvInventory.Columns.Add(new ListViewHeader("Qty", 125));
            lvInventory.FontName = GsFontNameGamefont;
            lvInventory.HeaderStyle = ListViewHeaderStyle.Fixed;
            lvInventory.FullRowSelect = true;

            for (int invCounter = 0; invCounter < player.Data.Items.Length; invCounter++)
            {
                row = new string[2];
                row[0] = player.Data.Items[invCounter];
                row[1] = player.Data.Itemsvalue[invCounter];
                lvInventory.Rows.Add(row);
            }
            frmInventory.Controls.Add(lvInventory);

            lblEncumbrance = new Label("lblEncumbrance", new Point(10, lvInventory.Y + lvInventory.Height + 2),
                        new Point(555, 20), "Encumbrance", Color.Black);
            frmInventory.Controls.Add(lblEncumbrance);
            this._barEncumbrance = new ProgressBar(BarEncumbranceInstanceName, new Point(10, lvInventory.Y + lvInventory.Height), 555,
                                      20, ProgressBarStyle.Continuous);
            float encValue = ((float)player.Data.Itemcount / (int.Parse(lvStat.Rows[(int)Global.Stat.Str][1], CultureInfo.CurrentCulture) + 10)) *
                                100;
            if (encValue > 100)
            {
                encValue = 100;
            }
            this._barEncumbrance.Value = (int)encValue; 
            
            frmInventory.Controls.Add(this._barEncumbrance);
            frmInventory.Show();
            #endregion
            
            
        }

//  COMMENTED BY CODEIT.RIGHT
//        private object LPick(object[] list, int goal)
//        {
//            var result = (string)Pick(list);
//            for (int i = 1; i <= 5; ++i)
//            {
//                int best = int.Parse(result.Split('|')[1]);
//                var s = (string)Pick(list);
//                int b1 = int.Parse(s.Split('|')[1]);
//                if (Math.Abs(goal - best) > Math.Abs(goal - b1))
//                    result = s;
//            }
//            return result;
//        }

        private Item PickLowItem(Item[] list, int goal)
        {
            var result = (Item)Pick(list);
            for (int i = 1; i <= 5; ++i)
            {
                int best = result.Value;
                var s = (Item)Pick(list);
                int b1 = s.Value;
                if (Math.Abs(goal - best) > Math.Abs(goal - b1))
                    result = s;
            }
            return result;
        }

        private Item MonsterTask(int level)
        {
            bool definite = false;
            var result = new Item();
            var monster = new Item();

            for (int counter = level; counter >= 1; --counter)
            {
                if (Odds(2, 5))
                {
                    level += RandSign();
                }

                if (level < 1)
                {
                    level = 1;
                }

                // level = level of puissance of opponent(s) we'll return

                if (Odds(1, 25))
                {
                    // Use an NPC every once in a while
                    result = new Item(((Item)Pick(Global.Races)).Name, level);
                    if (Odds(1, 2))
                    {
                        result.Extra = new[] { "*", ((Item)Pick(Global.Classes)).Name };
                        result.Name = "passing " + result.Extra[1];
                    }
                    else
                    {
                        result.Name = (string)PickLow(Global.Titles) + ' ' + Global.GenerateName() + " the " + result.Name;
                        result.Extra = new[] { "*", "Unique" };
                        definite = true;
                    }
                }
                else if ((player.Questmonster != null) && Odds(1, 4))
                {
                    // Use the quest monster
                    monster = Global.Monsters[player.Questmonsterindex];
                    result = new Item(monster.Name, monster.Value, new[] { monster.Extra[0], monster.Name });
                }
                else
                {
                    // Pick the monster out of so many random ones closest to the level we want
                    monster = (Item)Pick(Global.Monsters);

                    for (int ii = 0; ii < 5; ++ii)
                    {
                        var m1 = (Item)Pick(Global.Monsters);
                        if (Math.Abs(level - m1.Value) < Math.Abs(level - monster.Value))
                        {
                            monster = m1;
                        }
                    }
                    result = new Item(monster.Name, monster.Value, new[] { monster.Extra[0], monster.Name });
                }
            }

            int qty = 1;
            if ((level - monster.Value) > 10)
            {
                // lev is too low. multiply...
                qty = (int)Math.Floor((double)(level + Global.RandomValue.Next(monster.Value)) / Math.Max(monster.Value, 1));
                if (qty < 1)
                {
                    qty = 1;
                }

                level = (int)Math.Floor((double)level / qty);
            }
            int i = 0;
            if ((level - monster.Value) <= -10)
            {
                result.Name = "imaginary " + result.Name;
            }
            else if ((level - monster.Value) < -5)
            {
                i = 10 + (level - monster.Value);
                i = 5 - Global.RandomValue.Next(i + 1);
                result.Name = this.Sick(i, this.Young((monster.Value - level) - i, result.Name));
            }
            else if (((level - monster.Value) < 0) && (Global.RandomValue.Next(2) == 1))
            {
                result.Name = this.Sick(level - monster.Value, result.Name);
            }
            else if (((level - monster.Value) < 0))
            {
                result.Name = this.Young(level - monster.Value, result.Name);
            }
            else if ((level - monster.Value) >= 10)
            {
                result.Name = "messianic " + result.Name;
            }
            else if ((level - monster.Value) > 5)
            {
                i = 10 - (level - monster.Value);
                i = 5 - Global.RandomValue.Next(i + 1);
                result.Name = this.Big(i, Special((level - monster.Value) - i, result.Name));
            }
            else if (((level - monster.Value) > 0) && (Global.RandomValue.Next(2) == 1))
            {
                result.Name = this.Big(level - monster.Value, result.Name);
            }
            else if (((level - monster.Value) > 0))
            {
                result.Name = this.Special(level - monster.Value, result.Name);
            }

            level = monster.Value * qty;

            if (!definite)
            {
                result.Name = Indefinite(result.Name, qty);
            }


            if (qty > 1)
            {
                result.Name = qty + " " + result.Name;
            }

            if (result.Extra[0].Equals("", StringComparison.CurrentCulture))
                result.Extra[0] = "*";
            if (!result.Extra[0].Equals("*", StringComparison.CurrentCulture))
                result.Extra[0] = monster.Name + " " + monster.Extra[0];
            return result;
        }

        private string NamedMonster(int level)
        {
            int lev = 0;
            Item m = null;
            string result = "";
            for (int i = 0; i < 5; ++i)
            {
                m = ((Item)Pick(Global.Monsters));
                if (result.Equals("", StringComparison.CurrentCulture) || (Math.Abs(level - m.Value)) < Math.Abs(level - lev))
                {
                    result = m.Name;
                    lev = m.Value;
                }
            }
            return Global.GenerateName() + " the " + m.Name;
        }

        private bool Odds(int chance, int outof)
        {
            return (Global.RandomValue.Next(outof) < chance);
        }

        private object Pick(object[] array)
        {
            return array[Global.RandomValue.Next(0, array.Length)];
        }

        private object PickLow(object[] array)
        {
            return array[this.RandomLow(array.Length)];
        }

        private string Prefix(object[] a, int m, string s, string sep)
        {
            if (sep == "") sep = " ";
            m = Math.Abs(m);
            if (m < 1 || m > a.Length) return s; // In case of screwups
            return (string)a[m - 1] + sep + s;
        }

        private int RandomLow(int below)
        {
            var range = new[] { Global.RandomValue.Next(below), Global.RandomValue.Next(below) };
            return range.Min();
        }

        private int RandSign()
        {
            return Global.RandomValue.Next(2) * 2 - 1;
        }

        private void SaveDeviceSaveCompleted(object sender, FileActionCompletedEventArgs args)
        {
            // just write some debug output for our verification
            Debug.WriteLine("SaveCompleted!");
        }

        private string Sick(int m, string s)
        {
            m = 6 - Math.Abs(m);
            return Prefix(new object[] { "dead", "comatose", "crippled", "sick", "undernourished" }, m, s, " ");
        }

        private string Special(int m, string s)
        {
            if (s.IndexOf(' ') > 0)
                return Prefix(new object[] { "veteran", "cursed", "warrior", "undead", "demon" }, m, s, " ");
            else
                return Prefix(new object[] { "Battle-", "cursed ", "Were-", "undead ", "demon " }, m, s, " ");
        }

        private int ToArabic(string roman)
        {
            return int.Parse(RomanNumeralConverter.Convert(roman));
        }

        private string ToRoman(int arabic)
        {
            return RomanNumeralConverter.Convert(arabic.ToString());
        }

        /// <summary> 
        /// Copy a section of the given texture into a new texture. 
        /// </summary> 
        /// <param name="texture">Spritesheet texture.</param> 
        /// <param name="area">Area to be copied.</param> 
        /// <returns>New texture containing the target area.</returns> 
        public Texture2D CopyFrom(Texture2D texture, Rectangle area)
        {
            // buffer to contain raw texture data 
            Color[] data = new Color[area.Width * area.Height];

            // pull the target section from the texture provided 
            texture.GetData<Color>(0, area, data, 0, data.Length);

            // make a new texture of the appropriate size 
            Texture2D copy = new Texture2D(texture.GraphicsDevice, area.Width, area.Height);

            copy.SetData<Color>(data);
            return copy;
        } 

        private void WinEquip()
        {
            int posn = Global.RandomValue.Next(lvEquipment.Rows.Count);
            Item[] stuff;
            string[] better;
            string[] worse;

            if (posn == 0)
            {
                stuff = Global.Weapons;
                better = Global.OffenseAttrib;
                worse = Global.OffenseBad;
            }
            else
            {
                stuff = (posn == 1) ? Global.Shields : Global.Armors;
                better = Global.DefenseAttrib;
                worse = Global.DefenseBad;
            }
            Item item = PickLowItem(stuff, int.Parse(lvTrait.Rows[(int)Global.Trait.Level][1], CultureInfo.CurrentCulture));
            int plus = int.Parse(lvTrait.Rows[(int)Global.Trait.Level][1], CultureInfo.CurrentCulture) - item.Value;
            if (plus < 0)
            {
                better = worse;
            }

            int count = 0;
            string returnname = item.Name;
            while ((count < 2) && (plus > 0))
            {
                var modifier = (string)Pick(better);
                item.Value = int.Parse(modifier.Split('|')[1], CultureInfo.CurrentCulture);
                modifier = modifier.Split('|')[0];
                if (returnname.IndexOf(modifier, StringComparison.CurrentCulture) > 0)
                {
                    break;
                }
                // no repeats
                if (Math.Abs(plus) < Math.Abs(item.Value))
                {
                    break;
                }
                // too much
                returnname = modifier + ' ' + returnname;
                plus -= item.Value;
                ++count;
            }

            if (plus < 0)
            {
                returnname = plus + " " + returnname;
            }

            if (plus > 0)
            {
                returnname = "+" + plus + " " + returnname;
            }

            switch ((lvEquipment.Rows[posn][0]).ToLower())
            {
                case "weapon":
                    equipObject.Sprite.Texture = CopyFrom(weaponSheet, weaponTexture[item.Name.ToLower(CultureInfo.CurrentCulture)]);
                    break;
                case "shield":
                    equipObject.Sprite.Texture = CopyFrom(shieldSheet, shieldTexture[item.Name.ToLower(CultureInfo.CurrentCulture)]);
                    break;
                case "helm":
                    equipObject.Sprite.Texture = CopyFrom(helmSheet, helmTexture[item.Name.ToLower(CultureInfo.CurrentCulture)]);
                    break;
                case "hauberk":
                    equipObject.Sprite.Texture = CopyFrom(hauberkSheet, hauberkTexture[item.Name.ToLower(CultureInfo.CurrentCulture)]);
                    break;
                case "brassairts":
                    equipObject.Sprite.Texture = CopyFrom(brassairtsSheet, brassairtsTexture[item.Name.ToLower(CultureInfo.CurrentCulture)]);
                    break;
                case "vambrace":
                    equipObject.Sprite.Texture = CopyFrom(vambraceSheet, vambraceTexture[item.Name.ToLower(CultureInfo.CurrentCulture)]);
                    break;
                case "gauntlets":
                    equipObject.Sprite.Texture = CopyFrom(gauntletsSheet, gauntletsTexture[item.Name.ToLower(CultureInfo.CurrentCulture)]);
                    break;
                case "gambeson":
                    equipObject.Sprite.Texture = CopyFrom(gambesonSheet, gambesonTexture[item.Name.ToLower(CultureInfo.CurrentCulture)]);
                    break;
                case "cuisses":
                    equipObject.Sprite.Texture = CopyFrom(cuissesSheet, cuissesTexture[item.Name.ToLower(CultureInfo.CurrentCulture)]);
                    break;
                case "greaves":
                    equipObject.Sprite.Texture = CopyFrom(greavesSheet, greavesTexture[item.Name.ToLower(CultureInfo.CurrentCulture)]);
                    break;
                case "sollerets":
                    equipObject.Sprite.Texture = CopyFrom(solleretSheet, solleretTexture[item.Name.ToLower(CultureInfo.CurrentCulture)]);
                    break;
            }

            equipObject.Position = new Vector2(Global.Graphics.PreferredBackBufferWidth / 2,
                                               Global.Graphics.PreferredBackBufferHeight / 2);
            equipObject.Velocity = new Vector2(-256.0f, 64.0f);
            equipObject.Alive = true;

            this.AddEquip(lvEquipment, posn, returnname);
        }

        private void WinItem()
        {
            this.AddItem(lvInventory, InterestingItem() + " of " + Pick(Global.ItemOfs), 1);
        }

        private void WinSpell()
        {
            this.AddRoman(lvSpellbook,
                     Global.Spells[RandomLow(Math.Min(int.Parse(lvStat.Rows[(int)Global.Stat.Wis][1], CultureInfo.CurrentCulture) + int.Parse(lvTrait.Rows[(int)Global.Trait.Level][1], CultureInfo.CurrentCulture), Global.Spells.Length))], 1);
        }

        private void WinStat()
        {
            string i;
            if (Odds(1, 2))
            {
                i = ((string)Pick(Global.GetEnumNames<Global.Stat>()));
            }
            else
            {
                // Favor the best stat so it will tend to clump
                int maxValue = 0;
                int maxRow = 0;
                for (int statCounter = 0; statCounter < lvStat.Rows.Count; statCounter++)
                {
                    if (int.Parse(lvStat.Rows[statCounter][1], CultureInfo.CurrentCulture) > maxValue)
                    {
                        maxRow = statCounter;
                        maxValue = int.Parse(lvStat.Rows[statCounter][1], CultureInfo.CurrentCulture);
                    }
                }
                i = lvStat.Rows[maxRow][0];
            }
            this.AddItem(lvStat, i, 1);
        }

        private string Young(int m, string s)
        {
            m = 6 - Math.Abs(m);
            return Prefix(new object[] { "tiny", "little", "adolescent", "teenage", "underage" }, m, s, " ");
        }

        #endregion Methods
    }
}