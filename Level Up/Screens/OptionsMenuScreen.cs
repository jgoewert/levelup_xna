#region Header
// --------------------------------
// <copyright file="MainMenuScreen.cs" company="John Goewert">
//     Copyright John Goewert 2011.
// </copyright>
// <author>John Goewert</author>
// ---------------------------------
#endregion


using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using OrbUI.Components;
using OrbUI.Components.GUI;
using OrbUI.Components.GUI.Controls;
using OrbUI.Components.Input.Controller;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Media;

namespace GameStateManagement
{
    /// <summary>
    /// The main menu screen is the first thing displayed when the game starts up.
    /// </summary>
    internal class OptionsMenuScreen : MenuScreen

    {
        #region Fields
        private float pauseAlpha;

        private Texture2D backgroundTexture;
        private ContentManager content;
        private Form frmOptionsMenu;
//        private Label lblSoundVolume;
//        private Slider sliderSoundVolume;
        private Label lblMusicVolume;
        private Slider sliderMusicVolume;
        
        private Label lblMute;
        private CheckBox chkMute;

        private Label lblTutorial;
        private CheckBox chkTutorial;
        //        private Label lblMasterVolume;
//        private Slider sliderMasterVolume;
        //private Label lblMenu;
        //private Slider sliderMenu;
        private Button btnOptionsExit;
        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the MainMenuScreen class.
        /// </summary>
        public OptionsMenuScreen()
            : base("Options")
        {
            if (frmOptionsMenu.IsNullOrDisposed())
            {
                InitOptionsMenu();
            }
            btnOptionsExit.CursorUp += OptionsExitSelected;
        }

        #endregion Constructors

        #region Methods
        protected override void OnSelectEntry(int entryIndex, PlayerIndex playerIndex)
        {
        }

        private void InitOptionsMenu()
        {
            frmOptionsMenu = new Form("frmOptionsMenu", "", new Point(440, 270), new Point(400, 260), FormBorderStyle.None);
            frmOptionsMenu.DragAnywhere = false;
            frmOptionsMenu.ShowTitleBar = false;
            frmOptionsMenu.FontName = "gamefont";
            frmOptionsMenu.Font.LineSpacing = 0;
            frmOptionsMenu.TitleTextColor = Color.White;
            frmOptionsMenu.Show();

//            lblMute = new Label("lblMute", new Point(5, 20), new Point(380, 20), "Mute",
//                                       Color.Yellow);
//            frmOptionsMenu.Controls.Add(lblMute);


            lblMusicVolume = new Label("lblMusicVolume", new Point(5, 20), new Point(380, 20), "Music Volume - " + ((int)(MediaPlayer.Volume * 100.0f)).ToString("D") + "%",
                                       Color.White);
            frmOptionsMenu.Controls.Add(lblMusicVolume);
            sliderMusicVolume = new Slider("sliderMusicVolume", new Point(5, 50), 380);
            sliderMusicVolume.Max = 1.0f;
            sliderMusicVolume.Value = MediaPlayer.Volume;
            sliderMusicVolume.OnValueChanged += delegate(object sender, float value)
            {
                lblMusicVolume.Text = "Music Volume - " + ((int)(sliderMusicVolume.Value * 100.0f)).ToString("D") + "%";
                MediaPlayer.Volume = sliderMusicVolume.Value;
            };
            frmOptionsMenu.Controls.Add(sliderMusicVolume);

            chkMute = new CheckBox("chkMute", new Point(5, 70), new Point(200, 30), " Mute", MediaPlayer.IsMuted);
            chkMute.CheckedChanged += delegate(object sender, bool value)
            {
                MediaPlayer.IsMuted = value;
                Global.music.SetRandomSong();
            };
            frmOptionsMenu.Controls.Add(chkMute);

            chkTutorial = new CheckBox("chkTutorial", new Point(5, 120), new Point(300, 30), " Show Tutorial", Global.Tutorial);
            chkTutorial.CheckedChanged += delegate(object sender, bool value)
            {
                Global.Tutorial = value;
            };
            frmOptionsMenu.Controls.Add(chkTutorial);


            /*lblMenu = new Label("lblMenu", new Point(5, 110), new Point(380, 20), "Menu Style",
                                       Color.Yellow);
            frmOptionsMenu.Controls.Add(lblMenu);
            sliderMenu = new Slider("sliderMenu", new Point(5, 150), 380);
            sliderMenu.Max = 5;
            sliderMenu.Value = Global.MenuStyle;
            sliderMenu.OnValueChanged += delegate(object sender, float value)
            {
                sliderMenu.BeginUpdate();
                sliderMenu.Value = (int)sliderMenu.Value;
                sliderMenu.EndUpdate();

            };
            frmOptionsMenu.Controls.Add(sliderMenu);
            */
            btnOptionsExit = new Button("btnOptionsExit", new Point(100, 190), new Point(240, 40), "Close", Color.Black);
            frmOptionsMenu.Controls.Add(btnOptionsExit);
        }


        /// <summary>
        /// Draws the gameplay screen.
        /// </summary>
        public override void Draw(GameTime gameTime)
        {
            // Darken down any other screens that were drawn beneath the popup.
            ScreenManager.FadeBackBufferToBlack(TransitionAlpha * 2 / 3);
            
            ScreenManager.SpriteBatch.Begin();
            if (frmOptionsMenu.IsNotNullOrDisposed())
                frmOptionsMenu.Draw(ScreenManager.GraphicsDevice, ScreenManager.SpriteBatch);
            
            OrbFormCollection.Cursor.Draw(ScreenManager.GraphicsDevice, ScreenManager.SpriteBatch);

            ScreenManager.SpriteBatch.End();
        }

        public override void OnCancel(PlayerIndex playerIndex)
        {
            this.ExitScreen();
        }

        public void OptionsExitSelected(object sender)
        {
            Global.SaveSettings();
            this.ExitScreen();
        }

        public override void UnloadContent()
        {
            base.UnloadContent();
            if (frmOptionsMenu.IsNotNullOrDisposed())
                OrbFormCollection.Remove(frmOptionsMenu);
        }

        #endregion Methods
    }
}