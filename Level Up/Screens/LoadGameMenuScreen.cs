﻿#region Header
// --------------------------------
// <copyright file="LoadGameMenuScreen.cs" company="John Goewert">
//     Copyright John Goewert 2011.
// </copyright>
// <author>John Goewert</author>
// ---------------------------------
#endregion



using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Xml.Serialization;
using EasyStorage;
using Level_Up;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
#if (WINDOWS && DEBUG)
using Microsoft.Xna.Framework.GamerServices;
#elif XBOX360
using Microsoft.Xna.Framework.GamerServices;
#endif
using OrbUI.Components;
using OrbUI.Components.GUI;
using OrbUI.Components.GUI.Controls;

namespace GameStateManagement
{
    /// <summary>
    /// The options screen is brought up over the top of the main menu
    /// screen, and gives the user a chance to configure the game
    /// in various hopefully useful ways.
    /// </summary>
    internal class LoadGameMenuScreen : GameScreen
    {
        #region Fields

        private List<SaveGameData> characters = new List<SaveGameData>();
        private int index = 0;

        //private int offset;
        private float pauseAlpha;
        //private string CharToDelete;

        public static Form frmLoadChar;
        public static PictureBox pictureBack;
        public static ListView lvCharacter;
        public static PictureBox pictureForward;
        public static Button btnLoad;
        public static Button btnDelete;
        public static Button btnExit;

        #endregion Fields

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the LoadGameMenuScreen class.
        /// </summary>
        public LoadGameMenuScreen()
        {
            TransitionOnTime = TimeSpan.FromSeconds(1.5);
            TransitionOffTime = TimeSpan.FromSeconds(0.5);
            LoadCharacters();

            InitLoadGameMenu();

        }

        #endregion Constructors

        #region Methods
        
        public override void LoadContent()
        {
            base.LoadContent();

        }
        
        public void InitLoadGameMenu()
        {
            Viewport viewport = Global.Graphics.GraphicsDevice.Viewport;

            frmLoadChar = new Form("frmLoadChar", "", new Point((viewport.Width - 602) / 2, (viewport.Height - 200) / 2), new Point(602, 200), FormBorderStyle.Fixed);
            frmLoadChar.DragAnywhere = false;
            frmLoadChar.ShowTitleBar = false;
            frmLoadChar.FontName = "gamefont";
            frmLoadChar.Font.LineSpacing = 0;
            frmLoadChar.TitleTextColor = Color.White;
            frmLoadChar.Show();

            // needs a back screen button - grey out when not possible
            pictureBack = new PictureBox("pictureBack", new Point(20, 20), "Images/backButton", 64, 64, 1, false);
            frmLoadChar.Controls.Add(pictureBack);
            pictureBack.CursorUp += PreviousCharacter;

            lvCharacter = new ListView("lvCharacter",
                                           new Point(126, 20),
                                           new Point(360, 86));
            lvCharacter.Columns.Add(new ListViewHeader("Trait", 130));
            lvCharacter.Columns.Add(new ListViewHeader("Value", 228));
            lvCharacter.HeaderStyle = ListViewHeaderStyle.None;
            frmLoadChar.Controls.Add(lvCharacter);

            pictureForward = new PictureBox("pictureForward", new Point(496, 20), "Images/nextButton", 64, 64, 1, false);
            frmLoadChar.Controls.Add(pictureForward);
            pictureForward.CursorUp += NextCharacter;

            btnLoad = new Button("btnLoad", new Point(126, 126), 96, "Load", Color.Black);
            frmLoadChar.Controls.Add(btnLoad);
            btnLoad.CursorUp += SelectCharacter;

            btnDelete = new Button("btnDelete", new Point(232,126), 96, "Delete", Color.Red);
            frmLoadChar.Controls.Add(btnDelete);
            btnDelete.CursorUp += DeleteCharacter;

            btnExit = new Button("btnExit", new Point(338, 126), 156, "Main Menu", Color.Black);
            frmLoadChar.Controls.Add(btnExit);
            btnExit.CursorUp += ReturnToMainMenu;

            setCharacter();

        }

        /// <summary>
        /// Update the LoadGameMenu screen.
        /// </summary>
        public override void Update(GameTime gameTime, bool otherScreenHasFocus, bool coveredByOtherScreen)
        {
            // Gradually fade in or out depending on whether we are covered by the pause screen.
            if (coveredByOtherScreen)
            {
                pauseAlpha = Math.Min(pauseAlpha + 1f/32, 1);
                frmLoadChar.Enabled = false;
            }
            else
            {
                pauseAlpha = Math.Max(pauseAlpha - 1f / 32, 0);
            }

            if (pauseAlpha == 0)
            {
                frmLoadChar.Enabled = true;
            }

            base.Update(gameTime, otherScreenHasFocus, coveredByOtherScreen);
        }

        /// <summary>
        /// Draws the LoadGameMenu screen .
        /// </summary>
        public override void Draw(GameTime gameTime)
        {
            
            // Our player and enemy are both actually just text strings.
            SpriteBatch spriteBatch = ScreenManager.SpriteBatch;

            spriteBatch.Begin();
            OrbFormCollection.Draw(ScreenManager.GraphicsDevice, spriteBatch);
            spriteBatch.End();

            // If the game is transitioning on or off, fade it out to black.
            if (TransitionPosition > 0 || pauseAlpha > 0)
            {
                float alpha = MathHelper.Lerp(1f - TransitionAlpha, 1f, pauseAlpha / 2);

                ScreenManager.FadeBackBufferToBlack(alpha);
            }
        }

        /// <summary>
        /// Lets the game respond to player input. Unlike the Update method,
        /// this will only be called when the gameplay screen is active.
        /// </summary>
        public override void HandleInput(InputState input)
        {
            if (input == null)
                throw new ArgumentNullException("input");

            // Look up inputs for the active player profile.
            var playerIndex = (int)ControllingPlayer.Value;

            KeyboardState keyboardState = input.CurrentKeyboardStates[playerIndex];
            GamePadState gamePadState = input.CurrentGamePadStates[playerIndex];

            // The game pauses either if the user presses the pause button, or if
            // they unplug the active gamepad. This requires us to keep track of
            // whether a gamepad was ever plugged in, because we don't want to pause
            // on PC if they are playing with a keyboard and have no gamepad at all!
            bool gamePadDisconnected = !gamePadState.IsConnected &&
                                       input.GamePadWasConnected[playerIndex];

            // Mouse.SetPosition(btnRoll.X + frmNewChar.X + btnRoll.Width / 2 , btnRoll.Y + frmNewChar.Y + btnRoll.Height / 2);
            if (input.IsPauseGame(ControllingPlayer) || gamePadDisconnected)
            {
                ScreenManager.AddScreen(new PauseMenuScreen(), ControllingPlayer);
            }
        }

        public void LoadCharacters()
        {
            try
            {
                if (Global.SaveDevice.GetFiles("LevelUp").Length > 0)
                {
                    foreach (string file in Global.SaveDevice.GetFiles("LevelUp"))
                    {
                        Global.SaveDevice.LoadCompleted += SaveDeviceLoadCompleted;

                        if (Global.SaveDevice != null &&
                            Global.SaveDevice.FileExists("LevelUp", file) && file.Contains(".sav"))
                        {
                            // make sure the device is ready
                            if (Global.SaveDevice.IsReady)
                            {
                                Global.SaveDevice.Load(
                                    "LevelUp",
                                    file,
                                    stream =>
                                    {
                                        try
                                        {
                                            var serializer = new XmlSerializer(typeof(SaveGameData));
                                            SaveGameData data = null;
                                            data = new SaveGameData();
                                            data = (SaveGameData)serializer.Deserialize(stream);
                                            if (data.Gamertag == null)
                                            {
                                                throw new ContentLoadException("Save File is old version, pitch.");
                                            }
#if DEBUG || XBOX
                                            if (
                                                    data.Gamertag.Equals(
                                                        Gamer.SignedInGamers[Global.CurrentPlayer].Gamertag))
                                            {
#endif 
                                                  characters.Add(data);
                                            }
#if DEBUG || XBOX
                                        }
#endif
                                        catch (ContentLoadException e)
                                        {
                                            Debug.WriteLine(e.Message);
                                        }
                                    });
                            }
                        }
                    }
                }
                else
                {
                    // MenuEntries.Add(new MenuEntry("No Characters to Load"));
                }
            }
            catch (InvalidOperationException e)
            {
                Debug.WriteLine(e.Message);
            }

        }

        /// <summary>
        /// Unload graphics content used by the game.
        /// </summary>
        public override void UnloadContent()
        {
            OrbFormCollection.Remove(frmLoadChar);
            base.UnloadContent();
        }
        private void NextCharacter(object sender)
        {
            index++;
            if (index > (characters.Count - 1))
            {
                index = 0;
            }
            setCharacter();
        }

        private void PreviousCharacter(object sender)
        {
            index--;
            if (index < 0)
            {
                index = characters.Count - 1;
            }
            setCharacter();
        }

        private void setCharacter()
        {
            lvCharacter.Rows.Clear();
            if (characters.Count == 0)
            {
                index = 0;
                btnLoad.Enabled = false;
                btnDelete.Enabled = false;
                pictureForward.Enabled = false;
                pictureBack.Enabled = false;

            }
            else if (characters.Count > index)
            {
                btnLoad.Enabled = true;
                btnDelete.Enabled = true;
                pictureForward.Enabled = true;
                pictureBack.Enabled = true;
                lvCharacter.Rows.Add(new string[2]{"Name", characters[index].Traits[(int)Global.Trait.Name]});
                lvCharacter.Rows.Add(new string[2]{"Level", characters[index].Traits[(int)Global.Trait.Level]});
                lvCharacter.Rows.Add(new string[2]{"Class", characters[index].Traits[(int)Global.Trait.Class]});
                lvCharacter.Rows.Add(new string[2]{"Race", characters[index].Traits[(int)Global.Trait.Race]});
            }
        }

        private void SaveDeviceLoadCompleted(object sender, FileActionCompletedEventArgs args)
        {
        }

        private void SelectCharacter(object sender)
        {

            Player player = Player.Instance;
            player.Data = characters[index];

            LoadingScreen.Load(ScreenManager, true, Global.CurrentPlayer, new GameplayScreen());
            this.ExitScreen();
        }

        /// <summary>
        /// When the user cancels the main menu, ask if they want to exit the sample.
        /// </summary>
        public void DeleteCharacter(object sender)
        {
            frmLoadChar.Enabled = false;

            const string message = "Are you sure you want to delete this character?";

            var confirmExitMessageBox = new MessageBoxScreen(message);

            confirmExitMessageBox.Accepted += DeleteConfirmedCharacter;
            confirmExitMessageBox.Cancelled += ReEnableForm;
            ScreenManager.AddScreen(confirmExitMessageBox, Global.CurrentPlayer);
        }

        /// <summary>
        /// When the user cancels the main menu, ask if they want to exit the sample.
        /// </summary>
        public void ReturnToMainMenu(object sender)
        {
            LoadingScreen.Load(ScreenManager, false, Global.CurrentPlayer, new BackgroundScreen(), new MainMenuScreen());
        }

        public void ReEnableForm(object sender, PlayerIndexEventArgs e)
        {
            frmLoadChar.Enabled = true;
        }

        public void DeleteConfirmedCharacter(object sender, PlayerIndexEventArgs e)
        {
            frmLoadChar.Enabled = true;

            string FileName = characters[index].Traits[(int)Global.Trait.Name];
            Global.SaveDevice.Delete("LevelUp", FileName + ".sav");
            characters.RemoveAt(index);
            index = 0;
            setCharacter();
        }
        #endregion Methods
    }
}