#region Header
// --------------------------------
// <copyright file="TitleScreen.cs" company="John Goewert">
//     Copyright John Goewert 2011.
// </copyright>
// <author>John Goewert</author>
// ---------------------------------
#endregion



using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
#if DEBUG || XBOX
using Microsoft.Xna.Framework.GamerServices;
#endif
using OrbUI.Components.Input.Controller;

namespace GameStateManagement
{
    /// <summary>
    /// The main menu screen is the first thing displayed when the game starts up.
    /// </summary>
    internal class TitleScreen : MenuScreen
    {
        #region Constructors
        bool enabled = true;
        bool signedin = false;
        /// <summary>
        /// Initializes a new instance of the TitleScreen class.
        /// </summary>
        public TitleScreen()
            : base("Title")
        {
            // Create our menu entries.
#if XBOX
            var startMenuEntry = new MenuEntry("Press A to Start");
#else
            var startMenuEntry = new MenuEntry("Press Space to Start");
#endif
            // Hook up menu event handlers.
            startMenuEntry.Selected += StartMenuEntrySelected;
            MenuEntries.Add(startMenuEntry);
            Mouse.SetPosition(GraphicsDeviceManager.DefaultBackBufferWidth / 2, GraphicsDeviceManager.DefaultBackBufferHeight / 2);
        }

        #endregion Constructors

        #region Methods

        public override void LoadContent()
        {
            MediaPlayer.IsMuted = false;
            Global.music.SetSong("Rains Will Fall");
            base.LoadContent();
        }
        /// <summary>
        /// When the user cancels the main menu, ask if they want to exit.
        /// </summary>
        public override void OnCancel(PlayerIndex playerIndex)
        {
            const string message = "Are you sure you want to exit?";

            var confirmExitMessageBox = new MessageBoxScreen(message);

            confirmExitMessageBox.Accepted += ConfirmExitMessageBoxAccepted;

            ScreenManager.AddScreen(confirmExitMessageBox, playerIndex);
        }

        /// <summary>
        /// Event handler for when the user selects ok on the "are you sure
        /// you want to exit" message box.
        /// </summary>
        private void ConfirmExitMessageBoxAccepted(object sender, PlayerIndexEventArgs e)
        {
            ScreenManager.Game.Exit();
        }

        /// <summary>
        /// Event handler for when the Play Game menu entry is selected.
        /// </summary>
        private void StartMenuEntrySelected(object sender, PlayerIndexEventArgs eventArgs)
        {
#if DEBUG || XBOX
            Global.CurrentPlayer = eventArgs.PlayerIndex;
            if (Gamer.SignedInGamers[Global.CurrentPlayer] == null)
                Guide.ShowSignIn(1, false);
            else if (!Gamer.SignedInGamers[Global.CurrentPlayer].IsSignedInToLive)
                Guide.ShowSignIn(1, false);
#endif
#if XBOX
            CursorState.GamePadPlayerIndex = Global.CurrentPlayer;
#endif

#if WINDOWS
            if (this.enabled)
            {
                LoadingScreen.Load(ScreenManager, false, Global.CurrentPlayer, new BackgroundScreen(), new MainMenuScreen());
                this.enabled = false;
            }
            
#endif
            signedin = true;
              // Load the Main Menu
//            LoadingScreen.Load(ScreenManager, false, Global.CurrentPlayer,  new MainMenuScreen());
//            LoadingScreen.Load(ScreenManager, false, Global.CurrentPlayer, new BackgroundScreen(), new MainMenuScreen());
        }

        public override void Update(GameTime gameTime, bool otherScreenHasFocus, bool coveredByOtherScreen)
        {
            base.Update(gameTime, otherScreenHasFocus, coveredByOtherScreen);
#if DEBUG || XBOX
            if (!this.IsExiting)
            {
                if (signedin)
                {
                    if (Gamer.SignedInGamers[Global.CurrentPlayer] != null)
                    {
                        foreach (SignedInGamer gamer in Gamer.SignedInGamers)
                        {

                            if (gamer.IsSignedInToLive)
                            {
                                LoadingScreen.Load(ScreenManager, false, Global.CurrentPlayer, new BackgroundScreen(), new MainMenuScreen());
                                this.enabled = false;
                            }
                        }
                    }
                }
            }
#endif
        }
        #endregion Methods
    }
}