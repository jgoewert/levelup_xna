#region Header
// --------------------------------
// <copyright file="LoadCharOptionsScreen.cs" company="John Goewert">
//     Copyright John Goewert 2011.
// </copyright>
// <author>John Goewert</author>
// ---------------------------------
#endregion

using Level_Up;
using Microsoft.Xna.Framework.GamerServices;


namespace GameStateManagement
{
    /// <summary>
    /// The options screen is brought up over the top of the main menu
    /// screen, and gives the user a chance to configure the game
    /// in various hopefully useful ways.
    /// </summary>
    internal class LoadCharOptionsMenuScreen : MenuScreen
    {
        #region Fields

        private readonly MenuEntry LoadMenuEntry;
        private readonly MenuEntry DeleteMenuEntry;
        private readonly MenuEntry BackMenuEntry;
        private string FileName;
        private SaveGameData Data;
        private LoadGameMenuScreen LoadGameScreen;
        #endregion Fields

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the OptionsMenuScreen class.
        /// </summary>
        public LoadCharOptionsMenuScreen(LoadGameMenuScreen LoadGameScreen, string FileName, SaveGameData Data)
            : base("Load/Save")
        {
            this.LoadGameScreen = LoadGameScreen;
            this.FileName = FileName;
            this.Data = Data;

            // Create our menu entries.
            LoadMenuEntry = new MenuEntry("Load Character");
            DeleteMenuEntry = new MenuEntry("Delete Character");
            BackMenuEntry = new MenuEntry("Back");

            // Hook up menu event handlers.
            LoadMenuEntry.Selected += LoadMenuEntrySelected;
            DeleteMenuEntry.Selected += DeleteMenuEntrySelected;
            BackMenuEntry.Selected += OnCancel;

            // Add entries to the menu.
            MenuEntries.Add(LoadMenuEntry);
            MenuEntries.Add(DeleteMenuEntry);
            MenuEntries.Add(BackMenuEntry);
        }

        #endregion Constructors

        #region Methods

        /// <summary>
        /// Event handler for when the Elf menu entry is selected.
        /// </summary>
        private void LoadMenuEntrySelected(object sender, PlayerIndexEventArgs e)
        {
            Player player = Player.Instance;
            player.Data = this.Data;
            ScreenManager.RemoveScreen(this.LoadGameScreen);
            this.ExitScreen();
            LoadingScreen.Load(ScreenManager, true, Gamer.SignedInGamers[Global.CurrentPlayer].PlayerIndex, new GameplayScreen());
        }

        /// <summary>
        /// Event handler for when the Frobnicate menu entry is selected.
        /// </summary>
        private void DeleteMenuEntrySelected(object sender, PlayerIndexEventArgs e)
        {
            Global.SaveDevice.Delete("LevelUp", FileName + ".sav");
//            this.LoadGameScreen.DeleteButton(FileName);
            this.ExitScreen();
        }

        #endregion Methods
    }
}