#region Header
// --------------------------------
// <copyright file="PauseMenuScreen.cs" company="John Goewert">
//     Copyright John Goewert 2011.
// </copyright>
// <author>John Goewert</author>
// ---------------------------------
#endregion
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.GamerServices;

using OrbUI.Components;
using OrbUI.Components.GUI;
using OrbUI.Components.GUI.Controls;
using OrbUI.Components.GUI.Cursors;
using OrbUI.Components.Input.Controller;

namespace GameStateManagement
{
    /// <summary>
    /// Initializes a new instance of the PauseMenuScreen class.
    /// The pause menu comes up over the top of the game,
    /// giving the player options to resume or quit.
    /// </summary>
    internal class PauseMenuScreen : MenuScreen
    {
        #region Fields
        private float pauseAlpha = 1.0f;

        private Form frmPauseMenu;
        private Button btnResume;
        private Button btnHighScores;
        private Button btnOptions;
        private Button btnQuitGame;
        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the PauseMenuScreen class.
        /// </summary>
        public PauseMenuScreen()
            : base("Paused")
        {
            InitPauseMenu();
        }

        private void InitPauseMenu()
        {
            frmPauseMenu = new Form("frmMainMenu", "", new Point(500, 270), new Point(280, 280), FormBorderStyle.None);
            frmPauseMenu.DragAnywhere = false;
            frmPauseMenu.ShowTitleBar = false;
            frmPauseMenu.FontName = "gamefont";
            frmPauseMenu.Font.LineSpacing = 0;
            frmPauseMenu.TitleTextColor = Color.White;
            frmPauseMenu.Show();

            btnResume = new Button("btnNew", new Point(5, 20), new Point(260, 50), "Resume", Color.Black);
            frmPauseMenu.Controls.Add(btnResume);
            btnResume.CursorUp += resumeGameMenuEntrySelected;

#if DEBUG || XBOX
            btnHighScores = new Button("btnHighScores", new Point(5, 80), new Point(260, 50), "High Scores", Color.Black);
            btnHighScores.CursorUp += HighScoresMenuEntrySelected;

            if ((Guide.IsTrialMode) &&
                (Gamer.SignedInGamers[Global.CurrentPlayer] != null))
            {
            }
            else
            {
                frmPauseMenu.Controls.Add(btnHighScores);
            }
#endif

            btnOptions = new Button("btnOptions", new Point(5, 140), new Point(260, 50), "Options", Color.Black);
            frmPauseMenu.Controls.Add(btnOptions);
            btnOptions.CursorUp += OptionsMenuEntrySelected;

            btnQuitGame = new Button("btnMainExit", new Point(5, 200), new Point(260, 50), "Main Menu", Color.Black);
            frmPauseMenu.Controls.Add(btnQuitGame);
            btnQuitGame.CursorUp += QuitGameMenuEntrySelected;
        }


        public override void Update(GameTime gameTime, bool otherScreenHasFocus, bool coveredByOtherScreen)
        {
            base.Update(gameTime, otherScreenHasFocus, coveredByOtherScreen);
            frmPauseMenu.TopMost = true;
            if (this.IsExiting)
            {
                if (frmPauseMenu.IsNotNullOrDisposed())
                    OrbFormCollection.Remove(frmPauseMenu);
            }
        }

        #endregion Constructors

        #region Methods
        protected override void OnSelectEntry(int entryIndex, PlayerIndex playerIndex)
        {
        }

        public override void UnloadContent()
        {
            if (frmPauseMenu.IsNotNullOrDisposed())
                OrbFormCollection.Remove(frmPauseMenu);
            base.UnloadContent();
        }

        /// <summary>
        /// Event handler for when the user selects ok on the "are you sure
        /// you want to quit" message box. This uses the loading screen to
        /// transition from the game back to the main menu screen.
        /// </summary>
        private void ConfirmQuitMessageBoxAccepted(object sender, PlayerIndexEventArgs e)
        {
            LoadingScreen.Load(ScreenManager, false, Global.CurrentPlayer, new BackgroundScreen(),
                               new MainMenuScreen());
        }

#if DEBUG || XBOX
        /// <summary>
        /// Event handler for when the Options menu entry is selected.
        /// </summary>
        private void HighScoresMenuEntrySelected(object sender)
        {
            ScreenManager.AddScreen(new HighScoresMenuScreen(), Global.CurrentPlayer);
            this.ExitScreen();
        }
#endif

        private void resumeGameMenuEntrySelected(object sender)
        {
            OnCancel(Global.CurrentPlayer);
        }

        /// <summary>
        /// Event handler for when the Options menu entry is selected.
        /// </summary>
        private void OptionsMenuEntrySelected(object sender)
        {
            ScreenManager.AddScreen(new OptionsMenuScreen(),Global.CurrentPlayer);
            this.ExitScreen();
        }

        /// <summary>
        /// Event handler for when the Quit Game menu entry is selected.
        /// </summary>
        private void QuitGameMenuEntrySelected(object sender)
        {
            const string message = "Are you sure you want to exit to the main menu?";

            var confirmQuitMessageBox = new MessageBoxScreen(message);

            confirmQuitMessageBox.Accepted += ConfirmQuitMessageBoxAccepted;

            ScreenManager.AddScreen(confirmQuitMessageBox, ControllingPlayer);
            this.ExitScreen();

        }

        /// <summary>
        /// Draws the gameplay screen.
        /// </summary>
        public override void Draw(GameTime gameTime)
        {
            // Darken down any other screens that were drawn beneath the popup.
            ScreenManager.FadeBackBufferToBlack(TransitionAlpha * 2 / 3);

            ScreenManager.SpriteBatch.Begin();
            if (frmPauseMenu.IsNotNullOrDisposed())
                frmPauseMenu.Draw(ScreenManager.GraphicsDevice, ScreenManager.SpriteBatch);

            OrbFormCollection.Cursor.Draw(ScreenManager.GraphicsDevice, ScreenManager.SpriteBatch);

            ScreenManager.SpriteBatch.End();
        }


        #endregion Methods
    }
}