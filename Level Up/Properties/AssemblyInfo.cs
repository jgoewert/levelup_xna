﻿#region Header
// --------------------------------
// <copyright file="AssemblyInfo.cs" company="John Goewert">
//     Copyright John Goewert 2011.
// </copyright>
// <author>John Goewert</author>
// ---------------------------------
#endregion

using System;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Security.Permissions;

[assembly: CLSCompliant(false)]

// General Information about an assembly is controlled through the following
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Level Up!")]
[assembly: AssemblyProduct("Level Up!")]
[assembly: AssemblyDescription("The MMORPG of the future")]
[assembly: AssemblyCompany("John Goewert")]
[assembly: AssemblyCopyright("Copyright © John Goewert 2011")]
[assembly: AssemblyTrademark("Level Up!")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible
// to COM components.  If you need to access a type in this assembly from
// COM, set the ComVisible attribute to true on that type. Only Windows
// assemblies support COM.
[assembly: ComVisible(false)]

// On Windows, the following GUID is for the ID of the typelib if this
// project is exposed to COM. On other platforms, it unique identifies the
// title storage container when deploying this assembly to the device.
[assembly: Guid("2fe307bc-fa37-42b8-a36a-3c4ef9eac1c5")]

// Version information for an assembly consists of the following four values:
//      Major Version
//      Minor Version
//      Build Number
//      Revision
[assembly: AssemblyVersion("1.0.0.0")]