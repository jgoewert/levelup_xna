﻿#region Header
// --------------------------------
// <copyright file="RomanNumeralConverter.cs" company="John Goewert">
//     Copyright John Goewert 2011.
// </copyright>
// <author>John Goewert</author>
// ---------------------------------
#endregion

// using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;

namespace LevelUp
{
    /* [TestFixture]
    public class RomanNumeralConverterTest
    {
        [Test]
        public void When_passed_1_should_convert_to_i()
        {
            Assert.AreEqual("I", RomanNumeralConverter.Convert("1"));
        }

        [Test]
        public void When_passed_2_should_convert_to_ii()
        {
            Assert.AreEqual("II", RomanNumeralConverter.Convert("2"));
        }

        [Test]
        public void When_passed_3_should_convert_to_iii()
        {
            Assert.AreEqual("III", RomanNumeralConverter.Convert("3"));
        }

        [Test]
        public void When_passed_4_should_convert_to_iv()
        {
            Assert.AreEqual("IV", RomanNumeralConverter.Convert("4"));
        }

        [Test]
        public void When_passed_5_should_convert_to_v()
        {
            Assert.AreEqual("V", RomanNumeralConverter.Convert("5"));
        }

        [Test]
        public void When_passed_6to8_should_convert_up_to_viii()
        {
            Assert.AreEqual("VI", RomanNumeralConverter.Convert("6"));
            Assert.AreEqual("VII", RomanNumeralConverter.Convert("7"));
            Assert.AreEqual("VIII", RomanNumeralConverter.Convert("8"));
        }

        [Test]
        public void When_passed_arabics_should_convert_to_roman_numerals()
        {
            Assert.AreEqual("CCXCI", RomanNumeralConverter.Convert("291"));
            Assert.AreEqual("XLIX", RomanNumeralConverter.Convert("49"));
            Assert.AreEqual("CDXC", RomanNumeralConverter.Convert("490"));
            Assert.AreEqual("CML", RomanNumeralConverter.Convert("950"));
            Assert.AreEqual("CMXCIX", RomanNumeralConverter.Convert("999"));
        }

        [Test]
        public void When_passed_i_should_convert_to_1()
        {
            Assert.AreEqual("1", RomanNumeralConverter.Convert("I"));
        }

        [Test]
        public void When_passed_ii_should_convert_to_2()
        {
            Assert.AreEqual("2", RomanNumeralConverter.Convert("II"));
        }

        [Test]
        public void When_passed_iii_should_convert_to_3()
        {
            Assert.AreEqual("3", RomanNumeralConverter.Convert("III"));
        }

        [Test]
        public void When_passed_iv_should_convert_to_4()
        {
            Assert.AreEqual("4", RomanNumeralConverter.Convert("IV"));
        }

        [Test]
        public void When_passed_v_should_convert_to_5()
        {
            Assert.AreEqual("5", RomanNumeralConverter.Convert("V"));
        }

        [Test]
        public void When_passed_vi_to_viii_should_convert_to_6_to_8()
        {
            Assert.AreEqual("6", RomanNumeralConverter.Convert("VI"));
            Assert.AreEqual("7", RomanNumeralConverter.Convert("VII"));
            Assert.AreEqual("8", RomanNumeralConverter.Convert("VIII"));
        }

        [Test]
        public void When_passed_roman_numerals_should_convert_to_arabics()
        {
            Assert.AreEqual("291", RomanNumeralConverter.Convert("CCXCI"));
            Assert.AreEqual("49", RomanNumeralConverter.Convert("XLIX"));
            Assert.AreEqual("490", RomanNumeralConverter.Convert("CDXC"));
            Assert.AreEqual("950", RomanNumeralConverter.Convert("CML"));
            Assert.AreEqual("999", RomanNumeralConverter.Convert("CMXCIX"));
        }
    }
    */

    public class RomanNumeral
    {
        #region Fields

        public int ArabicValue;

        public string Numeral;


        private RomanNumeral canBeModifiedBy;

        #endregion Fields

        #region Constructors

        public RomanNumeral(string numeral, int arabicValue)
            : this(numeral, arabicValue, null)
        {
        }

        public RomanNumeral(string numeral, int arabicValue, RomanNumeral canBeModifiedBy)
        {
            Numeral = numeral;
            this.ArabicValue = arabicValue;
            this.canBeModifiedBy = canBeModifiedBy;
        }

        #endregion Constructors

        #region Properties

        private RomanNumeral Modified
        {
            get
            {
                if (canBeModifiedBy == null) return null;
                return new RomanNumeral(canBeModifiedBy.Numeral + Numeral, ArabicValue - canBeModifiedBy.ArabicValue);
            }
        }

        #endregion Properties

        #region Methods

        public RomanNumeral Matches(int arabicValue)
        {
            if (this.ArabicValue <= arabicValue)
                return this;

            RomanNumeral modifiedNumeral = Modified;
            if (modifiedNumeral != null && modifiedNumeral.ArabicValue <= arabicValue)
                return modifiedNumeral;
            return null;
        }

        public RomanNumeral Matches(string romanNumeral)
        {
            RomanNumeral modifiedNumeral = Modified;
            if (modifiedNumeral != null && romanNumeral.StartsWith(modifiedNumeral.Numeral, StringComparison.CurrentCulture))
                return modifiedNumeral;

            if (Numeral[0] == romanNumeral[0])
            {
                return this;
            }

            return null;
        }

        #endregion Methods
    }

    public class RomanNumeralConverter
    {
        #region Fields

        private IList<RomanNumeral> parsed;

        #endregion Fields

        #region Constructors

        #endregion Constructors

        #region Properties

        public int Arabic
        {
            get { return parsed.Sum(x => x.ArabicValue); }
        }

        public string RomanNumeral
        {
            get
            {
                return parsed
                    .Aggregate(new StringBuilder(),
                               (sb, romanNumeral) => sb.Append(romanNumeral.Numeral))
                    .ToString();
            }
        }

        #endregion Properties

        #region Methods

        public static string Convert(string arabicOrRomanNumeral)
        {
            var converter = new RomanNumeralConverter();
            int arabic;
            if (int.TryParse(arabicOrRomanNumeral, out arabic))
                return converter.WithArabic(arabic).RomanNumeral;
            else
                return converter.WithRomanNumerals(arabicOrRomanNumeral).Arabic.ToString(CultureInfo.CurrentCulture);
        }

        public RomanNumeralConverter WithArabic(int number)
        {
            var parsed = new List<RomanNumeral>();
            while (number > 0)
            {
                RomanNumeral romanNumeral = RomanNumerals.FindFirstMatching(numeral => numeral.Matches(number));
                number -= romanNumeral.ArabicValue;
                parsed.Add(romanNumeral);
            }
            this.parsed = parsed;
            return this;
        }

        public RomanNumeralConverter WithRomanNumerals(string romanNumerals)
        {
            var parsed = new List<RomanNumeral>();
            while (romanNumerals.Length > 0)
            {
                RomanNumeral romanNumeral = RomanNumerals.FindFirstMatching(numeral => numeral.Matches(romanNumerals));
                romanNumerals = romanNumerals.Substring(romanNumeral.Numeral.Length);
                parsed.Add(romanNumeral);
            }
            this.parsed = parsed;
            return this;
        }

        #endregion Methods
    }

    public static class RomanNumerals
    {
        #region Fields

        public static RomanNumeral I = new RomanNumeral("I", 1);
        public static RomanNumeral X = new RomanNumeral("X", 10, I);
        public static RomanNumeral C = new RomanNumeral("C", 100, X);
        public static RomanNumeral D = new RomanNumeral("D", 500, C);
        public static RomanNumeral L = new RomanNumeral("L", 50, X);
        public static RomanNumeral M = new RomanNumeral("M", 1000, C);
        public static RomanNumeral V = new RomanNumeral("V", 5, I);

        public static IEnumerable<RomanNumeral> All = new List<RomanNumeral>
                                                          {
                                                              M,
                                                              D,
                                                              C,
                                                              L,
                                                              X,
                                                              V,
                                                              I
                                                          };

        #endregion Fields

        #region Methods

        public static RomanNumeral FindFirstMatching(Func<RomanNumeral, RomanNumeral> matcher)
        {
            return (from numeral in All
                    let matched = matcher(numeral)
                    where matched != null
                    select matched).First();
        }

        #endregion Methods
    }
}